#include <iostream>

int main(void)
{
	int n = 0;
	std::string input;
	int value;

	int stackArr[100000] = { 0, };
	int* pTop = stackArr;
	int* pPop = stackArr;

	std::cin >> n;

	for (int i = 0; i < n; ++i) 
	{
		std::cin >> input;

		if (input == "push")
		{
			std::cin >> value;
			*pPop++ = value;
		}
		else if (input == "top")
		{
			std::cout << (pPop == stackArr ? -1 : *(pPop - 1)) << std::endl;
		}
		else if (input == "size")
		{
			std::cout << pPop - pTop << std::endl;
		}
		else if (input == "empty")
		{
			std::cout << (pPop == stackArr ? 1 : 0) << std::endl;
		}
		else if (input == "pop")
		{
			std::cout << (pPop == stackArr ? -1 : *--pPop ) << std::endl;
		}
	}

	return 0;
}