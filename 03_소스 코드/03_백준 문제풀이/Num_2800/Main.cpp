#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <algorithm>
#include <cstring>
#include <vector>
#include <cmath>
#include <set>
#include <stack>

using namespace std;

int compStrings(const void* str1, const void* str2) 
{
	return (strcmp(*(char**)str1, *(char**)str2));
}

int qsort_solution()
{
	char s[201] = { '\0', };

	scanf("%s", s);

	int bks[11];
	memset(bks, -1, sizeof(int) * 11);

	char* p = s;
	int* pbks = bks;
	vector<pair<int, int>> vecBrackets;

	// 입력
	// : 200개 이내 char형

	// 문제 해결
	// : 괄호 쌍의 위치를 저장
	// ex) s[50]과 s[70]의 괄호가 쌍일 경우, 합쳐서 저장(vector)

	while (*p != '\0')
	{
		if (*p == '(')
		{
			*pbks++ = (p - s);
		}
		else if (*p == ')')
		{
			int v1 = *(pbks - 1);
			int v2 = p - s;
			vecBrackets.emplace_back(v1, v2);
			--pbks;
		}
		++p;
	}

	// 오류
	// : 괄호 쌍 (제대로 X or 하나도 없을 경우)
	if (pbks != bks || vecBrackets.empty())
	{
		return 0;
	}

	size_t len = p - s;
	p = s;

	// 출력 개수
	// : n개의 괄호 쌍 -> 2^n -1 개(괄호 쌍을 적어도 하나 이상 제거해야함)

	int check[11] = { 0, };
	int* pck = check;

	size_t vecLen = vecBrackets.size();

	char* pbuffer[1024] = { nullptr, };
	int bufCnt = 0;


	for (int i = 0; i < (2 << vecLen) - 1; ++i)
	{
		// check - modify
		pck = check;
		while (*pck != 0)
		{
			*pck = 0;
			++pck;
		}
		*pck = 1;

		// check - executing
		pck = check;
		int val[21] = { 0, };
		int cnt = 0;
		size_t diff;

		while (vecLen > (diff = pck - check))
		{
			if (*pck == 1)
			{
				val[cnt++] = vecBrackets[diff].first;
				val[cnt++] = vecBrackets[diff].second;
			}
			++pck;
		}

		int* pval = val;
		p = s;
		char tmpBuffer[201] = { '\0', };
		char* ptb = tmpBuffer;
		while (*p != '\0')
		{
			bool tf = false;
			int pos = p - s;
			for (int i = 0; i < cnt; ++i)
			{
				if (val[i] == pos)
				{
					tf = true;
					break;
				}
			}

			if (tf == true)
			{
				++p;
			}
			else
			{
				*ptb++ = *p++;
			}
		}

		size_t strLen = ptb - tmpBuffer;

		// 중복 제거
		int idx = 0;
		bool duplication = false;
		while (nullptr != pbuffer[idx])
		{
			if (!strcmp(pbuffer[idx], tmpBuffer))
			{
				duplication = true;
				break;
			}
			++idx;
		}

		if (duplication == false)
		{
			pbuffer[idx] = new char[strLen + 1];	// 메모리를 조금 더 절약할 수 있음
			//pbuffer[idx] = new char[len];			// 
			strcpy(pbuffer[idx], tmpBuffer);
			++bufCnt;
		}
	}

	int i = 0;

	qsort(pbuffer, bufCnt, sizeof(*pbuffer), compStrings);
	i = 0;
	while (nullptr != *(pbuffer + i))
	{
		printf("%s\n", pbuffer[i]);
		++i;
	}

	return 0;
}

int set_solution()
{
	char s[201] = { '\0', };
	char* p = s;

	scanf("%s", s);

	int bks[11];
	memset(bks, -1, sizeof(int) * 11);
	int* pbks = bks;

	bool bs[201] = { false, };
	vector<pair<int, int>> vecBrackets;

	set<string> answer;

	// 입력
	// : 200개 이내 char형

	// 문제 해결
	// : 괄호 쌍의 위치를 저장
	// ex) s[50]과 s[70]의 괄호가 쌍일 경우, 합쳐서 저장(vector)

	while (*p != '\0')
	{
		if (*p == '(')
		{
			*pbks++ = (p - s);
		}
		else if (*p == ')')
		{
			int v1 = *(pbks - 1);
			int v2 = p - s;
			vecBrackets.emplace_back(v1, v2);
			--pbks;
		}
		++p;
	}

	// 오류
	// : 괄호 쌍 (제대로 X or 하나도 없을 경우)
	if (pbks != bks || vecBrackets.empty())
	{
		return 0;
	}

	size_t len = p - s;
	p = s;

	size_t vecLen = vecBrackets.size();

	for (int n = 1; n < (1 << vecLen); ++n)
	{
		p = s;
		memset(bs, 0, sizeof(bs));
		string temp;

		for (int i = 0; i < vecLen; ++i)
		{
			if (n & (1 << i))
			{
				bs[vecBrackets[i].first] = true;
				bs[vecBrackets[i].second] = true;
			}
		}

		for (int i = 0; i < (int)len; ++i)
		{
			if (bs[i] == false)
			{
				temp.push_back(*p++);
			}
			else
			{
				++p;
			}
		}
		answer.insert(temp);
	}

	for (auto& x : answer)
	{
		cout << x << endl;
	}

	return 0;
}

int A[10], B[10], idx;
bool check[200];
set<string> answer;

int other_solution()
{
	string str; cin >> str;
	stack<pair<int, char>> s;
	for (int i = 0; i < (int)str.size(); i++) {
		if (str[i] == '(') s.push(make_pair(idx, '(')), A[idx++] = i;
		if (str[i] == ')') B[s.top().first] = i, s.pop();
	}

	for (int n = 1; n < (1 << idx); n++) {
		memset(check, 0, sizeof(check));
		string temp;
		for (int i = 0; i < idx; i++)
			if (n & (1 << i))
				check[A[i]] = check[B[i]] = true;

		for (int i = 0; i < (int)str.size(); i++)
			if (!check[i]) temp.push_back(str[i]);
		answer.insert(temp);
	}
	
	for (auto& x : answer)
	{
		cout << x << endl;
	}

	return 0;
}

int main()
{
	// qsort_solution();

	//other_solution();

	set_solution();

	return 0;
}