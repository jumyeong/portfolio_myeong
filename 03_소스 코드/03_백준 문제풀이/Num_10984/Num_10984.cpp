﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	int T, n, c, cs = 0;
	float g, gs = 0;

	scanf("%d", &T);
	while (T--)
	{
		cs = 0, gs = 0;
		scanf("%d", &n);
		while (n--)
		{
			scanf("%d %f", &c, &g);
			cs += c;
			gs += c * g;
		}
		printf("%d %.1f\n", cs, gs / cs);
	}
}