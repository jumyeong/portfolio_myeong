﻿//#define _CRT_SECURE_NO_WARNINGS
//#include <iostream>
//
//int main()
//{
//	int n1, n2;
//	scanf("%d %d", &n1, &n2);
//
//	// 소수
//	int PrimeNum = 0;
//	// LCM(Largest common multiple):최소공배수, GCF(Greatest common divisor):최대공약수
//	int LCM = 1, GCF = 1;	
//
//	// 비교적 작은 수, 큰 수
//	int max;
//	
//	int PrimeNums[100] = {0};
//
//	if (n1 > n2)
//		max = n1;
//	else
//		max = n2;
//
//	bool nums[10005];
//	// max + 1에서 소수를 비교할 영역(1~10000)
//	// 실제 소수로 쓰일 영역: 2 ~ 100 사이
//	// 문제점...
//	// 1. 동적할당을 할 시 문제 발생 가능성 높음
//	// 2. 크기를 +1 정도로는 문제가 발생할 가능성 있음(대충 +5정도는 해줘야 문제가 없음)
//	//bool* nums = (bool*)malloc(sizeof(bool)*(max + 1));
//	//bool* nums = new bool[max + 1];		
//
//
//	// 에라토스테네스의 체
//	std::fill_n(nums, max + 1, true);
//	
//	nums[0] = 0;
//	nums[1] = 0;
//
//	for (int i = 2; i * i <= max; i++)
//	{
//		if ((*nums) + i)	// nums[i]
//			for (int j = i * i; j <= max; j += i)
//				nums[j] = false;
//	}
//	// 소수 저장
//	
//	int j = 0;
//	for (int i = 0; i <= max; i++)
//	{
//		if (nums[i] == true)
//			PrimeNums[j++] = i;
//	}
//	// 소수 리스트
//
//	int tmp1 = n1, tmp2 = n2;
//
//	int i = 0;
//	while (PrimeNums[i])
//	{
//		// 두 수의 공약수
//		if (tmp1 % PrimeNums[i] == 0 && tmp2 % PrimeNums[i] == 0)
//		{
//			GCF *= PrimeNums[i];
//			tmp1 /= PrimeNums[i];
//			tmp2 /= PrimeNums[i];
//			i = 0;
//		}
//		else
//			i++;
//	}
//
//	LCM = (n1 * n2) / GCF;
//	
//	printf("%d\n%d\n", GCF, LCM);
//
//	return 0;
//}


// 다른 사람의 풀이
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	int a, b, i, g = 1;

	scanf("%d %d", &a, &b);
	
	// < 해석 >
	// : 두 수 중 임의의 수를 지정하고,
	// 그 값 전까지 나머지가 0인 값을 확인한다.
	// 나머지가 0인 값은 공약수로 최대 공약수를 얻기 위해서는
	// 가장 나중에 나온 나머지가 0이 되는 수만 저장되면 된다.
	
	//  추가로, 두 수 중 아무거나 상관없는 이유는
	// 공약수이기 때문에 두 값에 나눌 수 있는 수이기 때문에 작은 값이던 큰 값이던
	// 상관없다.
	for (i = 2; i <= a; i++)
	{
		if (a % i == 0 && b % i == 0) g = i;
	}

	// : 여기서는 최대 공약수를 구하는 식을 사용했다.
	// (최소 공배수) = (입력값 A) * (입력값 B) / (최대 공약수)
	printf("%d\n%d", g, a * b / g);
}