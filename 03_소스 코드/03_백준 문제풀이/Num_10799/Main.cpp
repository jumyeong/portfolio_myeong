#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <cstring>

int main()
{
	char line[100001] = { '\0', };

	scanf("%s", line);

	int num = strlen(line);

	char* p = line;
	int cnt = 0;
	int sum = 0;

	while (num-- > 0)
	{
		if (*p == '(')
		{
			++cnt;
		}
		else if (*p == ')')
		{
			--cnt;
			if (*(p - 1) == ')')
			{
				sum += 1;
			}
			else
			{
				sum += cnt;
			}
		}
		++p;
	}
	printf("%d\n", sum);

	return 0;
}