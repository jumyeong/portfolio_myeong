#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

int main()
{
	int n=0, p=0;
	char name[20] = {}, tmp[20] = {};
	int m, c;

	scanf("%d", &n);
	for (int i = 0; i < n; i++)
	{
		scanf("%d", &p);
		m = 0;
		for (int j = 0; j < p; j++)
		{  
			scanf("%d %s", &c, &tmp);
			
			if (m < c)
			{
				m = c;
				strcpy(name, tmp);
			}
		}
		printf("%s\n", name);
	}
}

// 버퍼 생길때, fflush(stdin) 추가 ->단, 알고리즘 시험에서는 사용하지 말것!(오래걸림)