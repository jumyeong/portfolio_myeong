#define _CRT_SECURE_NO_WARNINGS

#include <iostream>

int main()
{
	int n;
	char formula[100];
	char* pf = formula;
	double opends[26] = { 0, };
	double* op = opends;
	double result[100] = { 0 , };
	double* pResult = result;

	scanf("%d\n", &n);

	// 'A' : 65 , 'Z' : 90
	if (fgets(formula, 100, stdin) != NULL) {
	}

	while (n > 0) {
		scanf("%lf", op++);
		--n;
	}
	int opSize = op - opends;

	while (*pf != '\0') {
		if (*pf >= 65 && *pf <= 90) {
			for (int i = 0; i < opSize; ++i) {
				if (*pf == i + 65) {
					*pResult++ = opends[i];
					break;
				}
			}
		} else {
			if (pResult - result < 2) {
				break;
			}

			char ch = *pf;
			double o1 = *(pResult - 1);
			double o2 = *(pResult - 2);
			double res;

			if (ch == '*') {
				res = o2 * o1;
			}
			else if (ch == '+') {
				res = o2 + o1;
			}
			else if (ch == '/') {
				res = o2 / o1;
			}
			else if (ch == '-') {
				res = o2 - o1;
			}
			else {
				break;
			}
			*(pResult - 2) = res;
			pResult = pResult - 1;
		}

		++pf;
	}

	printf("%.2lf\n", result[0]);

	return 0;
}