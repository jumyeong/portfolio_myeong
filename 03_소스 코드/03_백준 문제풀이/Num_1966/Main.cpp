#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <queue>
#include <vector>

int main()
{
	int num;
	scanf("%d", &num);

	while (num-- > 0)
	{
		int docCnt;
		int docIdx;
		int findDoc = -1;

		std::queue<int> q;
		std::priority_queue<int> pq;


		scanf("%d %d", &docCnt, &docIdx);

		for (int i = 0; i < docCnt; ++i) 
		{
			int in;
			scanf("%d", &in);
			q.push(in);
			pq.push(in);

			if (docIdx == i)
			{
				findDoc = in;
			}
		}
		
		int orderNum = 1;

		while (true)
		{
			if (pq.top() <= q.front())
			{
				if (docIdx == 0)
				{
					printf("%d\n", orderNum);
					break;
				}

				q.pop();
				pq.pop();
				++orderNum;
			}
			else 
			{
				int fn = q.front();
				q.pop();
				q.push(fn);
				
			}

			--docIdx;
			if (docIdx < 0)
			{
				docIdx = q.size() - 1;
			}

		}

	}

	return 0;
}