﻿// Num_2440.cpp
#define _CRT_SECURE_NO_WARNINGS
#include <cstdio>

int main()
{
	int n;
	scanf("%d", &n);

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (j + i >= n)
				break;
			printf("*");
		}
		printf("\n");
	}
}