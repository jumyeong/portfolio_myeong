//#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stack>
#include <sstream>
#include <string>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
	int n;
	int temp;
	std::stack<std::pair<int, int>> arrStack;
	std::ostringstream str;

	std::cin >> n;

	for (int i = 0; i < n; ++i)
	{
		std::cin >> temp;

		while (arrStack.empty() == false && arrStack.top().first < temp)
		{
			arrStack.pop();
		}

		if (arrStack.empty())
		{
			str << "0 ";
		}
		else
		{
			str << arrStack.top().second << " ";
		}

		arrStack.push({ temp, i + 1 });
	}
	std::cout << str.str();

	return 0;
}