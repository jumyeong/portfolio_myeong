﻿// Num_2739.cpp
// 
// 
////////// 내 풀이
//#define _CRT_SECURE_NO_WARNINGS
//#include <iostream>
//
//#include <string>
//
//int main()
//{
//	int N = 0;
//	scanf("%d", &N);
//	std::string str = {};
//
//	// 반복문의 개수가 고정되어 있기 때문에 printf() 사용해도 상관없음
//	for (int i = 1; i < 10; i++)
//	{
//		str += std::to_string(N) + " * ";
//		str += std::to_string(i) + " = " + std::to_string(N * i);
//		str += "\n";
//	}
//	printf("%s", str.c_str());
//}

// 위의 변경 코드
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	int N = 0;
	scanf("%d", &N);
	for(int i = 1; i < 10; i++)
	{
		printf("%d * %d = %d\n", N, i, N * i);
	}
}