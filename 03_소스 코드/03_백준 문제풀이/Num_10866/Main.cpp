#include <iostream>
#include <cstring>
#include <cstdio>

int main()
{
	int n = 0;

	scanf("%d", &n);

	char s[20] = { '\0', };
	char initChar[20] = { '\0', };
	int numbers[10001];
	std::fill_n(numbers, 10001, -1);


	const char* funcName[20] = { "push_front", "push_back", "pop_front", "pop_back"
		, "size", "empty", "front", "back" };

	int val;
	int i = 0;

	while (n > i)
	{
		memcpy(s, initChar, 20 * sizeof(char));
		val = 0;

		scanf("%s", s);
		
		if (strcmp(s, "push_front") == 0)
		{
			scanf("%d\n", &val);

			int* p = numbers;

			while (*p != -1) 
			{
				++p;
			}

			while (p != numbers)
			{
				*(p + 1) = *p;
				--p;
			}
			*(p + 1) = *p;
			*p = val;
		}
		else if (strcmp(s, "push_back") == 0)
		{
			scanf("%d\n", &val);

			int* p = numbers;

			while (*p != -1)
			{
				++p;
			}

			*p = val;
		}
		else if (strcmp(s, "pop_front") == 0)
		{
			int* p = numbers;

			printf("%d\n", *p);
			
			while (*p != -1)
			{
				*p = *(p + 1);
				++p;
			}
		}
		else if (strcmp(s, "pop_back") == 0)
		{
			int* p = numbers;

			while (*p != -1)
			{
				++p;
			}

			if (p == numbers)
			{
				printf("%d\n", *p);
			}
			else
			{
				printf("%d\n", *(p - 1));
				*(p - 1) = -1;
			}
		}
		else if (strcmp(s, "size") == 0)
		{
			int* p = numbers;

			while (*p != -1)
			{
				++p;
			}

			printf("%d\n", (int)(p - numbers));
		}
		else if (strcmp(s, "empty") == 0)
		{
			int* p = numbers;

			while (*p != -1)
			{
				++p;
			}
			printf("%d\n", (p == numbers ? 1 : 0));
		}
		else if (strcmp(s, "front") == 0)
		{
			printf("%d\n", numbers[0]);
		}
		else if (strcmp(s, "back") == 0)
		{
			int* p = numbers;

			while (*p != -1)
			{
				++p;
			}

			if (p == numbers)
			{
				printf("%d\n", *p);
			}
			else
			{
				printf("%d\n", *(p - 1));
			}
		}
		
		++i;
	}

	return 0;
}