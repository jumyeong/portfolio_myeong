#define _CRT_SECURE_NO_WARNINGS


//// 코드 안보고 해결한 코드
//#include <stdio.h>
//#include <string.h>
//
//// 문자 '0' = 48
//int main()
//{
//	char cur[9], next[9];
//	scanf("%s", cur);
//	scanf("%s", next);
//
//	int ch, cm, cs;
//	int nh, nm, ns;
//	ch = (cur[0] - 48) * 10 + (cur[1] - 48);
//	cm = (cur[3] - 48) * 10 + (cur[4] - 48);
//	cs = (cur[6] - 48) * 10 + (cur[7] - 48);
//
//	nh = (next[0] - 48) * 10 + (next[1] - 48);
//	nm = (next[3] - 48) * 10 + (next[4] - 48);
//	ns = (next[6] - 48) * 10 + (next[7] - 48);
//
//	int rh, rm, rs;
//	rh = nh - ch;
//	if (rh <= 0)
//	{
//		rh += 24;
//	}
//	rm = nm - cm;
//	if (rm <= 0)
//	{
//		rh--;
//		rm += 60;
//	}
//	rs = ns - cs;
//	if (rs < 0)
//	{
//		rm--;
//		rs += 60;
//	}
//
//	if (rs == 60)
//	{
//		rm++;
//		rs = 0;
//	}
//	if (rm == 60)
//	{
//		rh++;
//		rm = 0;
//	}
//	if (rh == 24)
//	{
//		rh = 0;
//	}
//
//	char uh, dh, um, dm, us, ds;
//	uh = rh / 10 + '0';
//	dh = rh % 10 + '0';
//
//	um = rm / 10 + '0';
//	dm = rm % 10 + '0';
//
//	us = rs / 10 + '0';
//	ds = rs % 10 + '0';
//
//	printf("%c%c:%c%c:%c%c\n", uh, dh, um, dm, us, ds);
//}


// 다른 코드 참고후 
#include <stdio.h>

int main()
{
	int h1, m1, s1, h2, m2, s2;
	int cur, next;
	int res;
	scanf("%d:%d:%d", &h1, &m1, &s1);
	scanf("%d:%d:%d", &h2, &m2, &s2);

	cur = h1 * 3600 + m1 * 60 + s1;
	next = h2 * 3600 + m2 * 60 + s2;
	res = cur < next ? next - cur : 86400 - cur + next ;

	printf("%02d:%02d:%02d", res / 3600, res % 3600 / 60, res % 60);
	return 0;
}