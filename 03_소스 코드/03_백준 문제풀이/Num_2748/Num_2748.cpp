﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int main()
//{
//	int n;
//	long long t1 = 0, t2 = 1, tmp;
//	scanf("%d", &n);
//
//	for (int i = 1; i < n; i++)
//	{
//		tmp = t1 + t2;
//		t1 = t2;
//		t2 = tmp;
//	}
//	printf("%lld", t2);
//}

// 다른 풀이 방법
int main()
{
	int n;
	long long int a[100] = { 0, 1, 1 };
	for (int i = 3; i <= 90; i++)
		a[i] = a[i - 1] + a[i - 2];
	// 장점 :
	// 미리 계산하여 저장해둘 수 있음
	// 단, 저장하는 공간을 그만큼 더 사용하게 된다.
	scanf("%d", &n);
	printf("%lld", a[n]);
}

