#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

int main()
{
	int x;
	int min=0;
	int max=0;
	int m, d, y, ymd;
	char young[15] = {}, old[15] = {}, name[15] = {};

	scanf("%d", &x);

	while (x--)
	{
		scanf("%s %d %d %d", name, &d, &m, &y);
		ymd = y * 10000 + m * 100 + d;		// 높으면 나이가 어림, 낮으면 나이가 많음

		if (min == 0)
		{
			min = ymd, strcpy(old, name);
			continue;
		}
		if (max == 0)
		{
			if (ymd < min)
			{
				max = min;
				min = ymd;
				strcpy(young, old);
				strcpy(old, name);
			}
			else
				max = ymd, strcpy(young, name);
			continue;
		}

		if (ymd < min)
		{
			min = ymd;
			strcpy(old, name);
		}
		else if (ymd > max)
		{
			max = ymd;
			strcpy(young, name);
		}
	}
	printf("%s\n%s\n", young, old);
}