#include <iostream>


int main()
{
	int n;
	int k;
	int resArr[5001] = { 0, };
	int arr[5001] = { 0, };
	int* p = arr;
	int* pEnd = arr;
	int* q = resArr;

	scanf("%d %d", &n, &k);

	int i = 1;
	while (i <= n) {
		*p++ = i++;
	}
	p = arr;

	int removeCnt = 1;

	while (*arr != 0)
	{
		if (removeCnt == k)
		{
			*q++ = *p++;
			removeCnt = 0;
		}
		else
		{
			++p;
			++pEnd;
		}

		*pEnd = *p;

		++removeCnt;

		if (*p == 0)
		{
			p = arr;
			pEnd = arr;
		}
	}

	q = resArr;
	printf("<");
	while (*q != 0)
	{
		if (*(q + 1) == 0) {
			printf("%d>", *q++);
		}
		else
		{
			printf("%d, ", *q++);
		}
	}

	return 0;
}