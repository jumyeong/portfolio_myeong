#include <iostream>
#include <cstdio>

int list[1000000] = { 0, };

int OtherSolution() {
	int N, k = 1;
	scanf("%d", &N);
	while (k < N) k += k;
	printf("%d", 2 * N - k);
	// 규칙 확인
	// 1) N이 2의 승수일 경우, 최대 N 값 그대로 출력
	// 2) N이 2의 승수가 아닐 경우, 
	//  " 2^(n-1) <  N  < 2^n " 을 기준으로
	// N을 최소값부터 N의 승수전 값까지  2 * 1, 2 * 2, 2 * 3, 2 * 4, ...., 2의 승수 전까지 결과값 나옴 

	return 0;
}
int main()
{
	if (false) {
		int number;

		scanf("%d\n", &number);

		int* p = list;
		int* end = p + number;

		for (int i = 0; i < number; ++i) {
			list[i] = i + 1;
		}

		while (*p != 0) {
			if (*(p + 1) != 0)
			{
				*end = *(p + 1);
				++end;
				p = p + 2;
			}
			else
			{
				break;
			}
		}

		printf("%d\n", *p);
	}

	OtherSolution();

	return 0;
}
