#include <iostream>
#include <cstdio>
#include <cstring>

int main()
{
	int n;
	scanf("%d\n", &n);

	// �ùٸ� ��ȣ
	// 1. ¦��
	// 2. �� ��ȣ�� ������ ����
	// 3. ������ ��ȣ�� ������ ������ fail

	int res[] = { 1, 0 };

	for (int i = 0; i < n; ++i)
	{
		int leftCnt = 0;
		int rightCnt = 0;

		char str[50] = { '\0', };

		scanf("%s", str);

		size_t len = strlen(str);


		const char* p = str;
		// Ȧ�� �϶�,
		if (len & 0x01) 
		{
			printf("NO\n");
			continue;
		}

		while (*p != '\0')
		{
			if (*p == '(')
			{
				++leftCnt;
			}
			else
			{
				++rightCnt;
			}

			if (rightCnt > leftCnt) 
			{
				break;
			}
			++p;
		}

		printf("%s\n", (leftCnt != rightCnt ? "NO" : "YES"));
	}

	return 0;
}