#define _CRT_SECURE_NO_WARNINGS

#include <iostream>

int main()
{
	char s[30];

	scanf("%s", s);

	char* p = s;
	char cmp[30] = { '\0', };
	char* cp = cmp;

	int res = 0;
	int tmp = 1;
	int chk = 0;

	while (*p != '\0')
	{
		if (*p == '(')
		{
			++chk;
			tmp *= 2;
			*cp++ = '(';
		}
		else if (*p == '[')
		{
			++chk;
			tmp *= 3;
			*cp++ = '[';
		}
		else if (*p == ')')
		{
			if (p == s)
			{
				chk = 1;
				break;
			}

			if (cp == cmp || *(cp - 1) != '(')
			{
				chk = 1;
				break;
			}

			if (*(p - 1) == '(')
			{
				--chk;
				res += tmp;
				tmp /= 2;
				--cp;
			}
			else if (*(p - 1) == '[')
			{
				chk = 1;
				break;
			}
			else
			{
				--chk;
				tmp /= 2;
				--cp;
			}

			if (tmp < 1)
			{
				chk = 1;
				break;
			}
		}
		else if (*p == ']')
		{
			if (p == s) 
			{
				chk = 1;
				break;
			}

			if (cp == cmp || *(cp - 1) != '[')
			{
				chk = 1;
				break;
			}

			if (*(p - 1) == '[')
			{
				--chk;
				res += tmp;
				tmp /= 3;
				--cp;
			}
			else if (*(p - 1) == '(')
			{
				chk = 1;
				break;
			}
			else
			{
				--chk;
				tmp /= 3;
				--cp;
			}

			if (tmp < 1)
			{
				chk = 1;
				break;
			}
		}

		++p;
	}

	if (tmp != 1 || chk != 0)
	{
		printf("%d", 0);
	}
	else
	{
		printf("%d", res);
	}

	return 0;
}