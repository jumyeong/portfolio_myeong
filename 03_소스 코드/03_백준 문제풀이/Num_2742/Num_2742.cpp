﻿//// Num_2742.cpp
//#define _CRT_SECURE_NO_WARNINGS
//#include <iostream>
//#include <string>
//
//int main()
//{
//	int in;
//	std::string str, tmpStr;
//	scanf("%d", &in);
//
//	int i = 1;
//	do	
//	{
//		str += std::to_string(i) + '\n';
//	} while (in != i++);
//
//	printf("%s", str.c_str());
//}

// Num_2742.cpp 역순
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>

int main()
{
	int in;
	std::string str, tmpStr;
	scanf("%d", &in);

	while (in)
	{
		str += std::to_string(in--) + '\n';
	}

	printf("%s", str.c_str());
}