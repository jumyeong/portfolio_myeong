﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	int n;
	int a, b;
	for (scanf("%d", &n); n--;)
	{
		scanf("%d %d", &a, &b);
		printf("%d\n", a + b);
	}
}