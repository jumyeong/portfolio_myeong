#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <queue>

int main()
{
	int n;

	// 2^31 < 자연수 => int 범위 내
	scanf("%d", &n);

	std::priority_queue<int> pq;

	while (n > 0)
	{
		int tmp;
		scanf("%d", &tmp);

		if (tmp == 0)
		{
			if (pq.empty())
			{
				printf("%d\n", 0);
			}
			else
			{
				printf("%d\n", pq.top());
				pq.pop();
			}
		}
		else
		{
			pq.push(tmp);
		}

		--n;
	}

	return 0;
}