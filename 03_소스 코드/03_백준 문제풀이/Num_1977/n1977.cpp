//#include <iostream>
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	int M, N;
	int sum = 0, min = 10000;

	//std::cin >> M >> N;
	scanf("%d%d", &M, &N);
	for (int i = 1; i * i <= N; i++)
	{
		if (i * i >= M)
		{
			sum += i * i;
			min = min < sum ? min : sum;
		}
	}
	
	printf(sum ? "%d\n%d": "-1", sum, min);
}

// 입력 받을 경우 : scanf()
// 입력(문자) 받을 경우 : getchar()
// 참고: https://2heedu.tistory.com/64
// https://www.acmicpc.net/blog/view/56