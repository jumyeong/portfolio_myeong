#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <string>
#include <set>

using namespace std;

int main()
{
	int n, m;
	// 1 <= N, M <= 10000

	scanf("%d %d", &n, &m);

	set<string> set1;

	while (n > 0)
	{
		string tmp;
		cin >> tmp;

		set1.insert(tmp);

		--n;
	}

	int cnt = 0;
	while (m > 0)
	{
		string tmp;
		cin >> tmp;

		if (set1.count(tmp)) 
		{
			++cnt;
		}
		--m;
	}

	cout << cnt << '\n';

	return 0;
}