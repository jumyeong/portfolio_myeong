﻿// Num_2438.cpp

//// My Code
//#define _CRT_SECURE_NO_WARNINGS
//#include <stdio.h>
//#include <string>
//
//int main()
//{
//	int N;
//	scanf("%d", &N);
//
//	std::string star;
//	for (int i = 1; i <= N; i++)
//	{
//		star += '*';
//		printf("%s\n", star.c_str());
//	}
//}

// Other Code
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	int N;
	scanf("%d", &N);

	for (int i = 1; i <= N; i++)
	{
		for (int j = 1; j <= i; j++)
		{
			printf("*");
		}
		printf("\n");
	}
}