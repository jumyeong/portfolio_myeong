#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <string>
#include <cstring>
#include <algorithm>
#include <map>

using namespace std;

int main()
{
	cin.tie(0);
	ios::sync_with_stdio(false);
	int n;
	int m;

	cin >> n >> m;
	
	map<string, string> m1;

	for (int i = 1; i <= n; ++i)
	{
		string s;
		cin >> s;

		m1[s] = to_string(i);
		m1[to_string(i)] = s;
	}

	for (int i = 0; i < m; ++i)
	{
		string s;
		cin >> s;
		
		cout << m1[s] << "\n";
	}
	return 0;
}