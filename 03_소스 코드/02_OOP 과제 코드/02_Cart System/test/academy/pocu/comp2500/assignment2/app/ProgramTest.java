package academy.pocu.comp2500.assignment2.app;

import academy.pocu.comp2500.assignment2.Aperture;
import academy.pocu.comp2500.assignment2.Banner;
import academy.pocu.comp2500.assignment2.BannerMaterial;
import academy.pocu.comp2500.assignment2.BannerSize;
import academy.pocu.comp2500.assignment2.BusinessCard;
import academy.pocu.comp2500.assignment2.BusinessCardColor;
import academy.pocu.comp2500.assignment2.BusinessCardSides;
import academy.pocu.comp2500.assignment2.Calendar;
import academy.pocu.comp2500.assignment2.CalendarType;
import academy.pocu.comp2500.assignment2.Cart;
import academy.pocu.comp2500.assignment2.Color;
import academy.pocu.comp2500.assignment2.Orientation;
import academy.pocu.comp2500.assignment2.PaperMaterial;
import academy.pocu.comp2500.assignment2.ShippingOption;
import academy.pocu.comp2500.assignment2.Size;
import academy.pocu.comp2500.assignment2.Stamp;
import academy.pocu.comp2500.assignment2.StampColor;
import academy.pocu.comp2500.assignment2.StampSize;
import academy.pocu.comp2500.assignment2.TextAperture;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class ProgramTest {
    @Test
    void Test00() {
        // 3.1
        UUID id = UUID.randomUUID();
        Stamp stamp = new Stamp(id, ShippingOption.PICK_UP, "text", StampColor.RED, StampSize.SHORT);
        assert (stamp.getShippingMethod() == ShippingOption.PICK_UP);

        Color color = stamp.getColor();
        assert (color.getRed() == 0xFF);
        assert (color.getGreen() == 0x00);
        assert (color.getBlue() == 0x00);

        // 3.2
        id = UUID.randomUUID();
        Cart cart = new Cart(id);
        cart.addProduct(stamp);
        assert (cart.getProducts().size() == 1);

        stamp.setShippingMethod(ShippingOption.SHIP);
        assert (stamp.getShippingMethod() == ShippingOption.SHIP);
        assert (cart.getProductOrNull(stamp.getId()).getShippingMethod() == ShippingOption.SHIP);
    }

    @Test
    void Test01() {
        // 3.3
        UUID id = UUID.randomUUID();
        Calendar calendar = new Calendar(id, ShippingOption.SHIP, CalendarType.WALL_CALENDAR);
        Color color = new Color(0xff, 0xff, 0xff);
        Size size = new Size(400, 400);

        assert (calendar.getPrice() == 1000);

        assert (calendar.getSize().getHeight() == size.getHeight());
        assert (calendar.getSize().getWidth() == size.getWidth());

        assert (calendar.getColor().getRed() == color.getRed());
        assert (calendar.getColor().getGreen() == color.getGreen());
        assert (calendar.getColor().getBlue() == color.getBlue());

        // 3.4
        id = UUID.randomUUID();
        Cart cart = new Cart(id);
        cart.addProduct(calendar);

        assert (cart.getProductOrNull(calendar.getId()).getShippingMethod() == ShippingOption.SHIP);
        cart.getProductOrNull(calendar.getId()).setShippingMethod(ShippingOption.PICK_UP);
        assert (cart.getProductOrNull(calendar.getId()).getShippingMethod() == ShippingOption.PICK_UP);
    }

    @Test
    void Test_X06() {
        UUID id = UUID.randomUUID();
        BusinessCard bc = new BusinessCard(id,
                ShippingOption.PICK_UP,
                Orientation.PORTIRAIT,
                BusinessCardSides.SINGLE,
                PaperMaterial.LINEN,
                BusinessCardColor.IVORY);
        int price = bc.getPrice();
        assert (price == 110);

        id = UUID.randomUUID();
        Banner banner = new Banner(id,
                ShippingOption.SHIP,
                Orientation.PORTIRAIT,
                new Color(0xff, 0xff, 0xff),
                BannerMaterial.GLOSS,
                BannerSize.ONE_AND_ONE_HALF);

        price = banner.getPrice();
        assert (price == 5000);

        id = UUID.randomUUID();
        TextAperture textAperture = new TextAperture(id,
                0, 0, new Size(20, 20), "textAperture");
        banner.addAperture(textAperture);

        price = banner.getPrice();
        assert (price == 5005);

        id = UUID.randomUUID();
        Cart cart = new Cart(id);
        cart.addProduct(bc);
        cart.addProduct(banner);
        assert (cart.getTotalPrice() == 5115);

        banner.removeAperture(textAperture);
        assert (cart.getTotalPrice() == 5110);
    }

    @Test
    void Test_X08() {
        UUID id = UUID.randomUUID();
        BusinessCard bc = new BusinessCard(id,
                ShippingOption.PICK_UP,
                Orientation.PORTIRAIT,
                BusinessCardSides.SINGLE,
                PaperMaterial.LINEN,
                BusinessCardColor.IVORY);

        id = UUID.randomUUID();
        TextAperture textAperture1 = new TextAperture(id,
                -1, 0, new Size(1, 1), "test01");
        bc.addAperture(textAperture1);
        assert (0 == bc.getApertures().size());

        id = UUID.randomUUID();
        TextAperture textAperture2 = new TextAperture(id,
                0, -1, new Size(1, 1), "test02");
        bc.addAperture(textAperture2);
        assert (0 == bc.getApertures().size());

        id = UUID.randomUUID();
        TextAperture textAperture3 = new TextAperture(id,
                90, 0, new Size(1, 1), "test03");
        bc.addAperture(textAperture3);
        assert (0 == bc.getApertures().size());

        id = UUID.randomUUID();
        TextAperture textAperture4 = new TextAperture(id,
                0, 50, new Size(1, 1), "test04");
        bc.addAperture(textAperture4);
        assert (0 == bc.getApertures().size());

        id = UUID.randomUUID();
        TextAperture textAperture5 = new TextAperture(id,
                0, 30, new Size(1, 20), "test05");
        bc.addAperture(textAperture5);
        assert (0 == bc.getApertures().size());

        id = UUID.randomUUID();
        TextAperture textAperture6 = new TextAperture(id,
                50, 0, new Size(40, 1), "test06");
        bc.addAperture(textAperture6);
        assert (0 == bc.getApertures().size());

    }
}