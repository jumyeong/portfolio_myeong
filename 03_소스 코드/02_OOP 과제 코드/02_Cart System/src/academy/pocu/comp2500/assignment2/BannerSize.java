package academy.pocu.comp2500.assignment2;

public enum BannerSize {
    ONE_AND_ONE_HALF,
    ONE_AND_ONE,
    TWO_AND_ONE_HALF,
    THREE_AND_ONE
}
