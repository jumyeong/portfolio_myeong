package academy.pocu.comp2500.assignment2;

public enum StampSize {
    SHORT,
    MIDDLE,
    LONG
}
