package academy.pocu.comp2500.assignment3;

import java.util.ArrayList;

public class Mine extends Unit implements ICollisionEventListener {
    protected int stepCount;
    protected final int triggerStepCount;

    public Mine(final IntVector2D intVector2D, final int triggerStepCount) {
        super(intVector2D);
        this.hp = 1;
        this.ap = 10;
        this.vision = 0;
        this.areaOfEffect = 0;
        this.unitType = UnitType.GROUND;

        this.attackableUnitTypes.add(UnitType.GROUND);

        this.triggerStepCount = triggerStepCount;
        this.invisible = true;
    }

    @Override
    public void onAttacked(int damage) {
        this.hp -= damage;
        this.hp = (this.hp > 0 ? this.hp : 0);
    }

    @Override
    public void onSpawn() {
        SimulationManager.getInstance().registerCollisionEventListener(this);
    }

    @Override
    public char getSymbol() {
        return 'N';
    }

    @Override
    public boolean collsionEvent(Unit unit) {
        if (this.equals(unit)
                || unit.unitType.equals(UnitType.AIR)) {
            return false;
        }

        int thisX = this.intVector2D.getX();
        int thisY = this.intVector2D.getY();

        int otherX = unit.intVector2D.getX();
        int otherY = unit.intVector2D.getY();

        if (thisX != otherX
                || thisY != otherY) {
            return false;
        }

        ++this.stepCount;

        if (this.stepCount < this.triggerStepCount) {
            return false;
        }

        this.hp = 0;
        this.attackIntent = new AttackIntent(this, this.intVector2D);
        return true;
    }
}
