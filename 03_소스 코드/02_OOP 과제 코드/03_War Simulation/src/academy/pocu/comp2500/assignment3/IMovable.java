package academy.pocu.comp2500.assignment3;

import java.util.ArrayList;

public interface IMovable {
    void move();
}
