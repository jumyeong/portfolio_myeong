package academy.pocu.comp2500.assignment3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class AttackIntent {
    private Unit owner;
    private IntVector2D targetArea;
    private HashMap<IntVector2D, Integer> attackedAreasAndDamage = new HashMap<>();

    public AttackIntent(Unit owner, IntVector2D targetArea) {
        this.owner = owner;
        this.targetArea = targetArea;

        calculateDamageInArea();
    }

    public Unit getOwner() {
        return this.owner;
    }

    public IntVector2D getTargetArea() {
        return this.targetArea;
    }

    public void setTargetArea(HashMap<IntVector2D, Integer> attackedAreasAndDamage) {
        this.attackedAreasAndDamage = attackedAreasAndDamage;
    }

    private void calculateDamageInArea() {
        int leftX = Math.max(this.targetArea.getX() - this.owner.getAreaOfEffect(), 0);
        int rightX = Math.min(this.targetArea.getX() + this.owner.getAreaOfEffect(), this.targetArea.getMaxX() - 1);
        int topY = Math.max(this.targetArea.getY() - this.owner.getAreaOfEffect(), 0);
        int bottomY = Math.min(this.targetArea.getY() + this.owner.getAreaOfEffect(), this.targetArea.getMaxY() - 1);

        ArrayList<Integer> rangeX = new ArrayList<>();
        ArrayList<Integer> rangeY = new ArrayList<>();

        while (leftX <= rightX) {
            rangeX.add(leftX);
            ++leftX;
        }
        while (topY <= bottomY) {
            rangeY.add(topY);
            ++topY;
        }

        for (int x : rangeX) {
            for (int y : rangeY) {
                double distanceFromAttackedArea = Math.max(Math.abs(x - this.targetArea.getX()), Math.abs(y - this.targetArea.getY()));
                int calculateDamage = (int) (((double) this.owner.getAp()) * (1.0 - distanceFromAttackedArea / (this.owner.getAreaOfEffect() + 1.0)));

                this.attackedAreasAndDamage.put(new IntVector2D(x, y), calculateDamage);
            }
        }
    }

    public void attackAreas(ArrayList<Unit> units) {
        for (Unit enemy : units) {
            if (enemy.equals(this.owner)
                    && !this.owner.getAttackableUnitTypes().contains(enemy.getUnitType())) {
                continue;
            }

            for (Entry<IntVector2D, Integer> areaAndDamage : this.attackedAreasAndDamage.entrySet()) {
                IntVector2D area = areaAndDamage.getKey();
                int damage = areaAndDamage.getValue();

                if (!enemy.equals(this.owner)
                        && this.owner.getAttackableUnitTypes().contains(enemy.getUnitType())
                        && enemy.getPosition().getX() == area.getX()
                        && enemy.getPosition().getY() == area.getY()) {
                    enemy.onAttacked(damage);
                }
            }
        }
    }
}
