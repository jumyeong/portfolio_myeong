package academy.pocu.comp2500.assignment3;

import java.util.ArrayList;

public class Wraith extends Unit implements IThinkable, IMovable {
    private IntVector2D firstPosition = new IntVector2D(this.intVector2D.getX(), this.intVector2D.getY());
    private int shieldCount = 1;
    private boolean onShield = false;

    public Wraith(final IntVector2D intVector2D) {
        super(intVector2D);
        this.hp = 80;
        this.ap = 6;
        this.vision = 4;
        this.areaOfEffect = 0;
        this.unitType = UnitType.AIR;

        this.attackableUnitTypes.add(UnitType.AIR);
        this.attackableUnitTypes.add(UnitType.GROUND);

        this.pointOfAttack = new int[][]{{0, 0}, {0, -1}, {1, 0}, {0, 1}, {-1, 0}};
    }

    @Override
    public IntentType think(ArrayList<Unit> units) {
        detectUnits(units);
        updateAttackableTiles();

        this.attackIntent = null;
        this.nextMovePosition = null;
        this.onShield = false;

        // 탐지된게 아무것도 없을 경우,
        if (this.detectedUnits.size() == 0) {
            this.nextMovePosition = decideMoveArea();
            return IntentType.MOVE;
        }

        // 탐지 됨.
        // 공격할 수 있는 타일에 적이 있는 지 여부.
        ArrayList<Unit> attackableEnemies = getAttackableEnemies();
        ArrayList<Unit> airTypeEnemies = new ArrayList<>();
        ArrayList<Unit> groundTypeEnemies = new ArrayList<>();

        if (attackableEnemies.size() == 1) {
            this.attackIntent = new AttackIntent(this, attackableEnemies.get(0).getPosition());
            return IntentType.ATTACK;
        } else if (attackableEnemies.size() > 1) {
            // Unit Type : Air, Ground
            for (Unit otherTypeUnit : attackableEnemies) {
                if (otherTypeUnit.unitType.equals(UnitType.AIR)) {
                    airTypeEnemies.add(otherTypeUnit);
                } else {
                    groundTypeEnemies.add(otherTypeUnit);
                }
            }

            if (airTypeEnemies.size() == 0) {
                this.attackIntent = createAttackIntent(groundTypeEnemies);
            } else {
                this.attackIntent = createAttackIntent(airTypeEnemies);
            }
            return IntentType.ATTACK;
        }

        this.nextMovePosition = decideMoveArea();
        return IntentType.MOVE;
    }

    private IntVector2D decideMoveArea() {
        int thisX = this.intVector2D.getX();
        int thisY = this.intVector2D.getY();

        // 시야에 적 없을 시,
        if (this.detectedUnits.size() == 0) {
            int targetX = this.firstPosition.getX();
            int targetY = this.firstPosition.getY();

            if (thisY != targetY) {
                thisY = (thisY > targetY ? thisY - 1 : thisY + 1);
            } else if (thisX != targetX) {
                thisX = (thisX > targetX ? thisX - 1 : thisX + 1);
            }
            return new IntVector2D(thisX, thisY);
        }

        ArrayList<Unit> airTypeEnemies = new ArrayList<>();
        ArrayList<Unit> groundTypeEnemies = new ArrayList<>();

        // Unit Type : Air, Ground
        for (Unit otherTypeUnit : this.detectedUnits) {
            if (otherTypeUnit.unitType.equals(UnitType.AIR)) {
                airTypeEnemies.add(otherTypeUnit);
            } else {
                groundTypeEnemies.add(otherTypeUnit);
            }
        }

        if (airTypeEnemies.size() != 0) {
            return priorityMovePosition(airTypeEnemies);
        }
        return priorityMovePosition(groundTypeEnemies);
    }

    @Override
    public void move() {
        this.intVector2D.setX(this.nextMovePosition.getX());
        this.intVector2D.setY(this.nextMovePosition.getY());
    }

    private IntVector2D priorityMovePosition(ArrayList<Unit> detectedUnits) {
        int thisX = this.intVector2D.getX();
        int thisY = this.intVector2D.getY();

        ArrayList<Unit> priorityUnits = detectedUnits;
        if (priorityUnits.size() == 1) {
            return targetingMovePriorityDirection(priorityUnits.get(0));
        }

        priorityUnits = compareToShortDistance(detectedUnits);
        if (priorityUnits.size() == 1) {
            return targetingMovePriorityDirection(priorityUnits.get(0));
        }

        priorityUnits = compareToMinHp(priorityUnits);
        if (priorityUnits.size() == 1) {
            return targetingMovePriorityDirection(priorityUnits.get(0));
        }

        // 8개 구역 확인
        for (Unit compareUnit : priorityUnits) {
            if (thisY > compareUnit.intVector2D.getY()
                    && thisX == compareUnit.intVector2D.getX()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisY > compareUnit.intVector2D.getY()
                    && thisX < compareUnit.intVector2D.getX()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisX < compareUnit.intVector2D.getX()
                    && thisY == compareUnit.intVector2D.getY()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisX < compareUnit.intVector2D.getX()
                    && thisY < compareUnit.intVector2D.getY()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisY < compareUnit.intVector2D.getY()
                    && thisX == compareUnit.intVector2D.getX()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisY < compareUnit.intVector2D.getY()
                    && thisX > compareUnit.intVector2D.getX()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisX > compareUnit.intVector2D.getX()
                    && thisY == compareUnit.intVector2D.getY()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisX > compareUnit.intVector2D.getX()
                    && thisY > compareUnit.intVector2D.getY()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        assert (false) : "Impossiable Target Unit Position";
        return new IntVector2D(thisX, thisY);
    }

    @Override
    public void onAttacked(int damage) {
        if (onShield == true) {
            return;
        }

        if (onShield == false && shieldCount > 0) {
            onShield = true;
            --shieldCount;
            return;
        }

        this.hp -= damage;
        this.hp = (this.hp > 0 ? this.hp : 0);
    }

    @Override
    public void onSpawn() {
        SimulationManager.getInstance().registerThinkable(this);
    }

    @Override
    public char getSymbol() {
        return 'W';
    }
}
