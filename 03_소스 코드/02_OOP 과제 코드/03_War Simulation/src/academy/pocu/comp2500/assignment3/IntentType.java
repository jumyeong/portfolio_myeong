package academy.pocu.comp2500.assignment3;

public enum IntentType {
    ATTACK,
    MOVE,
    NONE
}
