package academy.pocu.comp2500.assignment3;

import java.util.ArrayList;
import java.util.HashMap;

public class Destroyer extends Unit implements IThinkable {

    private HashMap<IntVector2D, Integer> bombingAllAreas = new HashMap<>();

    public Destroyer(final IntVector2D intVector2D) {
        super(intVector2D);
        this.hp = 999;
        this.ap = 999;
        this.vision = 0;
        this.areaOfEffect = 0;
        this.unitType = UnitType.AIR;

        this.attackableUnitTypes.add(UnitType.AIR);
        this.attackableUnitTypes.add(UnitType.GROUND);

        this.pointOfAttack = new int[][]{
                {0, -2}, {1, -2},
                {2, -1}, {2, 0}, {2, 1},
                {1, 2}, {0, 2}, {-1, 2},
                {-2, 1}, {-2, 0}, {-2, -1},
                {-1, -2}};

        for (int i = 0; i < 16 * 8; ++i) {
            this.bombingAllAreas.put(new IntVector2D(i % 16, i / 16), this.ap);
        }
    }

    @Override
    public IntentType think(ArrayList<Unit> units) {
        this.attackIntent = null;

        if (units.size() == 1) {
            return IntentType.NONE;
        }

        // 전부 폭격 시작
        this.attackIntent = new AttackIntent(this, new IntVector2D(0, 0));
        this.attackIntent.setTargetArea(this.bombingAllAreas);
        return IntentType.ATTACK;
    }

    @Override
    public void onAttacked(int damage) {
        this.hp -= 1;
        this.hp = (this.hp > 0 ? this.hp : 0);
    }

    @Override
    public void onSpawn() {
        SimulationManager.getInstance().registerThinkable(this);
    }

    @Override
    public char getSymbol() {
        return 'D';
    }
}
