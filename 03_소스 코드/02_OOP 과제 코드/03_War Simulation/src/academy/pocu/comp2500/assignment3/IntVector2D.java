package academy.pocu.comp2500.assignment3;

public class IntVector2D {
    private static final int MAX_X = 16;
    private static final int MAX_Y = 8;
    private int x;
    private int y;

    public IntVector2D(int x, int y) {
        // (0, 0) : 맵의 좌상단
        this.x = x;
        this.y = y;
    }

    public int getMaxX() {
        return MAX_X;
    }

    public int getMaxY() {
        return MAX_Y;
    }

    public int getX() {
        return this.x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int lengthToIntVector2D(IntVector2D intVector2D) {
        return (Math.abs(this.x - intVector2D.x) + Math.abs(this.y - intVector2D.y));
    }
}
