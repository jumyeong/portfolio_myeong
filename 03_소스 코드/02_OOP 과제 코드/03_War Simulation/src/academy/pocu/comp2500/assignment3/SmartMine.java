package academy.pocu.comp2500.assignment3;

import java.util.ArrayList;

public class SmartMine extends Mine implements IThinkable {
    private int detectedEnemyNumber;

    public SmartMine(final IntVector2D intVector2D, final int triggerStepCount, final int detectedEnemyNumber) {
        super(intVector2D, triggerStepCount);
        this.ap = 15;
        this.vision = 1;
        this.areaOfEffect = 1;

        this.detectedEnemyNumber = detectedEnemyNumber;

        this.pointOfAttack = new int[][]{{0, 0}};
    }

    @Override
    public IntentType think(ArrayList<Unit> units) {
        detectUnits(units);
        updateAttackableTiles();

        this.attackIntent = null;

        if (this.detectedUnits.size() >= this.detectedEnemyNumber) {
            this.attackIntent = new AttackIntent(this, this.getPosition());
            this.hp = 0;
            return IntentType.ATTACK;
        }

        return IntentType.NONE;
    }

    @Override
    public void onAttacked(int damage) {
        this.hp -= damage;
        this.hp = (this.hp > 0 ? this.hp : 0);
    }

    @Override
    public void onSpawn() {
        SimulationManager.getInstance().registerThinkable(this);
        SimulationManager.getInstance().registerCollisionEventListener(this);
    }

    @Override
    public char getSymbol() {
        return 'A';
    }

}
