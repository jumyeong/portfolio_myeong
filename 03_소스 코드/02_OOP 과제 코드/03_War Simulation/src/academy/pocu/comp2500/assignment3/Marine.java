package academy.pocu.comp2500.assignment3;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Marine extends Unit implements IThinkable, IMovable {

    public Marine(final IntVector2D intVector2D) {
        super(intVector2D);
        this.hp = 35;
        this.ap = 6;
        this.vision = 2;
        this.areaOfEffect = 0;
        this.unitType = UnitType.GROUND;

        this.attackableUnitTypes.add(UnitType.AIR);
        this.attackableUnitTypes.add(UnitType.GROUND);

        this.pointOfAttack = new int[][]{{0, 0}, {0, -1}, {1, 0}, {0, 1}, {-1, 0}};
    }

    @Override
    public IntentType think(ArrayList<Unit> units) {
        detectUnits(units);
        updateAttackableTiles();

        this.attackIntent = null;
        this.nextMovePosition = null;

        // 탐지된게 아무것도 없을 경우,
        if (this.detectedUnits.size() == 0) {
            // 없으면 아무것도 안함
            return IntentType.NONE;
        }

        // 탐지 됨.
        // 공격할 수 있는 타일에 적이 있는 지 여부.
        ArrayList<Unit> attackableEnemies = getAttackableEnemies();

        if (attackableEnemies.size() == 1) {
            this.attackIntent = new AttackIntent(this, attackableEnemies.get(0).getPosition());
            return IntentType.ATTACK;
        } else if (attackableEnemies.size() > 1) {
            this.attackIntent = createAttackIntent(attackableEnemies);
            return IntentType.ATTACK;
        }

        // if (attackableEnemies.size() == 0);
        // 없으면 이동 가능 여부.
        this.nextMovePosition = decideMoveArea();
        return IntentType.MOVE;
    }

    private IntVector2D decideMoveArea() {
        int thisX = this.intVector2D.getX();
        int thisY = this.intVector2D.getY();

        ArrayList<Unit> priorityUnits = this.detectedUnits;
        if (priorityUnits.size() == 1) {
            return targetingMovePriorityDirection(priorityUnits.get(0));
        }

        priorityUnits = compareToShortDistance(this.detectedUnits);
        if (priorityUnits.size() == 1) {
            return targetingMovePriorityDirection(priorityUnits.get(0));
        }

        priorityUnits = compareToMinHp(priorityUnits);
        if (priorityUnits.size() == 1) {
            return targetingMovePriorityDirection(priorityUnits.get(0));
        }

        // 8개 구역 확인
        for (Unit compareUnit : priorityUnits) {
            if (thisY > compareUnit.intVector2D.getY()
                    && thisX == compareUnit.intVector2D.getX()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisY > compareUnit.intVector2D.getY()
                    && thisX < compareUnit.intVector2D.getX()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisX < compareUnit.intVector2D.getX()
                    && thisY == compareUnit.intVector2D.getY()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisX < compareUnit.intVector2D.getX()
                    && thisY < compareUnit.intVector2D.getY()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisY < compareUnit.intVector2D.getY()
                    && thisX == compareUnit.intVector2D.getX()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisY < compareUnit.intVector2D.getY()
                    && thisX > compareUnit.intVector2D.getX()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisX > compareUnit.intVector2D.getX()
                    && thisY == compareUnit.intVector2D.getY()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        for (Unit compareUnit : priorityUnits) {
            if (thisX > compareUnit.intVector2D.getX()
                    && thisY > compareUnit.intVector2D.getY()) {
                return targetingMovePriorityDirection(compareUnit);
            }
        }

        assert (false) : "Impossiable Target Unit Position";
        return new IntVector2D(thisX, thisY);
    }

    @Override
    public void move() {
        this.intVector2D.setX(this.nextMovePosition.getX());
        this.intVector2D.setY(this.nextMovePosition.getY());
    }

    @Override
    public void onAttacked(int damage) {
        this.hp -= damage;
        this.hp = (this.hp > 0 ? this.hp : 0);
    }

    @Override
    public void onSpawn() {
        SimulationManager.getInstance().registerThinkable(this);
    }

    @Override
    public char getSymbol() {
        return 'M';
    }
}
