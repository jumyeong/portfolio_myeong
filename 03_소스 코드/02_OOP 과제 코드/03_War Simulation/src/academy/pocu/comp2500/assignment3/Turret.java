package academy.pocu.comp2500.assignment3;

import java.util.ArrayList;

public class Turret extends Unit implements IThinkable {
    public Turret(final IntVector2D intVector2D) {
        super(intVector2D);
        this.hp = 99;
        this.ap = 7;
        this.vision = 2;
        this.areaOfEffect = 0;
        this.unitType = UnitType.GROUND;

        this.attackableUnitTypes.add(UnitType.AIR);

        this.pointOfAttack = new int[][]{{0, 0}, {0, -1}, {1, -1}, {1, 0}, {1, 1}, {0, 1}, {-1, 1}, {-1, 0}, {-1, -1}};
    }

    @Override
    public IntentType think(ArrayList<Unit> units) {
        detectUnits(units);
        updateAttackableTiles();

        this.attackIntent = null;

        // 탐지된게 아무것도 없을 경우,
        if (this.detectedUnits.size() == 0) {
            return IntentType.NONE;
        }

        // 탐지 됨.
        // 공격할 수 있는 타일에 적이 있는 지 여부.
        ArrayList<Unit> attackableEnemies = getAttackableEnemies();

        if (attackableEnemies.size() == 1) {
            this.attackIntent = new AttackIntent(this, attackableEnemies.get(0).getPosition());
            return IntentType.ATTACK;
        } else if (attackableEnemies.size() > 1) {
            this.attackIntent = createAttackIntent(attackableEnemies);
            return IntentType.ATTACK;
        }

        // if (attackableEnemies.size() == 0)
        return IntentType.NONE;
    }

    @Override
    public void onAttacked(int damage) {
        this.hp -= damage;
        this.hp = (this.hp > 0 ? this.hp : 0);
    }

    @Override
    public void onSpawn() {
        SimulationManager.getInstance().registerThinkable(this);
    }

    @Override
    public char getSymbol() {
        return 'U';
    }
}
