package academy.pocu.comp2500.assignment3;

import java.util.ArrayList;

public final class SimulationManager {
    private static SimulationManager instance;
    private ArrayList<Unit> units = new ArrayList<>();
    private ArrayList<IThinkable> thinkableUnits = new ArrayList<>();
    private ArrayList<IMovable> movableUnits = new ArrayList<>();
    private ArrayList<ICollisionEventListener> collisionEventListenerUnits = new ArrayList<>();

    private SimulationManager() {
    }

    private void removeDeadUnits() {
        ArrayList<Unit> removeUnits = new ArrayList<>();

        for (Unit unit : this.units) {
            if (!unit.isLive()) {
                removeUnits.add(unit);
            }
        }
        if (removeUnits.size() != 0) {
            for (Unit removeUnit : removeUnits) {
                if (this.units.contains(removeUnit)) {
                    this.units.remove(removeUnit);
                }
            }
        }

        ArrayList<IThinkable> removeThinkableUnits = new ArrayList<>();

        for (IThinkable thinkable : this.thinkableUnits) {
            if (!((Unit) thinkable).isLive()) {
                removeThinkableUnits.add(thinkable);
            }
        }
        if (removeThinkableUnits.size() != 0) {
            for (IThinkable removeThinkableUnit : removeThinkableUnits) {
                if (this.thinkableUnits.contains(removeThinkableUnit)) {
                    this.thinkableUnits.remove(removeThinkableUnit);
                }
            }
        }

        ArrayList<ICollisionEventListener> removeCollisionEventListenerUnits = new ArrayList<>();

        for (ICollisionEventListener collisionEventListener : this.collisionEventListenerUnits) {
            if (!((Unit) collisionEventListener).isLive()) {
                removeCollisionEventListenerUnits.add(collisionEventListener);
            }
        }
        if (removeCollisionEventListenerUnits.size() != 0) {
            for (ICollisionEventListener removeCollisionEventListenerUnit : removeCollisionEventListenerUnits) {
                if (this.collisionEventListenerUnits.contains(removeCollisionEventListenerUnit)) {
                    this.collisionEventListenerUnits.remove(removeCollisionEventListenerUnit);
                }
            }
        }
    }

    // 시그내처 변경 X
    public static SimulationManager getInstance() {
        if (instance == null) {
            return (instance = new SimulationManager());
        }
        return instance;
    }

    public ArrayList<Unit> getUnits() {
        if (this.units != null) {
            removeDeadUnits();
        }
        return this.units;
    }

    public void spawn(Unit unit) {
        unit.onSpawn();
        this.units.add(unit);
    }

    // 다음 메서드의 매개변수 자료형은 변경 가능(아래로 3개만)
    public void registerThinkable(IThinkable thinkable) {
        this.thinkableUnits.add(thinkable);
    }

    public void registerMovable(IMovable movable) {
        this.movableUnits.add(movable);
    }

    public void registerCollisionEventListener(ICollisionEventListener listener) {
        this.collisionEventListenerUnits.add(listener);
    }

    public void update() {
        ArrayList<Unit> attackUnits = new ArrayList<>();

        // 각 유닛들이 이번 프레임에서 할 행동(선택지: 공격, 이동, 아무것도 안 함)을 결정
        // 생각할 수 있는 유닛들?
        for (IThinkable thinkable : this.thinkableUnits) {
            IntentType intentType = thinkable.think(this.units);

            switch (intentType) {
                case ATTACK:
                    attackUnits.add((Unit) thinkable);
                    break;

                case MOVE:
                    registerMovable((IMovable) thinkable);
                    break;

                case NONE:
                    break;

                default:
                    assert (false) : "Impossiable IntentType";
                    break;
            }
        }

        // 움직일 수 있는 각 유닛에게 이동할 기회를 줌
        for (IMovable movable : this.movableUnits) {
            movable.move();
        }

        // 이동 후 충돌 처리
        for (ICollisionEventListener collisionEventListener : this.collisionEventListenerUnits) {
            for (Unit unit : this.units) {
                boolean isCollisionEvent = collisionEventListener.collsionEvent(unit);
                if (isCollisionEvent == true
                        && !attackUnits.contains((Unit) collisionEventListener)) {
                    attackUnits.add((Unit) collisionEventListener);
                }
            }
        }

        // 각 유닛에게 공격할 기회를 줌
        // 피해를 입어야 하는 각 유닛에게 피해를 입힘
        for (Unit attacker : attackUnits) {
            AttackIntent attackIntent = attacker.attack();
            attackIntent.attackAreas(this.units);
        }

        // 죽은 유닛들을 모두 게임에서 제거함
        removeDeadUnits();

        // 계속 변경됨
        this.movableUnits.clear();
    }
}
