package academy.pocu.comp2500.assignment3;

import java.util.ArrayList;

public abstract class Unit {
    protected IntVector2D intVector2D;
    protected AttackIntent attackIntent;
    protected UnitType unitType;
    protected ArrayList<UnitType> attackableUnitTypes = new ArrayList<>();
    protected ArrayList<IntVector2D> attackableTiles = new ArrayList<>();
    protected int[][] pointOfAttack;
    protected ArrayList<Unit> detectedUnits = new ArrayList<>();
    protected IntVector2D nextMovePosition;
    protected int vision;
    protected int areaOfEffect;
    protected int ap;
    protected int hp;
    protected boolean invisible = false;

    protected Unit(final IntVector2D intVector2D) {
        this.intVector2D = intVector2D;
    }

    public int getAreaOfEffect() {
        return this.areaOfEffect;
    }

    public int getVision() {
        return this.vision;
    }

    public int getAp() {
        return this.ap;
    }

    public UnitType getUnitType() {
        return this.unitType;
    }

    public ArrayList<UnitType> getAttackableUnitTypes() {
        return this.attackableUnitTypes;
    }

    public boolean isLive() {
        return (this.hp > 0);
    }

    protected void detectUnits(ArrayList<Unit> units) {
        this.detectedUnits.clear();

        int leftX = Math.max(this.intVector2D.getX() - this.vision, 0);
        int rightX = Math.min(this.intVector2D.getX() + this.vision, this.intVector2D.getMaxX() - 1);
        int topY = Math.max(this.intVector2D.getY() - this.vision, 0);
        int bottomY = Math.min(this.intVector2D.getY() + this.vision, this.intVector2D.getMaxY() - 1);

        ArrayList<Integer> rangeX = new ArrayList<>();
        ArrayList<Integer> rangeY = new ArrayList<>();

        while (leftX <= rightX) {
            rangeX.add(leftX);
            ++leftX;
        }
        while (topY <= bottomY) {
            rangeY.add(topY);
            ++topY;
        }

        for (Unit enemyUnit : units) {
            if (!this.equals(enemyUnit)
                    && enemyUnit.invisible == false
                    && this.attackableUnitTypes.contains(enemyUnit.unitType)
                    && rangeX.contains(enemyUnit.intVector2D.getX())
                    && rangeY.contains(enemyUnit.intVector2D.getY())) {
                this.detectedUnits.add(enemyUnit);
            }
        }
    }

    protected void updateAttackableTiles() {
        this.attackableTiles.clear();

        int x = this.intVector2D.getX();
        int y = this.intVector2D.getY();
        int maxX = this.intVector2D.getMaxX();
        int maxY = this.intVector2D.getMaxY();

        int attableX;
        int attableY;

        for (int i = 0; i < this.pointOfAttack.length; ++i) {
            attableX = x;
            attableY = y;

            attableX += this.pointOfAttack[i][0];
            attableY += this.pointOfAttack[i][1];

            if (attableX < 0
                    || attableY < 0
                    || attableX > maxX - 1
                    || attableY > maxY - 1) {
                continue;
            }
            this.attackableTiles.add(new IntVector2D(attableX, attableY));
        }
    }

    protected ArrayList<Unit> getAttackableEnemies() {
        ArrayList<Unit> targetUnits = new ArrayList<>();

        for (Unit detectedUnit : this.detectedUnits) {
            for (IntVector2D attackableTile : this.attackableTiles) {
                if (attackableTile.getX() == detectedUnit.getPosition().getX()
                        && attackableTile.getY() == detectedUnit.getPosition().getY()) {
                    targetUnits.add(detectedUnit);
                }
            }
        }
        return targetUnits;
    }

    protected AttackIntent createAttackIntent(ArrayList<Unit> attackableEnemys) {
        ArrayList<Unit> attackableUnits = compareToMinHp(attackableEnemys);

        if (attackableUnits.size() > 1) {
            for (IntVector2D attackableTile : this.attackableTiles) {
                for (Unit targetUnit : attackableUnits) {
                    if (targetUnit.getPosition().getX() == attackableTile.getX() &&
                            targetUnit.getPosition().getY() == attackableTile.getY()) {
                        return new AttackIntent(this, targetUnit.getPosition());
                    }
                }
            }
            assert (false) : "Impossiable Logic";
        }

        return new AttackIntent(this, attackableUnits.get(0).getPosition());
    }


    protected ArrayList<Unit> compareToShortDistance(ArrayList<Unit> compareUnits) {
        int minDistance = 16 * 8;
        ArrayList<Unit> priorityUnits = new ArrayList<>();
        int distance;

        for (Unit compareUnit : compareUnits) {
            distance = this.intVector2D.lengthToIntVector2D(compareUnit.intVector2D);
            if (minDistance > distance) {
                minDistance = distance;
            }
        }

        for (Unit compareUnit : compareUnits) {
            distance = this.intVector2D.lengthToIntVector2D(compareUnit.intVector2D);
            if (minDistance == distance) {
                priorityUnits.add(compareUnit);
            }
        }
        return priorityUnits;
    }

    protected IntVector2D targetingMovePriorityDirection(Unit targetUnit) {
        int thisX = this.intVector2D.getX();
        int thisY = this.intVector2D.getY();
        int targetUnitX = targetUnit.getPosition().getX();
        int targetUnitY = targetUnit.getPosition().getY();

        if (thisY != targetUnitY) {
            thisY = (thisY > targetUnitY ? thisY - 1 : thisY + 1);
        } else if (thisX != targetUnitX) {
            thisX = (thisX > targetUnitX ? thisX - 1 : thisX + 1);
        }
        return new IntVector2D(thisX, thisY);
    }

    protected ArrayList<Unit> compareToMinHp(ArrayList<Unit> compareUnits) {
        int minHp = 10000;
        ArrayList<Unit> priorityUnits = new ArrayList<>();

        for (Unit compareUnit : compareUnits) {
            if (minHp > compareUnit.hp) {
                minHp = compareUnit.hp;
            }
        }

        for (Unit compareUnit : compareUnits) {
            if (minHp == compareUnit.hp) {
                priorityUnits.add(compareUnit);
            }
        }
        return priorityUnits;
    }

    // 시그내처 변경 X
    public IntVector2D getPosition() {
        return this.intVector2D;
    }

    public int getHp() {
        return this.hp;
    }

    public AttackIntent attack() {
        return this.attackIntent;
    }

    public abstract void onAttacked(int damage);

    public abstract void onSpawn();

    public abstract char getSymbol();
}
