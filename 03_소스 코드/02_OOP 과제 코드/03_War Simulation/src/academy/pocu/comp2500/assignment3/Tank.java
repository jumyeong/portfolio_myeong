package academy.pocu.comp2500.assignment3;

import java.util.ArrayList;

public class Tank extends Unit implements IThinkable, IMovable {
    private boolean siegeMode = false;
    private boolean forward = true;

    public Tank(final IntVector2D intVector2D) {
        super(intVector2D);
        this.hp = 85;
        this.ap = 8;
        this.vision = 3;
        this.areaOfEffect = 1;
        this.unitType = UnitType.GROUND;

        this.attackableUnitTypes.add(UnitType.GROUND);

        this.pointOfAttack = new int[][]{
                {0, -2}, {1, -2},
                {2, -1}, {2, 0}, {2, 1},
                {1, 2}, {0, 2}, {-1, 2},
                {-2, 1}, {-2, 0}, {-2, -1},
                {-1, -2}};
    }

    @Override
    public IntentType think(ArrayList<Unit> units) {
        detectUnits(units);
        updateAttackableTiles();

        this.attackIntent = null;
        this.nextMovePosition = null;

        // 탐지된 게 아무것도 없을 경우,
        if (this.detectedUnits.size() == 0) {
            if (this.siegeMode == true) {
                this.siegeMode = false;
                return IntentType.NONE;
            }
            // 없으면 이동 가능 여부.
            this.nextMovePosition = decideMoveArea();
            return IntentType.MOVE;
        }

        // 탐지 됨.
        // 1. 현재 공성 모드가 아닌 경우 공성 모드로 변경
        if (this.siegeMode == false) {
            this.siegeMode = true;
            return IntentType.NONE;
        }

        ArrayList<Unit> attackableEnemies = getAttackableEnemies();
        if (attackableEnemies.size() == 1) {
            this.attackIntent = new AttackIntent(this, attackableEnemies.get(0).getPosition());
            return IntentType.ATTACK;
        } else if (attackableEnemies.size() > 1) {
            this.attackIntent = createAttackIntent(attackableEnemies);
            return IntentType.ATTACK;
        }

        return IntentType.NONE;
    }

    private IntVector2D decideMoveArea() {
        if (this.siegeMode == true) {
            return this.intVector2D;
        }
        int thisX = this.intVector2D.getX();

        if (thisX == this.intVector2D.getMaxX() - 1) {
            this.forward = false;
        } else if (thisX == 0) {
            this.forward = true;
        }

        thisX += (forward == true ? 1 : -1);

        return new IntVector2D(thisX, this.intVector2D.getY());
    }

    public void move() {
        this.intVector2D.setX(this.nextMovePosition.getX());
        this.intVector2D.setY(this.nextMovePosition.getY());
    }

    @Override
    public void onAttacked(int damage) {
        this.hp -= (this.siegeMode == true ? damage * 2 : damage);
        this.hp = (this.hp > 0 ? this.hp : 0);
    }

    @Override
    public void onSpawn() {
        SimulationManager.getInstance().registerThinkable(this);
    }

    @Override
    public char getSymbol() {
        return 'T';
    }
}
