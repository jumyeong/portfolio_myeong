package academy.pocu.comp2500.assignment1;

import java.util.ArrayList;
import java.util.UUID;

public class Reaction {
    private ArrayList<UUID> userIds = new ArrayList<>();
    private ReactionType reactionType;
    private int count;

    public Reaction(ReactionType reactionType) {
        this.reactionType = reactionType;
    }

    public ArrayList<UUID> getUserIds() {
        return this.userIds;
    }

    public int getCount() {
        return this.count;
    }

    public ReactionType getReactionType() {
        return this.reactionType;
    }

    public void addReactionCount(UUID userId) {
        if (null != findUserIdOrNull(userId)) {
            return;
        }
        this.count += 1;
        this.userIds.add(userId);
    }

    public void subtractReactionCount(UUID userId) {
        if (null != findUserIdOrNull(userId)) {
            return;
        }
        this.count -= 1;
        this.userIds.remove(userId);
    }

    public UUID findUserIdOrNull(UUID userId) {
        for (UUID copyUserId : this.userIds) {
            if (copyUserId.equals(userId)) {
                return copyUserId;
            }
        }
        return null;
    }
}
