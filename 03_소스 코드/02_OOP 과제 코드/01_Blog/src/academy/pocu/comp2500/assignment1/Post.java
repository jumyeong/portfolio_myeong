package academy.pocu.comp2500.assignment1;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;

public class Post {
    private final UUID id;
    private final UUID authorId;
    private String title;
    private String body;
    private final OffsetDateTime createdDateTime;
    private OffsetDateTime modifiedDateTime;

    private HashSet<String> tags = new HashSet<>();
    private ArrayList<Comment> comments = new ArrayList<>();
    private ArrayList<Reaction> reactions = new ArrayList<>();

    public Post(UUID id, UUID authorId, OffsetDateTime createdDateTime, String title, String body) {
        this.id = id;
        this.createdDateTime = createdDateTime;
        this.modifiedDateTime = this.createdDateTime;
        this.authorId = authorId;
        this.title = title;
        this.body = body;
    }

    // getter
    public UUID getAuthorId() {
        return this.authorId;
    }

    public String getTitle() {
        return this.title;
    }

    public UUID getId() {
        return this.id;
    }

    public String getBody() {
        return this.body;
    }

    public OffsetDateTime getCreatedDateTime() {
        return this.createdDateTime;
    }

    public OffsetDateTime getModifiedDateTime() {
        return this.modifiedDateTime;
    }

    public HashSet<String> getTags() {
        return this.tags;
    }

    public ArrayList<Comment> getComments() {
        this.comments.sort((Comment comment1, Comment comment2) -> (comment2.getVotes() - comment1.getVotes()));
        return this.comments;
    }

    public Reaction getReactionOrNull(ReactionType reactionType) {
        return findReactionOrNull(reactionType);
    }

    public ArrayList<Reaction> getReactions() {
        return this.reactions;
    }

    public boolean isTag(String tag) {
        for (String copyTag : this.tags) {
            if (copyTag.equals(tag)) {
                return true;
            }
        }
        return false;
    }

    public void setTitle(UUID authorId, String title) {
        if (!isAuthor(authorId)) {
            return;
        }
        this.title = title;
        setModifiedDateTime();
    }

    public void setBody(UUID authorId, String body) {
        if (!isAuthor(authorId)) {
            return;
        }
        this.body = body;
        setModifiedDateTime();
    }

    private boolean isAuthor(UUID authorId) {
        if (this.authorId.equals(authorId)) {
            return true;
        }
        return false;
    }

    public void addTag(String tag) {
        this.tags.add(tag);
        setModifiedDateTime();
    }

    public void removeTag(String tag) {
        this.tags.remove(tag);
        setModifiedDateTime();
    }

    public void addComment(Comment comment) {
        this.comments.add(comment);
    }

    public void removeComment(Comment comment) {
        this.comments.remove(comment);
    }

    public void addReaction(UUID userId, ReactionType reactionType) {
        Reaction copyReaction = findReactionOrNull(reactionType);

        if (copyReaction == null) {
            copyReaction = new Reaction(reactionType);
            this.reactions.add(copyReaction);
        }
        copyReaction.addReactionCount(userId);
    }

    public void removeReaction(UUID userId, ReactionType reactionType) {
        Reaction copyReaction = findReactionOrNull(reactionType);

        if (copyReaction != null) {
            copyReaction.subtractReactionCount(userId);

            if (copyReaction.getCount() == 0) {
                this.reactions.remove(copyReaction);
            }
        }
    }

    private Reaction findReactionOrNull(ReactionType reactionType) {
        for (Reaction copyReaction : this.reactions) {
            if (copyReaction.getReactionType().equals(reactionType)) {
                return copyReaction;
            }
        }
        return null;
    }

    private void setModifiedDateTime() {
        this.modifiedDateTime = OffsetDateTime.now();
    }
}
