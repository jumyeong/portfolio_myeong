package academy.pocu.comp2500.assignment1;

public enum SortingType {
    CREATED_DATETIME_DESCENDING_ORDER,      // 작성 일시 내림차순
    CREATED_DATETIME_ASCENDING_ORDER,       // 작성 일시 오름차순
    UPDATED_DATETIME_DESCENDING_ORDER,      // 수정 일시 내림차순
    UPDATED_DATETIME_ASCENDING_ORDER,       // 수정 일시 오름차순
    TITLE_ALPHABETICAL_ORDER                // 제목(사전 순서) 오름차순

}
