package academy.pocu.comp2500.assignment1;

import java.util.UUID;

public class User {
    private final UUID id;
    private String name;

    public User(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public UUID getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

}
