package academy.pocu.comp2500.assignment1;

public enum ReactionType {
    REACTION_TYPE_GREAT,
    REACTION_TYPE_SAD,
    REACTION_TYPE_ANGRY,
    REACTION_TYPE_FUN,
    REACTION_TYPE_LOVE
}
