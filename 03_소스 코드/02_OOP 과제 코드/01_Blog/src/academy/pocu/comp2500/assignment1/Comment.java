package academy.pocu.comp2500.assignment1;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.UUID;

public class Comment {
    private final UUID commentId;
    private final UUID authorId;
    private String content;
    private final OffsetDateTime createdDateTime;
    private OffsetDateTime modifiedDateTime;
    private ArrayList<Comment> subcomments = new ArrayList<>();
    private int votes;

    public Comment(UUID commentId, UUID authorId, OffsetDateTime createdDateTime, String content) {
        this.commentId = commentId;
        this.authorId = authorId;
        this.createdDateTime = createdDateTime;
        this.modifiedDateTime = this.createdDateTime;
        this.content = content;
    }

    public UUID getCommentId() {
        return this.commentId;
    }

    public UUID getAuthorId() {
        return this.authorId;
    }

    public String getContent() {
        return this.content;
    }

    public OffsetDateTime getCreatedDateTime() {
        return this.createdDateTime;
    }

    public OffsetDateTime getModifiedDateTime() {
        return this.modifiedDateTime;
    }

    public ArrayList<Comment> getSubcomments() {
        this.subcomments.sort((Comment subcomment1, Comment subcomment2) -> (subcomment2.getVotes() - subcomment1.getVotes()));
        return this.subcomments;
    }

    public int getVotes() {
        return this.votes;
    }

    public void setContent(String content) {
        this.content = content;
        setModifiedDateTime();
    }

    public void addSubcomment(Comment subcomment) {
        this.subcomments.add(subcomment);
    }

    public void upvote() {
        this.votes += 1;
    }

    public void downvote() {
        this.votes -= 1;
    }

    private void setModifiedDateTime() {
        this.modifiedDateTime = OffsetDateTime.now();
    }
}
