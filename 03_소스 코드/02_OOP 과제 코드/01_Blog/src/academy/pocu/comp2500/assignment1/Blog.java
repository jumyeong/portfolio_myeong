package academy.pocu.comp2500.assignment1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.UUID;

// 내부 클래스

// Blog Class
public class Blog {
    private final UUID id;
    private ArrayList<Post> posts = new ArrayList<>();
    private ArrayList<String> filterTags = new ArrayList<>();
    private UUID filterUserId;
    private SortingType sortingType;    // 블로그 글 정렬
    private FilterOption filterOption;  // 블로그의 필터 옵션

    public Blog(UUID id) {
        this.id = id;
        this.sortingType = SortingType.CREATED_DATETIME_DESCENDING_ORDER;   // 내림차순
        this.filterOption = FilterOption.FILTER_OPTION_NONE;
    }

    public UUID getId() {
        return this.id;
    }

    public ArrayList<Post> getPosts() {
        return this.posts;
    }

    public Post getPostOrNull(UUID postId) {
        Post copyPost = findPostOrNull(postId);
        return copyPost;
    }

    public SortingType getSortingType() {
        return this.sortingType;
    }

    public FilterOption getFilterOption() {
        return this.filterOption;
    }

    public void setFilterOptionByTag(String tag) {
        this.filterOption = FilterOption.FILTER_OPTION_TAG;
        for (String copyTag : this.filterTags) {
            if (copyTag.equals(tag)) {
                return;
            }
        }
        this.filterTags.add(tag);
    }

    public void setFilterOptionByUser(User user) {
        if (user == null) {
            unsetFilterOption();
        }
        this.filterOption = FilterOption.FILTER_OPTION_AUTHOR;
        this.filterUserId = user.getId();
    }

    public void unsetFilterOption() {
        this.filterOption = FilterOption.FILTER_OPTION_NONE;
        this.filterTags = new ArrayList<String>();
    }

    public void setSortingType(SortingType sortingType) {
        this.sortingType = sortingType;
    }

    public void addPost(Post post) {
        this.posts.add(post);
    }

    public ArrayList<Post> getPostsAtFilterOrNull() {
        ArrayList<Post> copyPost = new ArrayList<>();

        switch (this.filterOption) {
            case FILTER_OPTION_TAG:
                for (Post post : this.posts) {
                    for (String tag : this.filterTags) {
                        if (post.isTag(tag) == true) {
                            copyPost.add(post);
                            break;
                        }
                    }
                }
                break;
            case FILTER_OPTION_AUTHOR:
                for (Post post : this.posts) {
                    if (post.getAuthorId().equals(this.filterUserId)) {
                        copyPost.add(post);
                    }
                }
                break;
            case FILTER_OPTION_NONE:
                for (Post post : this.posts) {
                    copyPost.add(post);
                }
                break;
            default:
                assert (false) : "Unrecognized filter option: " + this.filterOption;
                break;
        }

        sortingPosts(copyPost);

        return copyPost;
    }

    private void sortingPosts(ArrayList<Post> posts) {
        switch (this.sortingType) {
            case CREATED_DATETIME_DESCENDING_ORDER:
                posts.sort(Comparator.comparing(Post::getCreatedDateTime).reversed());
                break;
            case CREATED_DATETIME_ASCENDING_ORDER:
                posts.sort(Comparator.comparing(Post::getCreatedDateTime));
                break;
            case UPDATED_DATETIME_DESCENDING_ORDER:
                posts.sort(Comparator.comparing(Post::getModifiedDateTime).reversed());
                break;
            case UPDATED_DATETIME_ASCENDING_ORDER:
                posts.sort(Comparator.comparing(Post::getModifiedDateTime));
                break;
            case TITLE_ALPHABETICAL_ORDER:
                posts.sort(Comparator.comparing(Post::getTitle));
                break;
            default:
                assert (false) : "Unrecognized sorting type: " + this.sortingType;
                break;
        }
    }

    private Post findPostOrNull(UUID postId) {
        for (Post post : this.posts) {
            if (post.getId().equals(postId)) {
                return post;
            }
        }
        return null;
    }
}
