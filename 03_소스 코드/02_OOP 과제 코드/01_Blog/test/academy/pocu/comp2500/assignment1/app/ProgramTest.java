package academy.pocu.comp2500.assignment1.app;

import academy.pocu.comp2500.assignment1.Blog;
import academy.pocu.comp2500.assignment1.Comment;
import academy.pocu.comp2500.assignment1.Post;
import academy.pocu.comp2500.assignment1.Reaction;
import academy.pocu.comp2500.assignment1.ReactionType;
import academy.pocu.comp2500.assignment1.SortingType;
import academy.pocu.comp2500.assignment1.User;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.UUID;

class ProgramTest {
    // User Stories
    @Test
    void Test01() {
        // 1.
        UUID id = UUID.randomUUID();
        Blog blog = new Blog(id);

        assert (blog.getId().equals(id));
        assert (blog.getSortingType().equals(SortingType.CREATED_DATETIME_DESCENDING_ORDER));

        assert (blog.getPosts() != null);
        assert (blog.getPosts().size() == 0);

        id = UUID.randomUUID();
        User user = new User(id, "user1");

        assert (user.getId().equals(id));
        assert (user.getName().equals("user1"));

        id = UUID.randomUUID();
        Post post = new Post(id, user.getId(), OffsetDateTime.now(),"t1", "b1");

        assert (post.getId().equals(id));
        assert (post.getAuthorId().equals(user.getId()));
        assert (post.getTitle().equals("t1"));
        assert (post.getBody().equals("b1"));
        assert (post.getCreatedDateTime().equals(post.getModifiedDateTime()));

        assert (post.getComments() != null);
        assert (post.getComments().size() == 0);

        assert (post.getTags() != null);
        assert (post.getTags().size() == 0);

        assert (post.getReactions() != null);
        assert (post.getReactions().size() == 0);

        blog.addPost(post);

        assert (blog.getPosts().size() == 1);
        assert (blog.getPostOrNull(post.getId()) != null);
        assert (blog.getPostOrNull(post.getId()).equals(post));
    }

    @Test
    void Test02() {
        // 2. Blog : AddPost()
        UUID id = UUID.randomUUID();
        Blog blog = new Blog(id);

        id = UUID.randomUUID();
        User user = new User(id,"user1");

        id = UUID.randomUUID();
        Post post1 = new Post(id, user.getId(), OffsetDateTime.now(), "t1", "b1");

        id = UUID.randomUUID();
        Post post2 = new Post(id, user.getId(), OffsetDateTime.now(), "t2", "b2");

        id = UUID.randomUUID();
        Post post3 = new Post(id, user.getId(), OffsetDateTime.now(), "t3", "b3");

        blog.addPost(post1);
        blog.addPost(post2);
        blog.addPost(post3);

        assert (blog.getPosts().size() == 3);
        assert (blog.getPostOrNull(post1.getId()).equals(post1));

        assert (blog.getPostOrNull(post2.getId()).equals(post2));

        assert (blog.getPostOrNull(post3.getId()).equals(post3));
    }

    @Test
    void Test03() {
        // 3. Filter Option : Tag
        UUID id = UUID.randomUUID();
        Blog blog = new Blog(id);

        id = UUID.randomUUID();
        User user = new User(id, "name1");

        id = UUID.randomUUID();
        Post post1 = new Post(id, user.getId(), OffsetDateTime.now(), "t1", "b1");

        id = UUID.randomUUID();
        Post post2 = new Post(id, user.getId(), OffsetDateTime.now(), "t2", "b2");

        id = UUID.randomUUID();
        Post post3 = new Post(id, user.getId(), OffsetDateTime.now(), "t3", "b3");

        blog.addPost(post1);
        blog.addPost(post2);
        blog.addPost(post3);

        post1.addTag("T1");
        post1.addTag("T2");
        post1.addTag("T3");
        post2.addTag("T2");
        post2.addTag("T3");
        post3.addTag("T3");

        blog.setFilterOptionByTag("T1");
        assert (blog.getPostsAtFilterOrNull().size() == 1);

        blog.setFilterOptionByTag("T2");
        assert (blog.getPostsAtFilterOrNull().size() == 2);

        blog.setFilterOptionByTag("T3");
        assert (blog.getPostsAtFilterOrNull().size() == 3);
    }

    @Test
    void Test04() {
        // 4. Filter Option : User
        UUID id = UUID.randomUUID();
        Blog blog = new Blog(id);

        id = UUID.randomUUID();
        User user1 = new User(id, "user1");

        id = UUID.randomUUID();
        User user2 = new User(id, "user2");

        id = UUID.randomUUID();
        Post post1 = new Post(id, user1.getId(), OffsetDateTime.now(), "t1", "b1");

        id = UUID.randomUUID();
        Post post2 = new Post(id, user1.getId(), OffsetDateTime.now(), "t2", "b2");

        id = UUID.randomUUID();
        Post post3 = new Post(id, user2.getId(), OffsetDateTime.now(), "t3", "b3");

        blog.addPost(post1);
        blog.addPost(post2);
        blog.addPost(post3);

        blog.setFilterOptionByUser(user1);
        assert (blog.getPostsAtFilterOrNull().size() == 2);

        blog.setFilterOptionByUser(user2);
        assert (blog.getPostsAtFilterOrNull().size() == 1);
    }

    @Test
    synchronized void Test05() throws InterruptedException {
        // 5. default sorting type : 작성 일시 기준
        UUID id = UUID.randomUUID();
        Blog blog = new Blog(id);

        id = UUID.randomUUID();
        User user1 = new User(id, "user1");

        id = UUID.randomUUID();
        User user2 = new User(id, "user2");

        id = UUID.randomUUID();
        Post post1 = new Post(id, user1.getId(), OffsetDateTime.now(), "t1", "b1");

        wait(10);
        id = UUID.randomUUID();
        Post post2 = new Post(id, user1.getId(), OffsetDateTime.now(), "t2", "b2");

        wait(10);
        id = UUID.randomUUID();
        Post post3 = new Post(id, user2.getId(), OffsetDateTime.now(), "t3", "b3");

        blog.addPost(post1);
        blog.addPost(post2);
        blog.addPost(post3);

        assert (blog.getPostsAtFilterOrNull().get(0).equals(post3));
        assert (blog.getPostsAtFilterOrNull().get(1).equals(post2));
        assert (blog.getPostsAtFilterOrNull().get(2).equals(post1));
    }

    @Test
    void Test06() throws InterruptedException {
        // 6. Sorting Option : 5개
        UUID id = UUID.randomUUID();
        Blog blog = new Blog(id);

        id = UUID.randomUUID();
        User user1 = new User(id, "user1");

        id = UUID.randomUUID();
        User user2 = new User(id, "user2");

        id = UUID.randomUUID();
        Post post1 = new Post(id, user1.getId(), OffsetDateTime.now(), "A1", "b1");
        Thread.sleep(10);

        id = UUID.randomUUID();
        Post post2 = new Post(id, user1.getId(), OffsetDateTime.now(), "B2", "b2");
        Thread.sleep(10);

        id = UUID.randomUUID();
        Post post3 = new Post(id, user2.getId(), OffsetDateTime.now(), "C3", "b3");
        Thread.sleep(10);

        blog.addPost(post1);
        blog.addPost(post2);
        blog.addPost(post3);

        post1.setBody(user1.getId(), "b1_update");

        blog.setSortingType(SortingType.TITLE_ALPHABETICAL_ORDER);
        assert (blog.getPostsAtFilterOrNull().get(0).equals(post1));

        blog.setSortingType(SortingType.CREATED_DATETIME_DESCENDING_ORDER);
        assert (blog.getPostsAtFilterOrNull().get(0).equals(post3));

        blog.setSortingType(SortingType.CREATED_DATETIME_ASCENDING_ORDER);
        assert (blog.getPostsAtFilterOrNull().get(0).equals(post1));

        blog.setSortingType(SortingType.UPDATED_DATETIME_DESCENDING_ORDER);
        assert (blog.getPostsAtFilterOrNull().get(0).equals(post1));

        blog.setSortingType(SortingType.UPDATED_DATETIME_ASCENDING_ORDER);
        assert (blog.getPostsAtFilterOrNull().get(0).equals(post2));
    }

    @Test
    void Test07() throws InterruptedException {
        // 7.
        UUID id = UUID.randomUUID();
        Blog blog = new Blog(id);

        id = UUID.randomUUID();
        User user1 = new User(id, "user1");

        id = UUID.randomUUID();
        User user2 = new User(id, "user2");

        id = UUID.randomUUID();
        Post post1 = new Post(id, user1.getId(), OffsetDateTime.now(), "A1", "b1");
        Thread.sleep(10);

        id = UUID.randomUUID();
        Post post2 = new Post(id, user1.getId(), OffsetDateTime.now(), "B2", "b2");
        Thread.sleep(10);

        blog.addPost(post1);
        blog.addPost(post2);

        Post copyPost = blog.getPostOrNull(post1.getId());
        assert (copyPost.equals(post1));

        copyPost.setTitle(user2.getId(),"A1_update");       // 작성자 X
        assert (!copyPost.getTitle().equals("A1_update"));

        copyPost.setTitle(user1.getId(), "A1_update");      // 작성자 O
        assert (copyPost.getTitle().equals("A1_update"));

        copyPost.setBody(user1.getId(), "b1_update");      // 작성자 O
        assert (copyPost.getBody().equals("b1_update"));
    }

    @Test
    void Test08() throws InterruptedException {
        // 8.
        UUID id = UUID.randomUUID();
        Blog blog = new Blog(id);

        id = UUID.randomUUID();
        User user1 = new User(id, "user1");

        id = UUID.randomUUID();
        User user2 = new User(id, "user2");

        id = UUID.randomUUID();
        Post post1 = new Post(id, user1.getId(), OffsetDateTime.now(), "A1", "b1");
        Thread.sleep(10);

        id = UUID.randomUUID();
        Post post2 = new Post(id, user1.getId(), OffsetDateTime.now(), "B2", "b2");
        Thread.sleep(10);

        blog.addPost(post1);
        blog.addPost(post2);

        Post copyPost = blog.getPostOrNull(post1.getId());
        assert (copyPost.equals(post1));

        copyPost.setTitle(user2.getId(),"A1_update");       // 작성자 X
        assert (!copyPost.getTitle().equals("A1_update"));

        copyPost.setTitle(user1.getId(), "A1_update");      // 작성자 O
        assert (copyPost.getTitle().equals("A1_update"));

        copyPost.setBody(user1.getId(), "b1_update");      // 작성자 O
        assert (copyPost.getBody().equals("b1_update"));
    }

    @Test
    void Test09() throws InterruptedException {
        // 10. 태그 추가 (작성자든 방문자든 상관x)
        UUID id = UUID.randomUUID();
        Blog blog = new Blog(id);

        id = UUID.randomUUID();
        User user1 = new User(id, "user1");

        id = UUID.randomUUID();
        User user2 = new User(id, "user2");

        id = UUID.randomUUID();
        Post post1 = new Post(id, user1.getId(), OffsetDateTime.now(), "A1", "b1");
        Thread.sleep(10);

        id = UUID.randomUUID();
        Post post2 = new Post(id, user1.getId(), OffsetDateTime.now(), "B2", "b2");
        Thread.sleep(10);

        blog.addPost(post1);
        blog.addPost(post2);

        Post copyPost1 = blog.getPostOrNull(post1.getId());

        copyPost1.addTag("T1");
        blog.setFilterOptionByTag("T1");
        assert (blog.getPostsAtFilterOrNull().size() == 1);

        copyPost1.removeTag("T1");
        blog.setFilterOptionByTag("T1");
        assert (blog.getPostsAtFilterOrNull().size() == 0);
    }

    @Test
    void Test10() throws InterruptedException {
        // 11. post 에 달린 모든 comment 가져오기
        UUID id = UUID.randomUUID();
        Blog blog = new Blog(id);

        id = UUID.randomUUID();
        User user1 = new User(id, "user1");

        id = UUID.randomUUID();
        User user2 = new User(id, "user2");

        id = UUID.randomUUID();
        Post post1 = new Post(id, user1.getId(), OffsetDateTime.now(), "A1", "b1");
        Thread.sleep(10);

        id = UUID.randomUUID();
        Post post2 = new Post(id, user1.getId(), OffsetDateTime.now(), "B2", "b2");
        Thread.sleep(10);

        blog.addPost(post1);
        blog.addPost(post2);

        id = UUID.randomUUID();
        Comment comment1 = new Comment(id, user1.getId(), OffsetDateTime.now(), "user1_comment");

        id = UUID.randomUUID();
        Comment comment2 = new Comment(id, user2.getId(), OffsetDateTime.now(), "user2_comment");

        post1.addComment(comment1);
        post1.addComment(comment2);

        assert (blog.getPostOrNull(post1.getId()).getComments().size() == 2);
    }

    @Test
    void Test11() throws InterruptedException {
        // 12. '추천수 - 비추천수 ' 의 결과대로 내림차순 정렬된 댓글 가져오기(가장 인기 있는 댓글 먼저 읽기)
        UUID id = UUID.randomUUID();
        Blog blog = new Blog(id);

        id = UUID.randomUUID();
        User user1 = new User(id, "user1");

        id = UUID.randomUUID();
        User user2 = new User(id, "user2");

        id = UUID.randomUUID();
        Post post1 = new Post(id, user1.getId(), OffsetDateTime.now(), "A1", "b1");
        Thread.sleep(10);

        id = UUID.randomUUID();
        Post post2 = new Post(id, user1.getId(), OffsetDateTime.now(), "B2", "b2");
        Thread.sleep(10);

        blog.addPost(post1);
        blog.addPost(post2);

        id = UUID.randomUUID();
        Comment comment1 = new Comment(id, user1.getId(), OffsetDateTime.now(), "user1_comment");

        id = UUID.randomUUID();
        Comment comment2 = new Comment(id, user2.getId(), OffsetDateTime.now(), "user2_comment");

        id = UUID.randomUUID();
        Comment comment3 = new Comment(id, user2.getId(), OffsetDateTime.now(), "user1_comment_2");

        post1.addComment(comment1);
        post1.addComment(comment2);
        post1.addComment(comment3);

        assert (blog.getPostOrNull(post1.getId()).getComments().size() == 3);

        comment1.upvote();
        comment1.upvote();
        comment1.upvote();

        comment3.downvote();

        comment2.downvote();
        comment2.downvote();

        assert (blog.getPostOrNull(post1.getId()).getComments().get(0).equals(comment1));
        assert (blog.getPostOrNull(post1.getId()).getComments().get(1).equals(comment3));
        assert (blog.getPostOrNull(post1.getId()).getComments().get(2).equals(comment2));

        int beforeVote = comment2.getVotes();

        comment2.upvote();
        comment2.upvote();
        comment2.upvote();
        comment2.upvote();
        comment2.upvote();
        comment2.upvote();

        // vote 수에 따라 sorting update 확인
        assert (blog.getPostOrNull(post1.getId()).getComments().get(0).equals(comment2));

        // vote 개수 변화 확인
        assert (beforeVote != comment2.getVotes());
        assert (blog.getPostOrNull(post1.getId()).getComments().get(2).getVotes() == comment3.getVotes());
        assert (blog.getPostOrNull(post1.getId()).getComments().get(1).getVotes() == comment1.getVotes());
    }

    @Test
    void Test12() throws InterruptedException {
        // 15. 자신이 달았던 댓글을 바꿔 고치기
        UUID id = UUID.randomUUID();
        Blog blog = new Blog(id);

        id = UUID.randomUUID();
        User user1 = new User(id, "user1");

        id = UUID.randomUUID();
        User user2 = new User(id, "user2");

        id = UUID.randomUUID();
        Post post1 = new Post(id, user1.getId(), OffsetDateTime.now(), "A1", "b1");
        Thread.sleep(10);

        id = UUID.randomUUID();
        Post post2 = new Post(id, user1.getId(), OffsetDateTime.now(), "B2", "b2");
        Thread.sleep(10);

        blog.addPost(post1);
        blog.addPost(post2);

        id = UUID.randomUUID();
        Comment comment1 = new Comment(id, user1.getId(), OffsetDateTime.now(), "user1_comment");

        id = UUID.randomUUID();
        Comment comment2 = new Comment(id, user2.getId(), OffsetDateTime.now(), "user2_comment");

        id = UUID.randomUUID();
        Comment comment3 = new Comment(id, user2.getId(), OffsetDateTime.now(), "user2_comment_2");

        post1.addComment(comment1);
        post1.addComment(comment2);
        post1.addComment(comment3);

        Post copyPost = blog.getPostOrNull(post1.getId());
        ArrayList<Comment> copyComments = copyPost.getComments();

        for (Comment copyComment : copyComments) {
            if (copyComment.getAuthorId().equals(user1.getId())) {
                copyComment.setContent("update_user1_comment");
            }
        }

        assert (comment1.getContent().equals("update_user1_comment"));
    }


    @Test
    void Test13() throws InterruptedException {
        // 16. 글에 리앳션 추가 하거나 제거 하기
        UUID id = UUID.randomUUID();
        Blog blog = new Blog(id);

        id = UUID.randomUUID();
        User user1 = new User(id, "user1");

        id = UUID.randomUUID();
        User user2 = new User(id, "user2");

        id = UUID.randomUUID();
        Post post1 = new Post(id, user1.getId(), OffsetDateTime.now(), "A1", "b1");
        Thread.sleep(10);

        id = UUID.randomUUID();
        Post post2 = new Post(id, user1.getId(), OffsetDateTime.now(), "B2", "b2");
        Thread.sleep(10);

        blog.addPost(post1);
        blog.addPost(post2);

        assert (post1.getReactions().size() == 0);

        post1.addReaction(user1.getId(), ReactionType.REACTION_TYPE_ANGRY);

        assert (post1.getReactions().size() == 1);

        post1.addReaction(user1.getId(), ReactionType.REACTION_TYPE_SAD);
        post1.addReaction(user2.getId(), ReactionType.REACTION_TYPE_ANGRY);

        assert (post1.getReactions().size() == 2);
        assert (post1.getReactionOrNull(ReactionType.REACTION_TYPE_ANGRY).getCount() == 2);

        post1.removeReaction(user1.getId(), ReactionType.REACTION_TYPE_ANGRY);

        assert (post1.getReactions().size() == 2);
        assert (post1.getReactionOrNull(ReactionType.REACTION_TYPE_SAD).getCount() == 1);

        post1.addReaction(user1.getId(), ReactionType.REACTION_TYPE_SAD);
        post1.addReaction(user1.getId(), ReactionType.REACTION_TYPE_SAD);

        assert (post1.getReactionOrNull(ReactionType.REACTION_TYPE_SAD).getCount() == 1);
    }
}