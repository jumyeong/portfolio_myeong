package academy.pocu.comp2500.assignment4.app;

import academy.pocu.comp2500.assignment4.Canvas;
import academy.pocu.comp2500.assignment4.ClearCommand;
import academy.pocu.comp2500.assignment4.CommandHistoryManager;
import academy.pocu.comp2500.assignment4.DecreasePixelCommand;
import academy.pocu.comp2500.assignment4.DrawPixelCommand;
import academy.pocu.comp2500.assignment4.FillHorizontalLineCommand;
import academy.pocu.comp2500.assignment4.FillVerticalLineCommand;
import academy.pocu.comp2500.assignment4.ICommand;
import academy.pocu.comp2500.assignment4.IncreasePixelCommand;
import academy.pocu.comp2500.assignment4.ToLowerPixelCommand;
import academy.pocu.comp2500.assignment4.OverdrawAnalyzer;
import academy.pocu.comp2500.assignment4.ToUpperPixelCommand;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedList;

class ProgramTest {
    @Test
    void Test01() {
        OverdrawAnalyzer analyzer = new OverdrawAnalyzer(6, 6);
        CommandHistoryManager manager = new CommandHistoryManager((Canvas) analyzer);

        ArrayList<ICommand> commands = new ArrayList<>();
        commands.add(new ClearCommand());
        commands.add(new FillVerticalLineCommand(1, '.'));
        commands.add(new IncreasePixelCommand(0, 3));
        commands.add(new ToUpperPixelCommand(1, 0));
        commands.add(new FillHorizontalLineCommand(4, 'X'));
        commands.add(new FillHorizontalLineCommand(4, 'V'));
        commands.add(new FillVerticalLineCommand(4, 't'));
        commands.add(new IncreasePixelCommand(4, 2));
        commands.add(new ToLowerPixelCommand(2, 3));
        commands.add(new IncreasePixelCommand(0, 0));
        commands.add(new FillVerticalLineCommand(2, 'm'));
        commands.add(new ToLowerPixelCommand(0, 4));
        commands.add(new ToLowerPixelCommand(1, 0));
        commands.add(new DrawPixelCommand(3, 1, 'o'));
        commands.add(new FillVerticalLineCommand(2, 'y'));
        commands.add(new FillHorizontalLineCommand(1, 'A'));

        for (int i = 0; i < 8; i++) {
            manager.execute(commands.get(i));
        }

        manager.redo();

        for (int i = 8; i < 10; i++) {
            manager.execute(commands.get(i));
        }

        manager.redo();

        manager.execute(commands.get(10));

        manager.undo();

        for (int i = 11; i < 14; i++) {
            manager.execute(commands.get(i));
        }

        manager.undo();

        for (int i = 14; i < 16; i++) {
            manager.execute(commands.get(i));
        }

        System.out.print(analyzer.getDrawing());
        System.out.println(analyzer.getPixelHistory(0, 1)); // 오류 생기는 부분 직접 입력
    }

    @Test
    void Test02() {
        Canvas canvas = new Canvas(20, 10);
        CommandHistoryManager chm = new CommandHistoryManager(canvas);
        DrawPixelCommand c1 = new DrawPixelCommand(1, 2, '1');
        DrawPixelCommand c2 = new DrawPixelCommand(3, 5, '2');

        assert (chm.execute(c1) == true);
        assert (chm.execute(c2) == true);

        assert (chm.undo() == true);
        assert (chm.redo() == true);

        assert (chm.undo() == true);

        canvas.drawPixel(1, 2, '5');

        assert (chm.undo() == false);
        assert (chm.redo() == true);

    }

    @Test
    void Test03() { // 빌드봇 테스트
        ICommand com1 = new DecreasePixelCommand(2, 0);
        ICommand com2 = new DrawPixelCommand(4, 0, '=');

        ICommand com3 = new FillVerticalLineCommand(4, 'Z');
        ICommand com4 = new ToUpperPixelCommand(3, 1);

        ICommand com5 = new ToLowerPixelCommand(2, 2);

        ICommand com6 = new ClearCommand();
        ICommand com7 = new ToUpperPixelCommand(0, 4);

        ICommand com8 = new ToLowerPixelCommand(4, 0);

        ICommand com9 = new FillVerticalLineCommand(2, '%');
        ICommand com10 = new ClearCommand();
        ICommand com11 = new FillVerticalLineCommand(1, 'R');
        ICommand com12 = new ClearCommand();
        ICommand com13 = new ToUpperPixelCommand(4, 2);
        ICommand com14 = new ClearCommand();


        OverdrawAnalyzer analyzer1 = new OverdrawAnalyzer(5, 5);
        CommandHistoryManager manager1 = new CommandHistoryManager(analyzer1);

//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com1);
//            System.out.println("1");
//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com2);
//            System.out.println("2");
//            System.out.println(analyzer1.getDrawing());

        manager1.redo();
//            System.out.println("3");
//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com3);
//            System.out.println("4");
//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com4);
//            System.out.println("5");
//            System.out.println(analyzer1.getDrawing());

        manager1.undo();
//            System.out.println("6");
//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com5);
//            System.out.println("7");
//            System.out.println(analyzer1.getDrawing());

        manager1.undo();
//            System.out.println("8");
//            System.out.println(analyzer1.getDrawing());

        manager1.undo();
//            System.out.println("9");
//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com6);
//            System.out.println("10");
//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com7);
//            System.out.println("11");
//            System.out.println(analyzer1.getDrawing());

        manager1.undo();
//            System.out.println("12");
//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com8);
//            System.out.println("13");
//            System.out.println(analyzer1.getDrawing());

        manager1.undo();
//            System.out.println("14");
//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com9);
//            System.out.println("15");
//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com10);
//            System.out.println("16");
//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com11);
//            System.out.println("17");
//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com12);
//            System.out.println("18");
//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com13);
//            System.out.println("19");
//            System.out.println(analyzer1.getDrawing());

        manager1.execute(com14);
//            System.out.println("20");
//            System.out.println(analyzer1.getDrawing());

//            System.out.println(analyzer1.getPixelHistory(4, 0).toString());
        assert (analyzer1.getPixelHistory(4, 0).toString().equals("[=, Z, =,  ]"));
    }

    @Test
    void Test04() { // 빌드봇 테스트 L16
        ICommand command1 = new DrawPixelCommand(4, 4, 'a');
        ICommand command2 = new ToLowerPixelCommand(0, 0);
        ICommand command3 = new DrawPixelCommand(2, 4, 'n');

        ICommand command4 = new ClearCommand();
        ICommand command5 = new DecreasePixelCommand(4, 3);
        ICommand command6 = new ToUpperPixelCommand(3, 0);
        ICommand command7 = new IncreasePixelCommand(3, 0);

        ICommand command8 = new ClearCommand();
        ICommand command9 = new FillVerticalLineCommand(0, 'g');
        ICommand command10 = new FillVerticalLineCommand(3, 'A');
        ICommand command11 = new DecreasePixelCommand(2, 0);
        ICommand command12 = new ToLowerPixelCommand(1, 2);
        ICommand command13 = new ClearCommand();

        ICommand command14 = new FillHorizontalLineCommand(1, 'J');
        ICommand command15 = new ToLowerPixelCommand(4, 1);
        ICommand command16 = new ToUpperPixelCommand(0, 2);


        OverdrawAnalyzer analyzer1 = new OverdrawAnalyzer(5, 5);
        CommandHistoryManager manager1 = new CommandHistoryManager(analyzer1);

        System.out.println(analyzer1.getDrawing());

        manager1.execute(command1);
        System.out.println("1");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command2);
        System.out.println("2");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command3);
        System.out.println("3");
        System.out.println(analyzer1.getDrawing());

        manager1.redo();
        System.out.println("4");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command4);
        System.out.println("5");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command5);
        System.out.println("6");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command6);
        System.out.println("7");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command7);
        System.out.println("8");
        System.out.println(analyzer1.getDrawing());

        manager1.redo();
        System.out.println("9");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command8);
        System.out.println("10");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command9);
        System.out.println("11");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command10);
        System.out.println("12");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command11);
        System.out.println("13");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command12);
        System.out.println("14");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command13);
        System.out.println("15");
        System.out.println(analyzer1.getDrawing());

        manager1.redo();
        System.out.println("16");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command14);
        System.out.println("17");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command15);
        System.out.println("18");
        System.out.println(analyzer1.getDrawing());

        manager1.execute(command16);
        System.out.println("19");
        System.out.println(analyzer1.getDrawing());

        manager1.undo();
        System.out.println("20");
        System.out.println(analyzer1.getDrawing());

        System.out.println(analyzer1.getOverdrawCount(0, 0)); // 올바른 결과 : 2
        assert (analyzer1.getOverdrawCount(0, 0) == 2);
    }

    @Test
    void Test_05_self() {
        Canvas canvas = new Canvas(5, 5);
        CommandHistoryManager commandHistoryManager = new CommandHistoryManager(canvas);

        ArrayList<ICommand> commands = new ArrayList<>();
        // 1
        System.out.println("[1]");
        commands.add(new DrawPixelCommand(4, 4, 'a'));

        commandHistoryManager.execute(commands.get(0));
        System.out.println(canvas.getDrawing());

        commandHistoryManager.undo();
        System.out.println(canvas.getDrawing());

        commandHistoryManager.redo();
        System.out.println(canvas.getDrawing());

        // 2
        System.out.println("[2]");
        commands.add(new IncreasePixelCommand(4, 4));

        commandHistoryManager.execute(commands.get(1));
        System.out.println(canvas.getDrawing());

        commandHistoryManager.undo();
        System.out.println(canvas.getDrawing());

        commandHistoryManager.redo();
        System.out.println(canvas.getDrawing());

        // 3
        System.out.println("[3]");
        commands.add(new DecreasePixelCommand(4, 4));

        commandHistoryManager.execute(commands.get(2));
        System.out.println(canvas.getDrawing());

        commandHistoryManager.undo();
        System.out.println(canvas.getDrawing());

        commandHistoryManager.redo();
        System.out.println(canvas.getDrawing());

        // 4
        System.out.println("[4]");
        commands.add(new ToUpperPixelCommand(4, 4));

        commandHistoryManager.execute(commands.get(3));
        System.out.println(canvas.getDrawing());

        commandHistoryManager.undo();
        System.out.println(canvas.getDrawing());

        commandHistoryManager.redo();
        System.out.println(canvas.getDrawing());

        // 5
        System.out.println("[5]");
        commands.add(new ToLowerPixelCommand(4, 4));

        commandHistoryManager.execute(commands.get(4));
        System.out.println(canvas.getDrawing());

        commandHistoryManager.undo();
        System.out.println(canvas.getDrawing());

        commandHistoryManager.redo();
        System.out.println(canvas.getDrawing());

        // 6
        System.out.println("[6]");
        commands.add(new FillHorizontalLineCommand(2, 'c'));

        commandHistoryManager.execute(commands.get(5));
        System.out.println(canvas.getDrawing());

        commandHistoryManager.undo();
        System.out.println(canvas.getDrawing());

        commandHistoryManager.redo();
        System.out.println(canvas.getDrawing());

        // 7
        System.out.println("[7]");
        commands.add(new FillVerticalLineCommand(2, 'd'));

        commandHistoryManager.execute(commands.get(6));
        System.out.println(canvas.getDrawing());

        commandHistoryManager.undo();
        System.out.println(canvas.getDrawing());

        commandHistoryManager.redo();
        System.out.println(canvas.getDrawing());

        // 8
        System.out.println("[8]");
        commands.add(new ClearCommand());

        commandHistoryManager.execute(commands.get(7));
        System.out.println(canvas.getDrawing());

        commandHistoryManager.undo();
        System.out.println(canvas.getDrawing());

        commandHistoryManager.redo();
        System.out.println(canvas.getDrawing());
    }

    @Test
    void Test_06_self() {
        OverdrawAnalyzer overdrawAnalyzer = new OverdrawAnalyzer(5, 5);
        CommandHistoryManager commandHistoryManager = new CommandHistoryManager(overdrawAnalyzer);
        ArrayList<ICommand> commands = new ArrayList<>();

        // 1
        System.out.println("[1]");
        commands.add(new DrawPixelCommand(4, 4, 'c'));
        commands.add(new DrawPixelCommand(4, 4, 'd'));
        commands.add(new DrawPixelCommand(4, 4, 'd'));

        // 2
        commandHistoryManager.execute(commands.get(0));
        System.out.println(overdrawAnalyzer.getDrawing());

        // 3
        commandHistoryManager.execute(commands.get(1));
        System.out.println(overdrawAnalyzer.getDrawing());

        // 4
        LinkedList<Character> pixelHistory = overdrawAnalyzer.getPixelHistory(4, 4);
        System.out.println(overdrawAnalyzer.getOverdrawCount(4, 4));

        for (char pixel : pixelHistory) {
            System.out.printf("%c", pixel);

            if (pixel != pixelHistory.get(pixelHistory.size() - 1)) {
                System.out.printf("->");
            } else {
                System.out.println();
            }
        }

        // 5
        commandHistoryManager.execute(commands.get(1));
        System.out.println(overdrawAnalyzer.getDrawing());

        // 6
        pixelHistory = overdrawAnalyzer.getPixelHistory(4, 4);
        System.out.println(overdrawAnalyzer.getOverdrawCount(4, 4));
        for (char pixel : pixelHistory) {
            System.out.printf("%c", pixel);

            if (pixel != pixelHistory.get(pixelHistory.size() - 1)) {
                System.out.printf("->");
            } else {
                System.out.println();
            }
        }


        // 1
        System.out.println("[2]");
        commands.add(new IncreasePixelCommand(4, 4));
        commands.add(new IncreasePixelCommand(4, 4));
        commands.add(new IncreasePixelCommand(4, 4));

        // 2
        commandHistoryManager.execute(commands.get(3));
        System.out.println(overdrawAnalyzer.getDrawing());

        // 3
        commandHistoryManager.execute(commands.get(4));
        System.out.println(overdrawAnalyzer.getDrawing());

        // 4
        pixelHistory = overdrawAnalyzer.getPixelHistory(4, 4);
        System.out.println(overdrawAnalyzer.getOverdrawCount(4, 4));

        for (char pixel : pixelHistory) {
            System.out.printf("%c", pixel);

            if (pixel != pixelHistory.get(pixelHistory.size() - 1)) {
                System.out.printf("->");
            } else {
                System.out.println();
            }
        }

        // 5
        commandHistoryManager.execute(commands.get(5));
        System.out.println(overdrawAnalyzer.getDrawing());

        // 6
        pixelHistory = overdrawAnalyzer.getPixelHistory(4, 4);
        System.out.println(overdrawAnalyzer.getOverdrawCount(4, 4));
        for (char pixel : pixelHistory) {
            System.out.printf("%c", pixel);

            if (pixel != pixelHistory.get(pixelHistory.size() - 1)) {
                System.out.printf("->");
            } else {
                System.out.println();
            }
        }

        // 1
        System.out.println("[3]");
        commands.add(new DecreasePixelCommand(4, 4));
        commands.add(new DecreasePixelCommand(4, 4));
        commands.add(new DecreasePixelCommand(4, 4));

        // 2
        commandHistoryManager.execute(commands.get(6));
        System.out.println(overdrawAnalyzer.getDrawing());

        // 3
        commandHistoryManager.execute(commands.get(7));
        System.out.println(overdrawAnalyzer.getDrawing());

        // 4
        pixelHistory = overdrawAnalyzer.getPixelHistory(4, 4);
        System.out.println(overdrawAnalyzer.getOverdrawCount(4, 4));

        for (char pixel : pixelHistory) {
            System.out.printf("%c", pixel);

            if (pixel != pixelHistory.get(pixelHistory.size() - 1)) {
                System.out.printf("->");
            } else {
                System.out.println();
            }
        }

        // 5
        commandHistoryManager.execute(commands.get(8));
        System.out.println(overdrawAnalyzer.getDrawing());

        // 6
        pixelHistory = overdrawAnalyzer.getPixelHistory(4, 4);
        System.out.println(overdrawAnalyzer.getOverdrawCount(4, 4));
        for (char pixel : pixelHistory) {
            System.out.printf("%c", pixel);

            if (pixel != pixelHistory.get(pixelHistory.size() - 1)) {
                System.out.printf("->");
            } else {
                System.out.println();
            }
        }
    }

    @Test
    void Test_07() {
        OverdrawAnalyzer overdrawAnalyzer1 = new OverdrawAnalyzer(5, 5);
        CommandHistoryManager commandHistoryManager = new CommandHistoryManager(overdrawAnalyzer1);
        commandHistoryManager.execute(new ToUpperPixelCommand(2, 0));
        commandHistoryManager.execute(new ToUpperPixelCommand(0, 1));
        commandHistoryManager.execute(new ToLowerPixelCommand(4, 3));
        commandHistoryManager.execute(new FillHorizontalLineCommand(0, 'e'));
        commandHistoryManager.execute(new DecreasePixelCommand(4, 4));
        commandHistoryManager.execute(new DecreasePixelCommand(0, 1));
        commandHistoryManager.execute(new ToLowerPixelCommand(0, 0));
        commandHistoryManager.execute(new DrawPixelCommand(3, 2, 'Z'));
        commandHistoryManager.execute(new IncreasePixelCommand(3, 4));
        commandHistoryManager.execute(new ToUpperPixelCommand(4, 4));
        commandHistoryManager.execute(new DrawPixelCommand(4, 0, '+'));
        commandHistoryManager.execute(new FillVerticalLineCommand(4, '^'));
        commandHistoryManager.execute(new ToLowerPixelCommand(0, 3));
        commandHistoryManager.execute(new ToLowerPixelCommand(1, 0));
        commandHistoryManager.redo();
        commandHistoryManager.undo();
        commandHistoryManager.execute(new ToLowerPixelCommand(1, 3));
        commandHistoryManager.execute(new FillVerticalLineCommand(3, 'I'));
        commandHistoryManager.execute(new IncreasePixelCommand(4, 4));
        commandHistoryManager.redo();

        System.out.print(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getOverdrawCount() == 19);
    }

    @Test
    void Test_08_self() {
        OverdrawAnalyzer overdrawAnalyzer1 = new OverdrawAnalyzer(5, 5);
        CommandHistoryManager commandHistoryManager = new CommandHistoryManager(overdrawAnalyzer1);

        commandHistoryManager.execute(new ToUpperPixelCommand(2, 0));
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new ToUpperPixelCommand(0, 1));
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new ToLowerPixelCommand(4, 3));
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new FillHorizontalLineCommand(0, 'e'));
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new ClearCommand());
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.undo();
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.redo();
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new DecreasePixelCommand(4, 4));
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new DecreasePixelCommand(0, 1));
        System.out.print(overdrawAnalyzer1.getDrawing());

    }

    @Test
    void Test_09() {
        OverdrawAnalyzer overdrawAnalyzer1 = new OverdrawAnalyzer(30, 25);
        CommandHistoryManager commandHistoryManager = new CommandHistoryManager(overdrawAnalyzer1);

        commandHistoryManager.execute(new FillHorizontalLineCommand(10, 't'));
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new FillHorizontalLineCommand(2, 'K'));
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new DrawPixelCommand(8, 6, '0'));
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new ClearCommand());
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.undo();
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.undo();
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new ClearCommand());
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new DrawPixelCommand(22, 18, 'K'));
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new ClearCommand());
        System.out.print(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.undo();
        System.out.print(overdrawAnalyzer1.getDrawing());
    }

    @Test
    void Test_10() {
        OverdrawAnalyzer overdrawAnalyzer1 = new OverdrawAnalyzer(30, 25);
        CommandHistoryManager commandHistoryManager = new CommandHistoryManager(overdrawAnalyzer1);

        commandHistoryManager.execute(new ToUpperPixelCommand(3, 23));
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.redo();
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new IncreasePixelCommand(27, 0));
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new ClearCommand());
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new FillVerticalLineCommand(19, '9'));
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new ToLowerPixelCommand(14, 1));
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new DrawPixelCommand(11, 12, 'b'));
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new DecreasePixelCommand(7, 12));
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new ClearCommand());
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.redo();
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new DecreasePixelCommand(12, 24));
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new DecreasePixelCommand(0, 2));
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new FillVerticalLineCommand(16, 'P'));
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new IncreasePixelCommand(12, 9));
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new ClearCommand());
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new ClearCommand());
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new FillVerticalLineCommand(8, 'K'));
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.execute(new ClearCommand());
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.undo();
        System.out.println(overdrawAnalyzer1.getDrawing());
        commandHistoryManager.redo();
        System.out.println(overdrawAnalyzer1.getDrawing());
    }

    @Test
    void Test_11() {
        OverdrawAnalyzer overdrawAnalyzer1 = new OverdrawAnalyzer(30, 25);
        CommandHistoryManager commandHistoryManager = new CommandHistoryManager(overdrawAnalyzer1);

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.execute(new FillVerticalLineCommand(16, 'B'));
        System.out.println(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.execute(new FillVerticalLineCommand(11, 'A'));
        System.out.println(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.execute(new FillHorizontalLineCommand(11, 'C'));
        System.out.println(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.execute(new FillHorizontalLineCommand(16, 'D'));
        System.out.println(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.execute(new ClearCommand());
        System.out.println(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.undo();
        System.out.println(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.redo();
        System.out.println(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.undo();
        System.out.println(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.redo();
        System.out.println(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.execute(new ClearCommand());
        System.out.println(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.undo();
        System.out.println(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.undo();
        System.out.println(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.undo();
        System.out.println(overdrawAnalyzer1.getDrawing());

        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
        commandHistoryManager.undo();
        System.out.println(overdrawAnalyzer1.getDrawing());
        assert (overdrawAnalyzer1.getWidth() == 30 && overdrawAnalyzer1.getHeight() == 25);
    }

    @Test
    void Test_12() {
        Canvas canvas = new Canvas(20, 10);
        CommandHistoryManager chm = new CommandHistoryManager(canvas);
        DrawPixelCommand c1 = new DrawPixelCommand(5, 5, 'n');
        DrawPixelCommand c2 = new DrawPixelCommand(5, 5, 'n');

        c1.execute(canvas);
        System.out.println(canvas.getDrawing());
        c2.execute(canvas);
        System.out.println(canvas.getDrawing());
        c2.undo();
        System.out.println(canvas.getDrawing());
        c2.undo(); // ?
        System.out.println(canvas.getDrawing());
    }

    @Test
    void Test_13() {
        Canvas canvas = new Canvas(5, 5);
        CommandHistoryManager chm = new CommandHistoryManager(canvas);
        DrawPixelCommand c1 = new DrawPixelCommand(2, 2, 'n');
        DrawPixelCommand c2 = new DrawPixelCommand(2, 2, 'g');

        c1.execute(canvas);
        System.out.println(canvas.getDrawing());
        c2.execute(canvas);
        System.out.println(canvas.getDrawing());
        c1.undo();
        System.out.println(canvas.getDrawing());
        c1.undo(); // ?
        System.out.println(canvas.getDrawing());
    }
}