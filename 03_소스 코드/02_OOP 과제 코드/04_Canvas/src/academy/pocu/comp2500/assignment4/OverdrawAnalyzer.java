package academy.pocu.comp2500.assignment4;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

public class OverdrawAnalyzer extends Canvas {
    private HashMap<Vector, LinkedList<Character>> pixelsHistory = new HashMap<>();

    public OverdrawAnalyzer(int width, int height) {
        super(width, height);

        for (int y = 0; y < getHeight(); ++y) {
            for (int x = 0; x < getWidth(); ++x) {
                this.pixelsHistory.put(new Vector(x, y), new LinkedList<>());
            }
        }
    }

    private Vector findVectorOrNull(final int x, final int y) {
        for (Vector vector : this.pixelsHistory.keySet()) {
            if (vector.getX() == x && vector.getY() == y) {
                return vector;
            }
        }
        return null;
    }

    private void addPixelHistory(final int x, final int y, final char pixel) {
        Vector copyVector = findVectorOrNull(x, y);

        if (copyVector == null) {
            return;
        }

        for (Map.Entry<Vector, LinkedList<Character>> entry : this.pixelsHistory.entrySet()) {
            if (entry.getKey().equals(copyVector)) {
                entry.getValue().add(pixel);
                break;
            }
        }
    }

    @Override
    public void drawPixel(final int x, final int y, final char pixel) {
        if (super.getPixel(x, y) != pixel) {
            super.drawPixel(x, y, pixel);

            this.addPixelHistory(x, y, pixel);
        }
    }

    @Override
    public void toUpper(int x, int y) {
        char pixel = getPixel(x, y);

        if (pixel > 96 && pixel < 123) {
            this.drawPixel(x, y, (char) (pixel - 32));
        }
    }

    @Override
    public void toLower(int x, int y) {
        char pixel = getPixel(x, y);

        if (pixel > 64 && pixel < 91) {
            this.drawPixel(x, y, (char) (pixel + 32));
        }
    }

    @Override
    public void fillHorizontalLine(int y, char pixel) {
        assert (y >= 0 && y < getHeight()) : "Overflow situation";

        if (pixel < 32 && pixel > 126) {
            return;
        }

        for (int x = 0; x < getWidth(); ++x) {
            this.drawPixel(x, y, pixel);
        }
    }

    @Override
    public void fillVerticalLine(int x, char pixel) {
        assert (x >= 0 && x < getWidth()) : "Overflow situation";

        if (pixel < 32 && pixel > 126) {
            return;
        }

        for (int y = 0; y < getHeight(); ++y) {
            this.drawPixel(x, y, pixel);
        }
    }

    @Override
    public void clear() {
        boolean isCheck = false;
        int checkAllEmpty = 0;

        for (int y = 0; y < getHeight(); ++y) {
            for (int x = 0; x < getWidth(); ++x) {
                if (getPixel(x, y) == ' ') {
                    ++checkAllEmpty;
                } else {
                    isCheck = true;
                    break;
                }
            }
            if (isCheck) {
                break;
            }
        }

        if (checkAllEmpty == getHeight() * getWidth()) {
            return;
        }

        super.clear();
    }

    public LinkedList<Character> getPixelHistory(final int x, final int y) {
        Vector copyVector = findVectorOrNull(x, y);

        LinkedList<Character> copyPixelHistory = null;
        for (Map.Entry<Vector, LinkedList<Character>> entry : this.pixelsHistory.entrySet()) {
            if (entry.getKey().equals(copyVector)) {
                copyPixelHistory = entry.getValue();
            }
        }
        return copyPixelHistory;
    }

    public int getOverdrawCount(final int x, final int y) {
        LinkedList<Character> copyPixelHistory = getPixelHistory(x, y);

        return copyPixelHistory.size();
    }

    public int getOverdrawCount() {
        int overdrawCount = 0;
        LinkedList<Character> copyPixelHistory;
        for (Map.Entry<Vector, LinkedList<Character>> entry : this.pixelsHistory.entrySet()) {
            copyPixelHistory = entry.getValue();
            overdrawCount += copyPixelHistory.size();
        }

        return overdrawCount;
    }
}
