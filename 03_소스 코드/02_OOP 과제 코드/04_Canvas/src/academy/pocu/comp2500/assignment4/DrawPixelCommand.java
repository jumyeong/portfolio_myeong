package academy.pocu.comp2500.assignment4;

public final class DrawPixelCommand implements ICommand {
    private final int x;
    private final int y;
    private final char newCharacter;
    private char oldCharacter;
    private Canvas canvas;
    private boolean twiceCheck = false;
    private int doubleUndoCheck = -1;
    private int doubleRedoCheck = -1;

    public DrawPixelCommand(final int x, final int y, final char character) {
        assert (x >= 0 && y >= 0) : "Impossiable Condition";
        assert (character > 32 && character < 126) : "Over range character";

        this.x = x;
        this.y = y;
        this.newCharacter = character;
    }

    @Override
    public boolean execute(final Canvas canvas) {
        if (this.x < 0 || this.y < 0
                || this.x >= canvas.getWidth()
                || this.y >= canvas.getHeight()
                || this.twiceCheck == true) {
            return false;
        }

        this.canvas = canvas;
        this.oldCharacter = canvas.getPixel(this.x, this.y);

        canvas.drawPixel(this.x, this.y, this.newCharacter);
        this.twiceCheck = true;

        if (canvas.getPixel(this.x, this.y) == this.oldCharacter) {
            this.doubleUndoCheck = 0;
            this.doubleRedoCheck = 0;
        }
        return true;
    }

    @Override
    public boolean undo() {
        if (this.canvas == null
                || this.newCharacter != canvas.getPixel(this.x, this.y)
                || (this.doubleUndoCheck != -1 && this.doubleUndoCheck == 1)) {
            return false;
        }

        this.doubleUndoCheck = 1;
        this.doubleRedoCheck = 0;
        this.canvas.drawPixel(this.x, this.y, this.oldCharacter);
        return true;
    }

    @Override
    public boolean redo() {
        if (this.canvas == null
                || this.oldCharacter != canvas.getPixel(this.x, this.y)
                || (this.doubleRedoCheck != -1 && this.doubleRedoCheck == 1)) {
            return false;
        }

        this.doubleUndoCheck = 0;
        this.doubleRedoCheck = 1;
        this.canvas.drawPixel(this.x, this.y, this.newCharacter);
        return true;
    }
}
