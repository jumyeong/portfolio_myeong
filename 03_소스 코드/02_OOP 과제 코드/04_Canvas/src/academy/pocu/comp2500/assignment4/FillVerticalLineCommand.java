package academy.pocu.comp2500.assignment4;

import java.util.ArrayList;

public final class FillVerticalLineCommand implements ICommand {
    private final int x;
    private final char newCharacter;
    private ArrayList<Character> oldCharacters = new ArrayList<>();
    private Canvas canvas;
    private boolean twiceCheck = false;

    public FillVerticalLineCommand(final int x, final char character) {
        assert (x >= 0) : "Impossiable Condition";
        assert (character > 32 && character < 126) : "Over range character";

        this.x = x;
        this.newCharacter = character;
    }

    @Override
    public boolean execute(final Canvas canvas) {
        if (this.x < 0
                || this.x >= canvas.getWidth()
                || this.twiceCheck == true) {
            return false;
        }

        this.canvas = canvas;

        for (int y = 0; y < this.canvas.getHeight(); ++y) {
            this.oldCharacters.add(canvas.getPixel(this.x, y));
        }

        canvas.fillVerticalLine(this.x, this.newCharacter);
        this.twiceCheck = true;

        return true;
    }

    @Override
    public boolean undo() {
        if (this.canvas == null) {
            return false;
        }

        for (int y = 0; y < this.canvas.getHeight(); ++y) {
            if (this.canvas.getPixel(this.x, y) != this.newCharacter) {
                return false;
            }
        }

        for (int y = 0; y < this.canvas.getHeight(); ++y) {
            this.canvas.drawPixel(this.x, y, this.oldCharacters.get(y));
        }

        return true;
    }

    @Override
    public boolean redo() {
        if (this.canvas == null) {
            return false;
        }

        for (int y = 0; y < this.canvas.getHeight(); ++y) {
            if (this.canvas.getPixel(this.x, y) != this.oldCharacters.get(y)) {
                return false;
            }
        }

        this.canvas.fillVerticalLine(this.x, this.newCharacter);

        return true;
    }
}
