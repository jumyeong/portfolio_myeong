package academy.pocu.comp2500.assignment4;

public final class Vector {
    private final int x;
    private final int y;

    public Vector(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }
}
