package academy.pocu.comp2500.assignment4;

import java.util.ArrayList;

public final class CommandHistoryManager {
    private Canvas canvas;
    private ArrayList<ICommand> commands = new ArrayList<>();
    private int pointer = 0;

    public CommandHistoryManager(final Canvas canvas) {
        assert (canvas != null) : "Fail to call constructor";

        this.canvas = canvas;
    }

    public boolean execute(final ICommand command) {
        if (command.execute(this.canvas)) {
            // 이전 undo 되었던 command 들을 모두 제거 (포토샵 생각해보기)
            while (this.pointer != this.commands.size()) {
                int removeIndex = this.commands.size() - 1;
                this.commands.remove(removeIndex);
            }

            this.commands.add(command);
            ++this.pointer;
            return true;
        }
        return false;
    }

    public boolean canUndo() {
        return (this.commands.isEmpty() || this.pointer < 1 ? false : true);
    }

    public boolean canRedo() {
        return (this.commands.isEmpty() || this.pointer >= this.commands.size() ? false : true);
    }

    public boolean undo() {
        if (canUndo() == false) {
            return false;
        }

        if (this.commands.get(this.pointer - 1).undo()) {
            --this.pointer;
            return true;
        }
        return false;
    }

    public boolean redo() {
        if (canRedo() == false) {
            return false;
        }

        if (this.commands.get(this.pointer).redo()) {
            ++this.pointer;
            return true;
        }
        return false;
    }
}
