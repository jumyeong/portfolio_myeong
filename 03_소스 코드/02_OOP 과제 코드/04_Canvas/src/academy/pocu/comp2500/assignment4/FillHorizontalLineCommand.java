package academy.pocu.comp2500.assignment4;

import java.util.ArrayList;

public class FillHorizontalLineCommand implements ICommand {
    private final int y;
    private char newCharacter;
    private ArrayList<Character> oldCharacters = new ArrayList<>();
    private Canvas canvas;
    private boolean twiceCheck = false;

    public FillHorizontalLineCommand(final int y, final char character) {
        assert (y >= 0) : "Impossiable Condition";
        assert (character > 32 && character < 126) : "Over range character";

        this.y = y;
        this.newCharacter = character;
    }

    @Override
    public boolean execute(final Canvas canvas) {
        if (this.y < 0
                || this.y >= canvas.getHeight()
                || this.twiceCheck == true) {
            return false;
        }

        this.canvas = canvas;

        for (int x = 0; x < this.canvas.getWidth(); ++x) {
            this.oldCharacters.add(canvas.getPixel(x, this.y));
        }

        canvas.fillHorizontalLine(this.y, this.newCharacter);
        this.twiceCheck = true;

        return true;
    }

    @Override
    public boolean undo() {
        if (this.canvas == null) {
            return false;
        }

        for (int x = 0; x < this.canvas.getWidth(); ++x) {
            if (this.canvas.getPixel(x, this.y) != this.newCharacter) {
                return false;
            }
        }

        for (int x = 0; x < this.canvas.getWidth(); ++x) {
            this.canvas.drawPixel(x, this.y, this.oldCharacters.get(x));
        }

        return true;
    }

    @Override
    public boolean redo() {
        if (this.canvas == null) {
            return false;
        }

        for (int x = 0; x < this.canvas.getWidth(); ++x) {
            if (this.canvas.getPixel(x, this.y) != this.oldCharacters.get(x)) {
                return false;
            }
        }

        this.canvas.fillHorizontalLine(this.y, this.newCharacter);

        return true;
    }
}