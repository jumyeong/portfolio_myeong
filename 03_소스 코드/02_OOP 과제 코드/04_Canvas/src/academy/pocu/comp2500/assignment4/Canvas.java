package academy.pocu.comp2500.assignment4;

public class Canvas {
    private final int width;
    private final int height;
    private char[][] pixel;     // [row][col] = [height][width] = [y][x]

    public Canvas(final int width, final int height) {
        assert (width >= 0 && height >= 0) : "Impossiable Condition";

        this.width = width;
        this.height = height;

        this.pixel = new char[this.height][this.width];
        for (int y = 0; y < this.height; ++y) {
            for (int x = 0; x < this.width; ++x) {
                this.pixel[y][x] = ' ';
            }
        }
    }
    // !!!혹시라도 x, y 값이 각각 width, height를 넘을 경우도 고려해야할 경우, 추가적인 작업 필요!!!

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public void drawPixel(final int x, final int y, final char pixel) {
        assert (x >= 0 && y >= 0 && x < this.width && y < this.height) : "Overflow situation";

        if (pixel < 32 && pixel > 126) {
            return;
        }

        this.pixel[y][x] = pixel;
    }

    public char getPixel(final int x, final int y) {
        assert (x >= 0 && y >= 0 && x < this.width && y < this.height) : "Overflow situation";

        return this.pixel[y][x];
    }

    public boolean increasePixel(final int x, final int y) {
        assert (x >= 0 && y >= 0 && x < this.width && y < this.height) : "Overflow situation";

        char pixel = (char) (getPixel(x, y) + 1);

        if (pixel < 32 || pixel > 126) {
            return false;
        }

        drawPixel(x, y, pixel);
        return true;
    }

    public boolean decreasePixel(final int x, final int y) {
        assert (x >= 0 && y >= 0 && x < this.width && y < this.height) : "Overflow situation";

        char pixel = (char) (getPixel(x, y) - 1);

        if (pixel < 32 || pixel > 126) {
            return false;
        }

        drawPixel(x, y, pixel);
        return true;
    }

    public void toUpper(final int x, final int y) {
        assert (x >= 0 && y >= 0 && x < this.width && y < this.height) : "Overflow situation";

        char pixel = getPixel(x, y);

        if (pixel > 96 && pixel < 123) {
            drawPixel(x, y, (char) (pixel - 32));
        } else {
            drawPixel(x, y, pixel);
        }
    }

    public void toLower(final int x, final int y) {
        assert (x >= 0 && y >= 0 && x < this.width && y < this.height) : "Overflow situation";

        char pixel = getPixel(x, y);

        if (pixel > 64 && pixel < 91) {
            drawPixel(x, y, (char) (pixel + 32));
        } else {
            drawPixel(x, y, pixel);
        }
    }

    public void fillHorizontalLine(final int y, final char pixel) {
        assert (y >= 0 && y < this.height) : "Overflow situation";

        if (pixel < 32 && pixel > 126) {
            return;
        }

        for (int x = 0; x < this.width; ++x) {
            drawPixel(x, y, pixel);
        }
    }

    public void fillVerticalLine(final int x, final char pixel) {
        assert (x >= 0 && x < this.width) : "Overflow situation";

        if (pixel < 32 && pixel > 126) {
            return;
        }

        for (int y = 0; y < this.height; ++y) {
            drawPixel(x, y, pixel);
        }
    }

    public void clear() {
        // 캔버스의 모든 픽셀을 지웁니다. => (내 해석) : ' ' 초기화하는 것이라 생각함 [[ 아니라면 변경 필요 ]]
        for (int y = 0; y < this.height; ++y) {
            for (int x = 0; x < this.width; ++x) {
                drawPixel(x, y, ' ');
            }
        }
    }

    public String getDrawing() {
        StringBuilder drawingCanvas = new StringBuilder();

        drawingCanvas.append(borderLine());
        for (int y = 0; y < this.height; ++y) {
            drawingCanvas.append('|');
            for (int x = 0; x < this.width; ++x) {
                drawingCanvas.append(getPixel(x, y));
            }
            drawingCanvas.append('|')
                    .append(System.lineSeparator());
        }
        drawingCanvas.append(borderLine());
        return drawingCanvas.toString();
    }

    private String borderLine() {
        StringBuilder borderLine = new StringBuilder();
        borderLine.append('+');
        for (int x = 0; x < this.width; ++x) {
            borderLine.append('-');
        }
        borderLine.append('+')
                .append(System.lineSeparator());
        return borderLine.toString();
    }
}
