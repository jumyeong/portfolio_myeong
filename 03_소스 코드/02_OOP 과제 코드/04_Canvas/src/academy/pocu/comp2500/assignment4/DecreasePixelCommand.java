package academy.pocu.comp2500.assignment4;

public final class DecreasePixelCommand implements ICommand {
    private final int x;
    private final int y;
    private char newCharacter;
    private char oldCharacter;
    private Canvas canvas;
    private boolean twiceCheck = false;

    public DecreasePixelCommand(final int x, final int y) {
        assert (x >= 0 && y >= 0) : "Impossiable Condition";

        this.x = x;
        this.y = y;
    }

    @Override
    public boolean execute(final Canvas canvas) {
        if (this.x < 0 || this.y < 0
                || this.x >= canvas.getWidth()
                || this.y >= canvas.getHeight()
                || this.twiceCheck == true) {
            return false;
        }

        this.oldCharacter = canvas.getPixel(this.x, this.y);

        if (canvas.decreasePixel(this.x, this.y)) {
            this.canvas = canvas;
            this.newCharacter = canvas.getPixel(this.x, this.y);
            this.twiceCheck = true;
            return true;
        }

        this.oldCharacter = 0;
        return false;
    }

    @Override
    public boolean undo() {
        if (this.canvas == null
                || this.newCharacter != canvas.getPixel(this.x, this.y)) {
            return false;
        }

        this.canvas.drawPixel(this.x, this.y, this.oldCharacter);
        return true;
    }

    @Override
    public boolean redo() {
        if (this.canvas == null
                || this.oldCharacter != canvas.getPixel(this.x, this.y)) {
            return false;
        }

        this.canvas.drawPixel(this.x, this.y, this.newCharacter);
        return true;
    }
}
