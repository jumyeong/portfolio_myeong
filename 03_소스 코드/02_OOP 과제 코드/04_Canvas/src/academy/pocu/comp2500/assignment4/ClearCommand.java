package academy.pocu.comp2500.assignment4;

public class ClearCommand implements ICommand {
    private final static char SPACE_CHARACTER = ' ';
    private Canvas newCanvas;
    private Canvas oldCanvas;
    private boolean twiceCheck = false;

    public ClearCommand() {
    }

    @Override
    public boolean execute(final Canvas canvas) {
        if (this.twiceCheck) {
            return false;
        }

        this.newCanvas = canvas;
        this.oldCanvas = new Canvas(this.newCanvas.getWidth(), this.newCanvas.getHeight());

        for (int y = 0; y < this.newCanvas.getHeight(); ++y) {
            for (int x = 0; x < this.newCanvas.getWidth(); ++x) {
                this.oldCanvas.drawPixel(x, y, this.newCanvas.getPixel(x, y));
            }
        }

        this.newCanvas.clear();
        this.twiceCheck = true;

        return true;
    }

    @Override
    public boolean undo() {
        if (this.newCanvas == null || this.oldCanvas == null) {
            return false;
        }

        for (int y = 0; y < this.oldCanvas.getHeight(); ++y) {
            for (int x = 0; x < this.oldCanvas.getWidth(); ++x) {
                if (this.newCanvas.getPixel(x, y) != SPACE_CHARACTER) {
                    return false;
                }
            }
        }

        for (int y = 0; y < this.oldCanvas.getHeight(); ++y) {
            for (int x = 0; x < this.oldCanvas.getWidth(); ++x) {
                this.newCanvas.drawPixel(x, y, this.oldCanvas.getPixel(x, y));
            }
        }

        return true;
    }

    @Override
    public boolean redo() {
        if (this.newCanvas == null || this.oldCanvas == null) {
            return false;
        }

        for (int y = 0; y < this.newCanvas.getHeight(); ++y) {
            for (int x = 0; x < this.newCanvas.getWidth(); ++x) {
                if (this.newCanvas.getPixel(x, y) != this.oldCanvas.getPixel(x, y)) {
                    return false;
                }
            }
        }

        this.newCanvas.clear();
        return true;
    }
}
