#include "CGAMESPRITE.h"

void CGAMESPRITE::Create(unsigned int _W, unsigned int _H, unsigned int _TransColor, bool _Reverse)
{
	m_TransColor = _TransColor;

	SPRITERECT Rc;

	if (false == _Reverse)
	{
		m_SpriteData.reserve((size_t)_W * (size_t)_H);

		Rc.m_SIZE = CVECTOR(m_pTex->GetW() / _W, m_pTex->GetH() / _H);

		CVECTOR Start = { 0, 0 };

		for (size_t y = 0; y < _H; y++)
		{
			for (size_t x = 0; x < _W; x++)
			{
				Rc.m_POS = Start;

				m_SpriteData.push_back(Rc);
				Start.X += Rc.m_SIZE.X;
			}
			Start.X = 0;
			Start.Y += Rc.m_SIZE.Y;
		}
	}
	else
	/////////////////////////// Change Code /////////////////////////////////////
	{
		m_SpriteData.reserve((size_t)_W * (size_t)_H * 2);

		Rc.m_SIZE = CVECTOR(m_pTex->GetW() / _W, (m_pTex->GetH() / _H));

		CVECTOR Start = { 0, 0 };

		for (size_t y = 0; y < _H; y++)
		{
			for (size_t x = 0; x < _W; x++)
			{
				Rc.m_POS = Start;

				m_SpriteData.push_back(Rc);
				Start.X += Rc.m_SIZE.X;
			}
			
			Start.X = 0;
			Start.Y += Rc.m_SIZE.Y;
		}

		Start.Y -= Rc.m_SIZE.Y;

		for (size_t y = (unsigned int)((double)_H * 2); y > 0; y--)
		{
			for (size_t x = 0; x < _W; x++)
			{
				Rc.m_POS = Start;

				m_SpriteData.push_back(Rc);
				Start.X += Rc.m_SIZE.X;
			}

			Start.X = 0;
			Start.Y -= Rc.m_SIZE.Y;
		}
	}
	/////////////////////////// Change Code /////////////////////////////////////
}
