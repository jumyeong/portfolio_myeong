#pragma once
#include <CGAMESTRING.h>
#include <Windows.h>
#include "CSUBOBJECT.h"

#pragma comment(lib, "msimg32.lib")
// �ǹ� - ms img 32. lib


class CGAMEOBJECT;
class CGAMESCENE;
class CRENDER : public CSUBOBJECT
{
public:
	friend CGAMEOBJECT;
	friend CGAMESCENE;

protected:
	BLENDFUNCTION m_BF;

public:
	bool IsAlpha()
	{
		return m_BF.SourceConstantAlpha < 255;
	}
	void Alpha(unsigned int _Alpha)
	{
		m_BF.SourceConstantAlpha = _Alpha;
	}

public:
	void PushRender();

public:
	virtual void Render();

public:
	CRENDER();
	virtual ~CRENDER();

};

