#pragma once

enum COLGROUP
{
	CG_MAP,
	CG_PLAYER		= 100,
	CG_BULLET		= 800,
	CG_INVENTORY	= 1000,
	CG_MOUSE		= 1200,
};

enum COLORDIR
{
	LT,
	L,
	LB,
	T,
	B,
	RT,
	R,
	RB,
	MAX
};

enum class CAMERAAUTHORITY
{
	CA_PLAYER,
	CA_WEAPON,
	CA_MOUSE,
	CA_FIX,
	CA_MAX
};

enum class WEAPONTYPE
{
	WT_BAZOOKA,
	WT_MAX
};