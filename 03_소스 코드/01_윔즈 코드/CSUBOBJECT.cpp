#include "CSUBOBJECT.h"
#include "CGAMEOBJECT.h"
#include <CGAMEWINDOW.h>
#include "CGAMERES.h"
#include "CGAMESCENE.h"

bool CSUBOBJECT::IsUpdate()
{
	return m_ParentObject->IsUpdate() && CGAMESTATE::IsUpdate();
}

bool CSUBOBJECT::IsLive()
{
	return m_ParentObject->IsLive() && CGAMESTATE::IsLive();
}

bool CSUBOBJECT::IsDeath()
{
	return m_ParentObject->IsDeath() || CGAMESTATE::IsDeath();
}

CVECTOR CSUBOBJECT::RenderPos() const
{
	if (true == m_ParentObject->GetCameraEffect())
	{
		return m_Pos + m_ParentObject->GetPos() - m_ParentObject->GetParentScene()->CameraPos();
	}
	else
	{
		return m_Pos + m_ParentObject->GetPos();
	}
}

void CSUBOBJECT::Debug()
{
	//////////////////// BRUSH & PEN ///////////////////////////

	HDC m_hdc = CGAMERES::Inst().BackBufferDC();

	HBRUSH m_Brush = (HBRUSH)GetStockObject(NULL_BRUSH);
	HBRUSH m_OldBrush = (HBRUSH)SelectObject(m_hdc, m_Brush);

	HPEN m_Pen;
	HPEN m_OldPen;

	if (false == isPenColorNull)
	{
		// ����
		m_Pen = CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
		m_OldPen = (HPEN)SelectObject(m_hdc, m_Pen);
	}
	else
	{
		// ����
		m_Pen = (HPEN)GetStockObject(NULL_PEN);
		m_OldPen = (HPEN)SelectObject(m_hdc, (HGDIOBJ)m_Pen);
	}
	
	CVECTOR ConPos = CGAMEWINDOW::MainWindow()->WindowPosTo3DPos(RenderPos());

	Rectangle(m_hdc,
		ConPos.IntX() - m_Size.HalfIntX(),
		ConPos.IntY() - m_Size.HalfIntY(),
		ConPos.IntX() + m_Size.HalfIntX(),
		ConPos.IntY() + m_Size.HalfIntY());
	SelectObject(m_hdc, m_OldPen);
	DeleteObject(m_Pen);

	SelectObject(m_hdc, m_OldBrush);
	DeleteObject(m_Brush);

	//////////////////// BRUSH & PEN ///////////////////////////

}
