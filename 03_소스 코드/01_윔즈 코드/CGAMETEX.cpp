#include "CGAMETEX.h"
#include <assert.h>
#include <CGAMEWINDOW.h>


CGAMETEX::~CGAMETEX()
{
	DeleteObject(m_OldBitMap);
	DeleteDC(m_Image);
}

void CGAMETEX::Loading()
{
	// 이미지 로드
	HANDLE ImageHandle = LoadImage(nullptr, m_File.PCONST_WCHAR(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	if (nullptr == ImageHandle)
	{
		assert(false);
	}

	m_BitMap = (HBITMAP)ImageHandle;

	// 이미지를 new 한 것과 의미라고 생각하면 됨.
	// (1, 1) 짜리 이미지 들어가있음 (nullptr일 때)
	m_Image = CreateCompatibleDC(nullptr);

	// 이미지 교체 : (1, 1) ---change---> (내가 로드한 이미지)
	m_OldBitMap = (HBITMAP)SelectObject(m_Image, m_BitMap);

	if (nullptr == m_Image) 
	{
		assert(false);
	}

	// bitmap에 대한 정보를 얻어올 수 있는 함수
	GetObject(m_BitMap, sizeof(m_Info), &m_Info);

	m_W = m_Info.bmWidth;
	m_H = m_Info.bmHeight;
}


CVECTOR CGAMETEX::GetColor(int X, int Y)
{
	unsigned int uColor = GetPixel(m_Image, X, Y);
	//SetPixel(m_Image, X, Y, 0x00ffffff);
	return CVECTOR::UintToColor(uColor);
}


void CGAMETEX::Create(const CVECTOR& _Size)
{
	if (0 >= _Size.IntX() || 0 >= _Size.IntY())
	{
		assert(false);
		return;
	}

	m_Image = CreateCompatibleDC(nullptr);

	HANDLE ImageHandle = CreateCompatibleBitmap(CGAMEWINDOW::MainWindow()->WINHDC(), _Size.IntX(), _Size.IntY());

	if (nullptr == ImageHandle)
	{
		assert(false);
	}

	m_BitMap = (HBITMAP)ImageHandle;

	m_OldBitMap = (HBITMAP)SelectObject(m_Image, m_BitMap);

	if (nullptr == m_Image)
	{
		assert(false);
	}

	GetObject(m_BitMap, sizeof(m_Info), &m_Info);

	m_W = m_Info.bmWidth;
	m_H = m_Info.bmHeight;
}
