#include "CGAMEPLAYBACK.h"
#include "CRENDERTEX.h"
#include <CGAMEWINDOW.h>
#include "CGAMETEX.h"
#include "CRENDERANI.h"

CRENDERTEX* CGAMEPLAYBACK::TexRender = nullptr;

CGAMEPLAYBACK::CGAMEPLAYBACK() : SubRender(nullptr), InnerWater(nullptr)
{
}

CGAMEPLAYBACK::~CGAMEPLAYBACK()
{
}

void CGAMEPLAYBACK::Loading()
{
	SetPos({ -800, 850 });

	TexRender = CreateRender<CRENDERTEX>(0, { 0, 0 }, { 0, 0 });
	TexRender->SetTex(L"bg_3645x1563.bmp");
	TexRender->TransColor(255, 255, 255, 0);
	TexRender->ScaleToImageSize();

	SubRender = CreateRender<CRENDERTEX>(10, { 0, 0 }, { 0, 0 });
	SubRender->SetTex(L"mt_3645x163.bmp");
	SubRender->TransColor(255, 255, 255, 0);
	SubRender->ScaleToImageSize();

	InnerWater = CreateRender<CRENDERTEX>(15, { 0, 0 }, { 0, 0 });
	InnerWater->SetTex(L"Innerblue_3646x210.bmp");
	InnerWater->TransColor(255, 255, 255, 0);
	InnerWater->ScaleToImageSize();

	for (int i = 0; i < 30; i++)
	{
		Waters[i] = CreateRender<CRENDERANI>(15, { 128, 24 }, { 0.0f, -0.1f });
		Waters[i]->CreateAni(L"blue.bmp", L"Water" + std::to_wstring(i), 0, 11, 0.1f, true);
	}

	CVECTOR ConPos = { TexRender->GetTex()->GetW(), TexRender->GetTex()->GetH() };
	ConPos = ConPos.HalfVector();
	ConPos.Y = -ConPos.Y;
	TexRender->SetPos(ConPos);

	ConPos.Y -= 705.0f;
	SubRender->SetPos(ConPos);

	ConPos.Y -= 166.0f;
	InnerWater->SetPos(ConPos);

	ConPos.X = 5.0f;
	ConPos.Y += 110.0f;
	for (size_t i = 0; i < 30; i++)
	{
		Waters[i]->SetPos(ConPos);
		Waters[i]->StartAni(L"Water" + std::to_wstring(i));
		ConPos.X += 128.0f;
	}
}
