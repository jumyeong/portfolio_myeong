#pragma once
#include <CGAMEIO.h>
#include <map>
#include "CGAMESPRITE.h"

class CGAMERES
{
public:
	static CGAMERES& Inst()
	{
		static CGAMERES NewInst;
		return NewInst;
	}

private:
	std::map<CGAMESTRING, CGAMETEX*> m_AllTex;
	// 더블 버퍼링
	CGAMETEX* m_BackBuffer;

public:
	void BackBufferCreate(const CVECTOR& _Size);

public:
	inline CGAMETEX* BackBuffer()
	{
		return m_BackBuffer;
	}
	inline HDC BackBufferDC()
	{
		return m_BackBuffer->IMGDC();
	}

	CGAMETEX* FindTex(const CGAMESTRING& _FileName);
	void LoadTex(const CGAMEFILE& _File);

	////////////////////////////////////// Spirte;

private:
	std::map<CGAMESTRING, CGAMESPRITE*> m_AllSprite;

public:
	CGAMESPRITE* FindSprite(const CGAMESTRING& _FileName);
	CGAMESPRITE* CreateSprite(const CGAMESTRING& _TexName, unsigned int _W, unsigned int _H, unsigned int _TransColor, bool _Reverse = false);
	CGAMESPRITE* CreateSprite(const CGAMESTRING& _TexName, const CGAMESTRING& _SpriteName, unsigned int _W, unsigned int _H, unsigned int _TransColor, bool _Reverse = false);

private:
	CGAMERES();
	~CGAMERES();
};

