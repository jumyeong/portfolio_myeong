#include "CPLAYSCENE.h"
#include "LOGICHEADER.h"
#include "CGAMERES.h"
#include <CGAMETIME.h>
#include <map>
#include "CGAMEINPUT.h"
#include "CGAMEPLAYER.h"
#include "CGAMEMAP.h"
#include "CGAMEPLAYBACK.h"
#include "CMOUSEOBJECT.h"
#include "CINVEN.h"
#include "CBAZOOKA.h"
#include "CRENDERTEX.h"
#include <CGAMEWINDOW.h>
#include "CGAMETEX.h"

CPLAYSCENE::CPLAYSCENE() 
	: vecPlayers()
	, m_Inven(nullptr)
	, m_Cursor(nullptr)
	, m_BoomHitCheck(false)
	, m_BoomObject(nullptr)
{
}

CPLAYSCENE::~CPLAYSCENE()
{
}

void CPLAYSCENE::Loading()
{
	CGAMEINPUT::CreateKey(L"CamaraTargetChange", 'P');
	CGAMEINPUT::CreateKey(L"CollisionColorRed", 'O');

	YSortOn(0);

	// Link Collision 
	LinkCol(CG_INVENTORY, CG_MOUSE);
	LinkCol(CG_BULLET, CG_PLAYER);

	// Create Map
	CreateObject<CGAMEPLAYBACK>();
	CreateObject<CGAMEMAP>();

	vecPlayers.reserve(10);

	// 커서
	m_Cursor = CreateObject<CMOUSEOBJECT>();

	// 인벤 
	m_Inven = CreateObject<CINVEN>();
	m_Inven->Off();


	vecPlayers.push_back(CreateObject<CGAMEPLAYER>());
	vecPlayers.push_back(CreateObject<CGAMEPLAYER>());
	vecPlayers.push_back(CreateObject<CGAMEPLAYER>());

	
	vecPlayers[0]->SetPos({ 400, -400 });
	CameraPos(vecPlayers[0]->GetPos());
	vecPlayers[0]->SetTargetPlayer(true);
	vecPlayers[0]->GetPlayerCameraPos(vecPlayers[0]->GetPos());

	vecPlayers[1]->SetPos({ 180, -300 });
	vecPlayers[1]->GetPlayerCameraPos(vecPlayers[1]->GetPos());

	vecPlayers[2]->SetPos({ 600, 0 });
	vecPlayers[2]->GetPlayerCameraPos(vecPlayers[2]->GetPos());

}

void CPLAYSCENE::SceneUpdate()
{
	// 다음 Update, 폭발 개체를 제거
	if (nullptr != m_BoomObject)
	{
		m_BoomObject = nullptr;
	}

	if (true == CGAMEINPUT::Down(L"INVEN"))
	{
		if (false == m_Inven->IsOn())
		{
			CGAMEPLAYER::PlayerCameraAuth = CAMERAAUTHORITY::CA_FIX;
			m_Inven->On();
			m_Cursor->ShowInvenCursor(true);
		}
		else 
		{
			CGAMEPLAYER::PlayerCameraAuth = CAMERAAUTHORITY::CA_PLAYER;
			m_Inven->NullColorCollision();
			m_Inven->Off();
			m_Cursor->ShowInvenCursor(false);
		}
		return;
	}

	if (true == CGAMEINPUT::Down(L"L") || true == CGAMEINPUT::Press(L"L")
		|| true == CGAMEINPUT::Down(L"R") || true == CGAMEINPUT::Press(L"R")
		|| true == CGAMEINPUT::Down(L"JUMP"))
	{
		CGAMEPLAYER::PlayerCameraAuth = CAMERAAUTHORITY::CA_PLAYER;
		m_Inven->NullColorCollision();
		m_Inven->Off();
		m_Cursor->ShowInvenCursor(false);
		return;
	}

#ifdef _DEBUG
	// 인벤의 콜리젼 색 변경버튼
	if (true == CGAMEINPUT::Down(L"CollisionColorRed") && true == m_Inven->IsOn())
	{
		m_Inven->ChangeColorCollision();
		return;
	}
#endif

	if (true == m_Inven->IsOn() && true == CGAMEINPUT::Up(L"SelectingWeapon"))
	{
		m_Inven->NullColorCollision();
		m_Inven->Off();
		m_Cursor->ShowInvenCursor(false);
		return;
	}

	// 인벤의 무기 클릭 On 상태일때,
	if (true == m_Inven->GetClick())
	{
		CGAMEPLAYER::PlayerCameraAuth = CAMERAAUTHORITY::CA_PLAYER;
		for (int i = 0; i < 3; i++)
		{
			if (true == vecPlayers[i]->GetTargetPlayer())
			{
				vecPlayers[i]->SetClickWeaponName(m_Inven->GetClickWeaponName());
				m_Inven->SetClick(false); // 클릭 Off 변경
				break;
			}
		}
	}

	// Boom 여부 확인
	for (int i = 0; i < vecPlayers.size(); i++)
	{
		if (true == vecPlayers[i]->GetTargetPlayer())
		{
			m_BoomObject = vecPlayers[i]->GetPlayerBullet();

			if (nullptr != m_BoomObject
				&& true ==  vecPlayers[i]->GetPlayerBullet()->CheckBoom()
				&& CVECTOR::ZERO != vecPlayers[i]->GetPlayerBullet()->GetBoomPos())
			{
				m_BoomHitCheck = true;
				m_BoomPos = vecPlayers[i]->GetPlayerBullet()->GetBoomPos();
				break;
			}
		}
	}

	// Boom 발생
	if (true == m_BoomHitCheck)
	{
		

		for (int i = 0; i < vecPlayers.size(); i++)
		{
			CVECTOR BoomRangeCheck;
			BoomRangeCheck.X = vecPlayers[i]->GetPos().X - m_BoomPos.X;
			BoomRangeCheck.Y = vecPlayers[i]->GetPos().Y - m_BoomPos.Y;
			
			// 범위가 100.0f 이내일 경우, 그 플레이어는 hit 정보 저장
			if (100.0f >= BoomRangeCheck.LEN())
			{

				vecPlayers[i]->SetHit(true);
				vecPlayers[i]->SetHitType(WEAPONTYPE::WT_BAZOOKA);
				vecPlayers[i]->SetHitPos(m_BoomPos);
			}
		}
		m_BoomHitCheck = false;
	}

}

float CheckTime = 1.0f;
int frame = 0;

void CPLAYSCENE::DebugRender()
{
	CheckTime -= CGAMETIME::DeltaTime();
	if (0 >= CheckTime)
	{
		frame = (int)(1.0f / CGAMETIME::DeltaTime());
		CheckTime = 1.0f;
	}

	SetTextColor(CGAMERES::Inst().BackBufferDC(), RGB(255, 255, 255));
	SetBkColor(CGAMERES::Inst().BackBufferDC(), RGB(0, 0, 0));


	wchar_t Arr[256];
	swprintf_s(Arr, L"Frame : %d", frame);
	TextOut(CGAMERES::Inst().BackBufferDC(), 10, 10, Arr, lstrlen(Arr));
	
	swprintf_s(Arr, L"Player[0] : { %.1f, %.1f }", vecPlayers[0]->GetPos().X, vecPlayers[0]->GetPos().Y);
	TextOut(CGAMERES::Inst().BackBufferDC(), 10, 30, Arr, lstrlen(Arr));
	swprintf_s(Arr, L"Player[1] : { %.1f, %.1f }", vecPlayers[1]->GetPos().X, vecPlayers[1]->GetPos().Y);
	TextOut(CGAMERES::Inst().BackBufferDC(), 10, 50, Arr, lstrlen(Arr));
	swprintf_s(Arr, L"Player[2] : { %.1f, %.1f }", vecPlayers[2]->GetPos().X, vecPlayers[2]->GetPos().Y);
	TextOut(CGAMERES::Inst().BackBufferDC(), 10, 70, Arr, lstrlen(Arr));

	for (int i = 0; i < vecPlayers.size(); i++)
	{
		if (true == vecPlayers[i]->GetTargetPlayer())
		{
			if (nullptr != vecPlayers[i]->GetPlayerBullet())
			{
				swprintf_s(Arr, L"Missile : { %.1f, %.1f }", vecPlayers[i]->GetPlayerBullet()->GetPos().X, vecPlayers[i]->GetPlayerBullet()->GetPos().Y);
				TextOut(CGAMERES::Inst().BackBufferDC(), 10, 90, Arr, lstrlen(Arr));
			}
		}
	}
	if (nullptr != CGAMEMAP::TexRender)
	{
		swprintf_s(Arr, L"Map Size : { %.1f, %.1f }", CGAMEMAP::TexRender->GetSize().X, CGAMEMAP::TexRender->GetSize().Y);
		TextOut(CGAMERES::Inst().BackBufferDC(), 10, 110, Arr, lstrlen(Arr));
	}
	if (nullptr != CGAMEMAP::TexRender)
	{
		swprintf_s(Arr, L"Back Size : { %.1f, %.1f }", CGAMEPLAYBACK::TexRender->GetSize().X, CGAMEPLAYBACK::TexRender->GetSize().Y);
		TextOut(CGAMERES::Inst().BackBufferDC(), 10, 130, Arr, lstrlen(Arr));
	}

	swprintf_s(Arr, L"Cursor : { %.1f, %.1f }", m_Cursor->GetPos().X, m_Cursor->GetPos().Y);
	TextOut(CGAMERES::Inst().BackBufferDC(), 10, 150, Arr, lstrlen(Arr));

	swprintf_s(Arr, L"Camera : { %.1f, %.1f }", CameraPos().X, CameraPos().Y);
	TextOut(CGAMERES::Inst().BackBufferDC(), 10, 170, Arr, lstrlen(Arr));

}

void CPLAYSCENE::ChangeCameraTarget()
{
	for (size_t i = 0; i < vecPlayers.size(); i++)
	{
		if (true == vecPlayers[i]->GetTargetPlayer())
		{
			vecPlayers[i]->SetTargetPlayer(false);
			if (i + 1 < vecPlayers.size())
			{
				vecPlayers[i + 1]->SetTargetPlayer(true);
				vecPlayers[i + 1]->SetPlayerCameraCurPos();
			}
			else
			{
				vecPlayers[0]->SetTargetPlayer(true);
				vecPlayers[0]->SetPlayerCameraCurPos();
			}
			break;
		}
	}
}
