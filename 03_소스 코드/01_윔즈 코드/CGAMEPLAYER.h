#pragma once
#include "CGAMEOBJECT.h"
#include "LOGICHEADER.h"
#include <vector>

enum ACTORDIR
{
	AD_LEFT,
	AD_RIGHT,
	AD_UP,
	AD_DOWN,
};

enum PLAYERSTATE
{
	PS_IDLE,
	PS_FALL,
	PS_MOVE,
	PS_JUMP,
	PS_WEAPON,
	PS_HIT,
	PS_MAX,
};

class CRENDERTEX;
class CBAZOOKA;
class CINVEN;
class CCOL;
class CRENDERANI;
class CRENDERSPRITE;
class CGAMEPLAYER : public CGAMEOBJECT
{
private:
	// static CGAMEPLAYER* m_CurWorm;
	unsigned int Dir;

	// 각도
	CVECTOR m_AimPos;

	std::vector<bool> m_DirCheck;
	std::vector<CGAMESTRING> m_DirString;
	std::vector<CGAMESTRING> m_AniString;

	ACTORDIR m_eDir;
	PLAYERSTATE m_eState;

	CRENDERANI* PlayerAni;

	// Sub Object
	CRENDERANI* Arrow;
	CINVEN* m_Inven;

	CRENDERSPRITE* PlayerRender;
	CRENDERSPRITE* AimPoint;

	CCOL* m_PlayerCol;
	float m_Speed;

	// 사용되고 있는 플레이어 여부
	bool TargetPlayer;
	// 플레이어의 카메라 위치
	CVECTOR* m_PlayerCameraPos;

	bool PosDir[COLORDIR::MAX];

	float m_JumpPower;
	bool m_IsJumping;
	float m_Gravity;
	CVECTOR m_vGravity;
	float m_Accumulate;

	unsigned int EndMoving;

	CRENDERSPRITE* m_AllNumbers[10];

	CRENDERTEX* HpBar;
	int m_HpDigit[3];
	int m_Hp;

	bool IsOpeningWeapon;			// 무기모션을 확인하는 함수
	std::wstring ClickWeaponName;	// 클릭된 무기 이름
	std::wstring CurWeaponName;		// 현재 무기 이름

	float m_Degree;					// 각도
	
	CBAZOOKA* m_Bullet;				// 총알

	CRENDERSPRITE* m_FireGaze[16];	// 발사 게이지
	float GazePower;

	bool InWater;

	bool MouseMoveStart;
	bool MouseMoveEnd;

	CVECTOR TempMousePos;

	////////////////////////////////////////////

	CRENDERSPRITE* PlayerHitRender;

	bool isHit;
	WEAPONTYPE HitType;
	CVECTOR m_HitDir;
	bool CheckHitMap;

	float Dmg;
	float HitPower;

public:
	static CAMERAAUTHORITY PlayerCameraAuth;

// FSM을 쓰면 정말정말 함수가 많아지게 됩니다.
// 정리가 된 상태로 많아지는것 뿐이라
// 한가지 더 말하자면
// 상태 마다 함수를 만드는 것이다.
public:
	void ChangeState(PLAYERSTATE _Change, CGAMESTRING _AddStr = L"" , bool _NotAni = false);
	void IDLE();
	void FALL();
	void WATERFALL();

	void MOVE();
	void JUMP();
	void WEAPON();
	void HIT();

public:
	void DirCheck();
	
public:
	bool GroundCheck(); // (= BottomCheck)
	bool TopCheck();
	bool TopRightCheck();
	bool RightCheck();
	bool TopLeftCheck();
	bool LeftCheck();

public:
	// 확인 함수
	bool PreHillCheck(int _X, int _Y = 13);
	bool PreGroundCheck(int _X, int _Y = 13);
	void GroundPosSetting();

	void DirColorCheck(int _X = 7, int _Y = 13);

public:
	inline void SetTargetPlayer(bool _IsTarget)
	{
		TargetPlayer = _IsTarget;
	}
	inline bool GetTargetPlayer()
	{
		return TargetPlayer;
	}

public:
	// 카메라 위치 
	inline void GetPlayerCameraPos(CVECTOR _SceneCameraPos)
	{
		m_PlayerCameraPos = &_SceneCameraPos;
	}
	void SetPlayerCameraCurPos();
	void SetPlayerCameraMove(const ACTORDIR _DirStr, const float _Scale = 1.0f);


	// 무기 이름
public:
	inline void SetClickWeaponName(std::wstring _WeaponName) {	ClickWeaponName = _WeaponName; }

public:
	// 체력 숫자 배열로 바꾸는 함수

	void HpRenderOn();
	void HpRenderOff();
	void HpRender();

	inline void HpChangeDigit()
	{
		int num = m_Hp;

		for (int i = 0; 0 < num ; ++i)
		{
			m_HpDigit[i] = num % 10;
			num /= 10;
		}

		// 자리수가 높은 수부터 0이 있다면 -1로 변경(-1은 이미지를 출력하지 않은 숫자)
		for (int i = 0; i < 3; i++)
		{
			if (0 == m_HpDigit[3 - i - 1] && i != 2 )
			{
				m_HpDigit[3 - i - 1] = -1;
			}
			else
			{
				break;
			}
		}
	}
	

public:
	void GazePowerOff();
	void GazePowerOn();


public:
	CBAZOOKA* GetPlayerBullet();

	// 히트 함수들
public:
	void SetHit(bool _Hit)
	{
		isHit = _Hit;
	}
	void SetHitPos(CVECTOR _HitPos)
	{
		CVECTOR HitPos = _HitPos;
		m_HitDir = GetPos() - HitPos;

		m_HitDir.UnitVector();
		m_HitDir *= HitPower;

		// 폭발 반경
		float BoomRange = 0.0f;

		switch (HitType)
		{
		case WEAPONTYPE::WT_BAZOOKA:
			BoomRange = 100.0f;
			Dmg = 45.f;
			break;
		case WEAPONTYPE::WT_MAX:
			break;
		default:
			BoomRange = 1.0f;
			Dmg = 1.0f;
			break;
		}

		float HitLen = CVECTOR{ GetPos().X - HitPos.X, GetPos().Y - HitPos.Y }.LEN();
		float DmgPercent = Dmg / BoomRange;

		if (0.0f >= HitLen)
		{
			HitLen = 1.0f;
		}

		Dmg -= DmgPercent * HitLen;

		if (0.0f >= Dmg )
		{
			Dmg = 1.0f;
		}
		
		//HitPower = Dmg * 10.0f;
	}
	void SetHitType(WEAPONTYPE _WeaponType)
	{
		HitType = _WeaponType;
	}
	
public:
	int SpriteNum(CVECTOR _Next, CVECTOR _Cur);


public:
	void ChangeSpriteAni();

public:
	void Loading() override;
	void Update() override;

public:
	void ColEnter(CCOL* _this, CCOL* _Other) override;
	void ColStay(CCOL* _this, CCOL* _Other) override;
	void ColExit(CCOL* _this, CCOL* _Other) override;

public:
	CGAMEPLAYER();
	virtual ~CGAMEPLAYER();
};

