#pragma once
#include <CGAMEMATH.h>
#include <list>
#include <map>
#include <set>
#include <string>
#include <assert.h>

class CCOL;
class CRENDER;
class CGAMEOBJECT;
class CGAMESCENE
{
private:
	static bool IsDebug;

public:
	static bool END_SIGN;

public:
	static void DebugOn()
	{
		IsDebug = true;
	}
	static void DebugOff()
	{
		IsDebug = false;
	}

private:
	class CGAMESCENESTARTER
	{
	public:
		CGAMESCENESTARTER()
		{
		}
		~CGAMESCENESTARTER()
		{
			AllSceneDestory();
		}
	};

	static CGAMESCENESTARTER Starter;
	static void AllSceneDestory();
	//////////////////////// member
	// Scene
	// : 한 번에 한 장면만 가능
	// ( 맵 사용)
	// 1) 사물이 있을 것이다.
	// 2) 사물을 리스트로 관리할 것이다.(삽입/삭제 용이, 사물 제거 가능)

	friend CCOL;
	friend CGAMEOBJECT;
	friend CRENDER;

public:
	static void SceneProgress();

private:
	static std::map<std::wstring, CGAMESCENE*> m_AllSceneMap;
	static CGAMESCENE* m_CurScene;
	static CGAMESCENE* m_NextScene;

public:
	// 템플릿은 헤더에서만 구현하자.
	// : .cpp 파일에서도 할 수 있지만 템플릿을 쓰는 이유가 없어진다.
	// 가능한 자료형에 대한 함수를 또 전부 만들어야하기 때문에
	template <typename T>
	static void GameSceneCreate(const wchar_t* _Name)
	{
		// 같은 이름의 씬 일 경우,
		if (nullptr != FindScene(_Name))
		{
			assert(false);
		}
		// 씬 생성
		CGAMESCENE* NewScene = new T();

		m_AllSceneMap.insert(std::map<std::wstring, CGAMESCENE*>::value_type(_Name, NewScene));
	}
	static CGAMESCENE* FindScene(const wchar_t* _Name);
	
	static void ChangeScene(const wchar_t* _Name);

	////////////////////////////////member
	// Scene을 변경하는 방법
	// 두가지 방법
	// 콜백    => 함수포인터
	// virtual => 함수포인터

private:
	// 로딩 여부 확인
	bool IsLoading;
	std::list<CGAMEOBJECT*> m_AllObjectList;


public:
	// 씬에서 오브젝트를 만들어주는 기능
	template <typename T>
	T* CreateObject()
	{
		T* NewObject = new T();

		NewObject->m_pParentScene = this;
		NewObject->Loading();
		m_AllObjectList.push_back(NewObject);

		return NewObject;
	}

protected:
	// 순수 가상함수
	virtual void Loading() = 0;
	virtual void SceneUpdate() = 0;
	virtual void Progress();


	virtual void DebugRender() {}

	//////////////////////////// Render

private:
	// map : 이진트리( 레드블랙트리 )
	std::map<int, std::list<CRENDER*>> m_AllRender;
	std::set<int> YSort;
	
public:
	static bool YSortFunc(const CRENDER* _Left, const CRENDER* _Right);
	
	void YSortOn(int _Order)
	{
		YSort.insert(_Order);
	}

private:
	void PushRender(CRENDER* _Renderer);

	///////////////////////////////////// Col

private:
	std::map<int, std::list<CCOL*>> m_AllCol;
	std::map<int, std::set<int>> m_LinkData;

public:
	// Col 의 관계를 나타내주는 함수
	void LinkCol(int _Left, int _Right) 
	{
		std::map<int, std::set<int>>::iterator LeftIter = m_LinkData.find(_Left);

		// LeftIter에 _Left의 관계가 있다면,
		if (LeftIter != m_LinkData.end())
		{
			assert(false);
		}
		else
		{  
			// LeftIter에 _Left의 관계가 없으면 추가
			m_LinkData.insert(std::map<int, std::set<int>>::value_type(_Left, std::set<int>()));

			LeftIter = m_LinkData.find(_Left);
			m_AllCol.insert(std::map<int, std::list<CCOL*>>::value_type(_Left, std::list<CCOL*>()));
		}
		
		std::set<int>::iterator RightIter = LeftIter->second.find(_Right);

		// _Left에 관계된 _Right 관계가 있다면,
		if (RightIter != LeftIter->second.end())
		{
			assert(false);
		}
		else
		{
			LeftIter->second.insert(_Right);
			m_AllCol.insert(std::map<int, std::list<CCOL*>>::value_type(_Right, std::list<CCOL*>()));
		}
	}
private:
	void PushCol(CCOL* _Col);

	//////////////////////////////////////// Camera Pos
private:
	CVECTOR m_CameraPos;

public:
	CVECTOR CameraPos()
	{
		return m_CameraPos;
	}
	void CameraPos(const CVECTOR& _CameraPos)
	{
		m_CameraPos = _CameraPos;

		if (-140 > m_CameraPos.X)
		{
			m_CameraPos.X = -140;
		}
		if (1850 < m_CameraPos.X)
		{
			m_CameraPos.X = 1850;
		}
		if (-400 > m_CameraPos.Y)
		{
			m_CameraPos.Y = -400;
		}
		if (240 < m_CameraPos.Y)
		{
			m_CameraPos.Y = 240;
		}
	}
	void CameraPosMove(const CVECTOR& _CameraPos)
	{
		m_CameraPos += _CameraPos;

	}
	
	// 카메라를 잡고 있는 오브젝트를 변경하는 함수
	virtual void ChangeCameraTarget()	{ }
	//////////////////////////////////////// Camera Pos


public:
	void Update();
	void Render();
	void Col();
	void Release();
	void Debug();

protected:
	CGAMESCENE();
	virtual ~CGAMESCENE();

};

