#pragma once
#include "CGAMEOBJECT.h"

class CRENDERANI;
class CRENDERTEX;
class CGAMEPLAYBACK : public CGAMEOBJECT
{
public:
	static CRENDERTEX* TexRender;

	CRENDERTEX* InnerWater;
	CRENDERTEX* SubRender;

	CRENDERANI* Waters[30];

public:
	void Loading() override;

public:
	CGAMEPLAYBACK();
	virtual ~CGAMEPLAYBACK();
};

