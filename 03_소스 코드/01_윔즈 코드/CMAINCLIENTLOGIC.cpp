#include "CMAINCLIENTLOGIC.h"
#include <CGAMEWINDOW.h>
#include <CGAMETIME.h>
#include <CGAMEMATH.h>
#include <CGAMEINPUT.h>
#include "CTITLESCENE.h"
#include "CPLAYSCENE.h"
#include "CENDINGSCENE.h"
#include <CGAMEIO.h>
#include "CGAMERES.h"
#include <list>
#include "CGAMEPLAYER.h"
#include <CGAMESOUND.h>

int __stdcall CMAINCLENTLOGIC::MainStart()
{
	{
		CGAMESOUND::Init();
		CGAMEDIRECOTRY SDir;
		SDir.MoveParent(L"MYGAMESOLUTION");
		SDir.Move(L"BIN\\RES\\Sound");

		std::list<CGAMEFILE> m_AllFile = SDir.DirAllFile(L"wav");
		std::list<CGAMEFILE>::iterator StartFile = m_AllFile.begin();
		std::list<CGAMEFILE>::iterator EndFile = m_AllFile.end();

		for (; StartFile != EndFile; ++StartFile)
		{
			CGAMESOUND::Load(*StartFile);
		}
	}

	CGAMERES::Inst().BackBufferCreate(CGAMEWINDOW::MainWindow()->GetSize());
	CGAMETIME::TimeReset();


	CGAMEDIRECOTRY Dir;
	Dir.MoveParent(L"MYGAMESOLUTION");
	Dir.Move(L"BIN\\RES\\Image");

	std::list<CGAMEFILE> m_AllFile = Dir.DirAllFile(L"bmp");

	std::list<CGAMEFILE>::iterator StartFile = m_AllFile.begin();
	std::list<CGAMEFILE>::iterator EndFile = m_AllFile.end();

	for (; StartFile != EndFile; ++StartFile)
	{
		CGAMERES::Inst().LoadTex(*StartFile);
	}
	/////////////// 웜즈 모션들
	
	// default 모션
	CGAMERES::Inst().CreateSprite(L"wbrth1Left.bmp", 1, 20, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);
	CGAMERES::Inst().CreateSprite(L"wbrth1Right.bmp", 1, 20, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);

	// 랜덤 모션들
	//CGAMERES::Inst().CreateSprite(L"RightPlayer.bmp", 1, 36, CVECTOR::ColorToUintInt8(255, 255, 255, 0));
	//CGAMERES::Inst().CreateSprite(L"LeftPlayer.bmp", 1, 36, CVECTOR::ColorToUintInt8(255, 255, 255, 0));
	//CGAMERES::Inst().CreateSprite(L"RightMotion01.bmp", 1, 6, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);
	//CGAMERES::Inst().CreateSprite(L"LeftMotion01.bmp", 1, 6, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);

	// 떨어짐
	CGAMERES::Inst().CreateSprite(L"L_wfall.bmp", 1, 2, CVECTOR::ColorToUintInt8(255, 255, 255, 0));
	CGAMERES::Inst().CreateSprite(L"R_wfall.bmp", 1, 2, CVECTOR::ColorToUintInt8(255, 255, 255, 0));

	// 점프
	CGAMERES::Inst().CreateSprite(L"L_wflylnk.bmp", 1, 7, CVECTOR::ColorToUintInt8(255, 255, 255, 0));
	CGAMERES::Inst().CreateSprite(L"R_wflylnk.bmp", 1, 7, CVECTOR::ColorToUintInt8(255, 255, 255, 0));


	// 걷기
	CGAMERES::Inst().CreateSprite(L"L_wwalk1.bmp", 1, 15, CVECTOR::ColorToUintInt8(255, 255, 255, 0));
	CGAMERES::Inst().CreateSprite(L"R_wwalk1.bmp", 1, 15, CVECTOR::ColorToUintInt8(255, 255, 255, 0));


	// 데미지 받는 모션
	// Hit Fly (SPRITE)
	CGAMERES::Inst().CreateSprite(L"L_wfly1.bmp", 1, 32, CVECTOR::ColorToUintInt8(255, 255, 255, 0));
	CGAMERES::Inst().CreateSprite(L"R_wfly1.bmp", 1, 32, CVECTOR::ColorToUintInt8(255, 255, 255, 0));


	// Hit 미끄러짐 (ANI)
	CGAMERES::Inst().CreateSprite(L"L_wslideu.bmp", 1, 3, CVECTOR::ColorToUintInt8(255, 255, 255, 0));
	CGAMERES::Inst().CreateSprite(L"R_wslideu.bmp", 1, 3, CVECTOR::ColorToUintInt8(255, 255, 255, 0));

	// Hit 미끄러짐 후, Idle 상태 전환 (ANI)
	CGAMERES::Inst().CreateSprite(L"L_wsldlk1.bmp", 1, 22, CVECTOR::ColorToUintInt8(255, 255, 255, 0));
	CGAMERES::Inst().CreateSprite(L"R_wsldlk1.bmp", 1, 22, CVECTOR::ColorToUintInt8(255, 255, 255, 0));


	// Hit 떨어진 후, Idle 상태 전환 (ANI)
	CGAMERES::Inst().CreateSprite(L"L_wtwang.bmp", 1, 49, CVECTOR::ColorToUintInt8(255, 255, 255, 0));
	CGAMERES::Inst().CreateSprite(L"R_wtwang.bmp", 1, 49, CVECTOR::ColorToUintInt8(255, 255, 255, 0));




	///////////////////// 무기 모션들 /////////////////////

	// Bazooka 모션
	CGAMERES::Inst().CreateSprite(L"L_wbazlnk.bmp", 1, 7, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);
	CGAMERES::Inst().CreateSprite(L"R_wbazlnk.bmp", 1, 7, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);

	CGAMERES::Inst().CreateSprite(L"L_wbaz.bmp", 1, 32, CVECTOR::ColorToUintInt8(255, 255, 255, 0));
	CGAMERES::Inst().CreateSprite(L"R_wbaz.bmp", 1, 32, CVECTOR::ColorToUintInt8(255, 255, 255, 0));


	// Homing Missile 모션
	CGAMERES::Inst().CreateSprite(L"L_wbz2lnk.bmp", 1, 7, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);
	CGAMERES::Inst().CreateSprite(L"R_wbz2lnk.bmp", 1, 7, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);

	// Grenade 모션
	CGAMERES::Inst().CreateSprite(L"L_wgrnlnk.bmp", 1, 10, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);
	CGAMERES::Inst().CreateSprite(L"R_wgrnlnk.bmp", 1, 10, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);
	
	// Air Strike 모션
	CGAMERES::Inst().CreateSprite(L"L_wairlnk.bmp", 1, 10, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);
	CGAMERES::Inst().CreateSprite(L"R_wairlnk.bmp", 1, 10, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);

	// Holy Grenade 모션
	CGAMERES::Inst().CreateSprite(L"L_whgrlnk.bmp", 1, 10, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);
	CGAMERES::Inst().CreateSprite(L"R_whgrlnk.bmp", 1, 10, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);

	// Mine 모션
	CGAMERES::Inst().CreateSprite(L"L_wminlnk.bmp", 1, 10, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);
	CGAMERES::Inst().CreateSprite(L"R_wminlnk.bmp", 1, 10, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);

	// Shotgun 모션
	CGAMERES::Inst().CreateSprite(L"L_wshglnk.bmp", 1, 10, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);
	CGAMERES::Inst().CreateSprite(L"R_wshglnk.bmp", 1, 10, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);

	// Teleport 모션
	CGAMERES::Inst().CreateSprite(L"L_wtellnk.bmp", 1, 10, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);
	CGAMERES::Inst().CreateSprite(L"R_wtellnk.bmp", 1, 10, CVECTOR::ColorToUintInt8(255, 255, 255, 0), true);

	///////////////////////////////////////////////////////////////

	// 무기 Sprite
	CGAMERES::Inst().CreateSprite(L"missile.bmp", 1, 32, CVECTOR::ColorToUintInt8(255, 255, 255, 0));


	/////////////////////////////////////////////
	// etc
	// 커서
	CGAMERES::Inst().CreateSprite(L"cursorb.bmp", 1, 32, CVECTOR::ColorToUintInt8(255, 255, 255, 0));

	// 숫자  (바탕 검정색)
	CGAMERES::Inst().CreateSprite(L"Numbers.bmp", 10, 1, CVECTOR::ColorToUintInt8(0, 0, 0, 0));

	// 화살표
	CGAMERES::Inst().CreateSprite(L"arrowdnb.bmp", 1, 30, CVECTOR::ColorToUintInt8(255, 255, 255, 0));

	// 조준경
	CGAMERES::Inst().CreateSprite(L"L_crshairb.bmp", 1, 32, CVECTOR::ColorToUintInt8(255, 255, 255, 0));
	CGAMERES::Inst().CreateSprite(L"R_crshairb.bmp", 1, 32, CVECTOR::ColorToUintInt8(255, 255, 255, 0));

	//////////////////////////////////////////////
	// 임팩트
	// 폭발 임팩트
	CGAMERES::Inst().CreateSprite(L"circl100.bmp", 1, 4, CVECTOR::ColorToUintInt8(255, 255, 255, 0));

	// 게이지 임팩트
	CGAMERES::Inst().CreateSprite(L"blob.bmp", 1, 16, CVECTOR::ColorToUintInt8(255, 255, 255, 0));
	
	//////////////////////////////////////////////
	// BACKGROUND 
	// 물결
	CGAMERES::Inst().CreateSprite(L"blue.bmp", 12, 1, CVECTOR::ColorToUintInt8(255, 255, 255, 0));


	CGAMESCENE::GameSceneCreate<CTITLESCENE>(L"TITLE");
	CGAMESCENE::GameSceneCreate<CPLAYSCENE>(L"PLAY");
	CGAMESCENE::GameSceneCreate<CENDINGSCENE>(L"ENDING");

	// CGAMESCENE::ChangeScene(L"TITLE");
	CGAMESCENE::ChangeScene(L"PLAY");

	// Show 커서 표시 여부(show / no show)
	CGAMEWINDOW::ShowMouseCursor(false);

	return 0;
}

int __stdcall CMAINCLENTLOGIC::MainLoop()
{
	CGAMESOUND::Update();
	CGAMETIME::Update();
	CGAMEINPUT::Update();
	CGAMESCENE::SceneProgress();

	if (CGAMESCENE::END_SIGN)
	{
		return 0;
	}

	return 1;
}
