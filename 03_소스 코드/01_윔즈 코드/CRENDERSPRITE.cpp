#include "CRENDERSPRITE.h"
#include "CGAMERES.h"
#include <assert.h>
#include <CGAMEWINDOW.h>


CRENDERSPRITE::CRENDERSPRITE() : m_pSprite(nullptr), m_Index(0)
{
}

void CRENDERSPRITE::Sprite(const CGAMESTRING& _SpriteName, size_t _Index)
{
	m_pSprite = CGAMERES::Inst().FindSprite(_SpriteName);
	if (nullptr == m_pSprite)
	{
		assert(false);
	}

	m_Index = _Index;
}



void CRENDERSPRITE::Render()
{
	if (nullptr == m_pSprite)
	{
		CRENDER::Render();
		return;
	}

	SPRITERECT RC = m_pSprite->GetSpriteRect(m_Index);

	CVECTOR ConPos = CGAMEWINDOW::MainWindow()->WindowPosTo3DPos(RenderPos());

	TransparentBlt(CGAMERES::Inst().BackBufferDC(),
		ConPos.IntX() - m_Size.HalfIntX(),
		ConPos.IntY() - m_Size.HalfIntY(),
		m_Size.IntX(),
		m_Size.IntY(),
		m_pSprite->GetTexDC(),
		RC.m_POS.IntX(),
		RC.m_POS.IntY(),
		RC.m_SIZE.IntX(),
		RC.m_SIZE.IntY(),
		m_pSprite->TransColor());
}
