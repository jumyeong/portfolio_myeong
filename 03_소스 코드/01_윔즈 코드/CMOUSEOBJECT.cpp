#include "CMOUSEOBJECT.h"
#include <CGAMEWINDOW.h>
#include "LOGICHEADER.h"
#include "CGAMESCENE.h"
#include "CRENDERTEX.h"
#include "CRENDERSPRITE.h"

bool CMOUSEOBJECT::m_isMode = false;

CMOUSEOBJECT::CMOUSEOBJECT()
{
}

CMOUSEOBJECT::~CMOUSEOBJECT()
{
}

void CMOUSEOBJECT::Loading()
{
	mSpriteRender = CreateRender<CRENDERSPRITE>(CG_MOUSE, { 60.f, 60.f }, { 5.f, -5.f });
	mSpriteRender->Sprite(L"cursorb.bmp", 20);
	mSpriteRender->Off();

	mPointCollision = CreateCol(CG_MOUSE, { 1.0f, 1.0f }, { 0.0f, 0.0f }, COLTYPE::CT_POINT);
	
	CameraEffectOff();
}

void CMOUSEOBJECT::Update()
{
	SetPos(CGAMEWINDOW::MainWindow()->MousePosTo3D());

	//if (true == m_isMode)
	//{
	//	GetParentScene()->CameraPos(GetPos());
	//}
}

void CMOUSEOBJECT::ShowInvenCursor(bool isActive)
{
	if (nullptr == mSpriteRender)
	{
		return;
	}

	if (isActive)
	{
		mSpriteRender->On();
	}
	else
	{
		mSpriteRender->Off();
	}
}
