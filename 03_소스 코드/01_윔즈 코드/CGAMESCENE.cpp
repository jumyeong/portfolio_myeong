#include "CGAMESCENE.h"
#include "CGAMEOBJECT.h"
#include <CGAMEWINDOW.h>
#include <CGAMETIME.h>
#include <assert.h>
#include "CRENDER.h"
#include "CCOL.h"
#include "CGAMERES.h"
#include <CGAMEMATH.h>

#ifdef _DEBUG
bool CGAMESCENE::IsDebug = true;
#else
bool CGAMESCENE::IsDebug = false;
#endif // DEBUG

bool CGAMESCENE::END_SIGN = false;

std::map<std::wstring, CGAMESCENE*> CGAMESCENE::m_AllSceneMap;
CGAMESCENE* CGAMESCENE::m_CurScene = nullptr;
CGAMESCENE* CGAMESCENE::m_NextScene = nullptr;
CGAMESCENE::CGAMESCENESTARTER CGAMESCENE::Starter;


void CGAMESCENE::AllSceneDestory()
{
	std::map<std::wstring, CGAMESCENE*>::iterator Start = m_AllSceneMap.begin();
	std::map<std::wstring, CGAMESCENE*>::iterator End = m_AllSceneMap.end();

	for (; Start != End; ++Start)
	{
		delete Start->second;
	}
}

void CGAMESCENE::SceneProgress()
{
	// 다음 씬이 있다면,
	if (nullptr != m_NextScene)
	{
		// 로딩되지 않았다면,
		if (false == m_NextScene->IsLoading)
		{
			m_NextScene->Loading();
			m_NextScene->IsLoading = true;

			// 시간을 한 번 초기화해서 로딩이 게임 플레이에 영향을 미치지 않게 하는 것
			CGAMETIME::TimeReset();
			CGAMETIME::Update();
		}

		m_CurScene = m_NextScene;
		m_NextScene = nullptr;
	}

	m_CurScene->Progress();
}

//////////////////////////////////////// mgr

CGAMESCENE* CGAMESCENE::FindScene(const wchar_t* _Name)
{
	std::map<std::wstring, CGAMESCENE*>::iterator FindIter = m_AllSceneMap.find(_Name);

	if (FindIter == m_AllSceneMap.end())
	{
		return nullptr;
	}

	return FindIter->second;
}


void CGAMESCENE::ChangeScene(const wchar_t* _Name)
{
	m_NextScene = FindScene(_Name);

	if (nullptr == m_NextScene)
	{
		assert(false);
	}
}
///////////////////////////////////////////////// member

CGAMESCENE::CGAMESCENE() : IsLoading(false)
{
}

CGAMESCENE::~CGAMESCENE()
{
	std::list<CGAMEOBJECT*>::iterator Start = m_AllObjectList.begin();
	std::list<CGAMEOBJECT*>::iterator End = m_AllObjectList.end();

	for (; Start != End; ++Start)
	{
		delete (*Start);
	}
}

bool CGAMESCENE::YSortFunc(const CRENDER* _Left, const CRENDER* _Right)
{
	return _Left->RenderPos().Y > _Right->RenderPos().Y;
}
void CGAMESCENE::Update()
{
	SceneUpdate();

	std::list<CGAMEOBJECT*>::iterator Start = m_AllObjectList.begin();
	std::list<CGAMEOBJECT*>::iterator End = m_AllObjectList.end();

	for (; Start != End; ++Start)
	{
		if (false == (*Start)->IsUpdate())
		{
			continue;
		}
		(*Start)->UpdatePrev();
	}

	Start = m_AllObjectList.begin();
	End = m_AllObjectList.end();

	for (; Start != End; ++Start)
	{
		if (false == (*Start)->IsUpdate())
		{
			continue;
		}
		(*Start)->Update();
	}

	Start = m_AllObjectList.begin();
	End = m_AllObjectList.end();

	for (; Start != End; ++Start)
	{
		if (false == (*Start)->IsUpdate())
		{
			continue;
		}
		(*Start)->UpdateNext();
	}
}

void CGAMESCENE::Render()
{
	Rectangle(CGAMERES::Inst().BackBufferDC(), 0, 0,
		CGAMEWINDOW::MainWindow()->GetSize().IntX(), // 그곳에서부터의 크기이다.
		CGAMEWINDOW::MainWindow()->GetSize().IntX());

	std::map<int, std::list<CRENDER*>>::iterator StartRenderGroup = m_AllRender.begin();
	std::map<int, std::list<CRENDER*>>::iterator EndRenderGroup = m_AllRender.end();

	for (; StartRenderGroup != EndRenderGroup; ++StartRenderGroup)
	{
		if (YSort.end() != YSort.find(StartRenderGroup->first))
		{
			StartRenderGroup->second.sort(YSortFunc);
		}

		std::list<CRENDER*>::iterator StartRender = StartRenderGroup->second.begin();
		std::list<CRENDER*>::iterator EndRender = StartRenderGroup->second.end();

		for (; StartRender != EndRender; ++StartRender)
		{
			// 부모 오브젝트까지 체크해야한다.
			if (false == (*StartRender)->IsUpdate())
			{
				continue;
			}
			(*StartRender)->Render();
		}
	}

	Debug();
	

	BitBlt
	(
		CGAMEWINDOW::MainWindow()->WINHDC(), // DC는 그릴수 있는 권한이자 도화지다 여긴 그릴 도화지를 지정
		0,
		0,
		CGAMEWINDOW::MainWindow()->GetSize().IntX(), // 그곳에서부터의 크기이다.
		CGAMEWINDOW::MainWindow()->GetSize().IntY(),
		CGAMERES::Inst().BackBufferDC(), // 복사하려는 그림이 뭐냐? 그림에 있는 이미지를 윈도우에다가 그려주려고 하는것
		0,  // 그 이미지의 어디서부터????
		0,  // 그 이미지의 어디서부터????
		SRCCOPY // 그 방식이 뭐야? // 고속복사
	);
}

void CGAMESCENE::Col()
{
	for (auto& Group : m_AllCol)
	{
		for (auto& Col : Group.second)
		{
			Col->ColUpdate();
		}
	}

	std::map<int, std::set<int>>::iterator LeftIter = m_LinkData.begin();
	std::map<int, std::set<int>>::iterator LeftEndIter = m_LinkData.end();

	for (; LeftIter != LeftEndIter; ++LeftIter)
	{
		
		std::set<int>::iterator RightIter = LeftIter->second.begin();
		std::set<int>::iterator RightEndIter = LeftIter->second.end();

		for (; RightIter != RightEndIter; ++RightIter)
		{
			// Left 충돌 그룹의 번호
			int Left = LeftIter->first;

			// Right 충돌 그룹의 번호
			int Right = (*RightIter);

			std::map<int, std::list<CCOL*>>::iterator LeftGiter = m_AllCol.find(Left);
			std::map<int, std::list<CCOL*>>::iterator RightGiter = m_AllCol.find(Right);

			std::list<CCOL*>::iterator LeftStartCol = LeftGiter->second.begin();
			std::list<CCOL*>::iterator LeftEndCol = LeftGiter->second.end();

			for (; LeftStartCol != LeftEndCol; ++LeftStartCol)
			{
				if (false == (*LeftStartCol)->IsUpdate())
				{
					continue;
				}

				std::list<CCOL*>::iterator RightStartCol = RightGiter->second.begin();
				std::list<CCOL*>::iterator RightEndCol = RightGiter->second.end();

				for (; RightStartCol != RightEndCol; ++RightStartCol)
				{
					if (false == (*RightStartCol)->IsUpdate())
					{
						continue;
					}

					(*LeftStartCol)->ColCheck((*RightStartCol));
				}
			}
		}
	}

}

void CGAMESCENE::Debug()
{
	if (false == IsDebug)
	{
		return;
	}

	DebugRender();

	for (auto& Obj : m_AllObjectList)
	{
		Obj->Debug();
	}

	for (auto& Group : m_AllCol)
	{
		for (auto& Col : Group.second)
		{
			Col->Debug();
		}
	}
}

void CGAMESCENE::Release()
{
	{
		std::map<int, std::list<CRENDER*>>::iterator StartRenderGroup = m_AllRender.begin();
		std::map<int, std::list<CRENDER*>>::iterator EndRenderGroup = m_AllRender.end();

		for (; StartRenderGroup != EndRenderGroup; ++StartRenderGroup)
		{
			std::list<CRENDER*>::iterator StartRender = StartRenderGroup->second.begin();
			std::list<CRENDER*>::iterator EndRender = StartRenderGroup->second.end();

			for (; StartRender != EndRender; )
			{
				// 부모 오브젝트까지 체크해야한다.
				if (false == (*StartRender)->IsDeath())
				{
					++StartRender;
					continue;
				}
				StartRender = StartRenderGroup->second.erase(StartRender);
			}
		}
	}

	{
		std::map<int, std::list<CCOL*>>::iterator StartColGroup = m_AllCol.begin();
		std::map<int, std::list<CCOL*>>::iterator EndColGroup = m_AllCol.end();

		for (; StartColGroup != EndColGroup; ++StartColGroup)
		{
			std::list<CCOL*>::iterator StartCol = StartColGroup->second.begin();
			std::list<CCOL*>::iterator EndCol = StartColGroup->second.end();

			for (; StartCol != EndCol; )
			{
				// 부모 오브젝트까지 체크해야한다.
				if (false == (*StartCol)->IsDeath())
				{
					++StartCol;
					continue;
				}
				StartCol = StartColGroup->second.erase(StartCol);
			}
		}
	}

	std::list<CGAMEOBJECT*>::iterator Start = m_AllObjectList.begin();
	std::list<CGAMEOBJECT*>::iterator End = m_AllObjectList.end();

	for (; Start != End; )
	{
		if (false == (*Start)->IsDeath())
		{
			++Start;
			continue;
		}

		delete (*Start);

		Start = m_AllObjectList.erase(Start);
	}

}

// 씬은 여러 오브젝트를 가지고 있고 그들을 행동시켜주는 함수
void CGAMESCENE::Progress()
{
	Update();
	Render();
	Col();
	Release();
}

void CGAMESCENE::PushRender(CRENDER* _Renderer)
{
	std::map<int, std::list<CRENDER*>>::iterator OrderIter = m_AllRender.find(_Renderer->GetOrder());

	// 그 순서가 없을 수도 있다.
	if (m_AllRender.end() == OrderIter)
	{
		m_AllRender.insert(std::map<int, std::list<CRENDER*>>::value_type(_Renderer->GetOrder(), std::list<CRENDER*>()));
		OrderIter = m_AllRender.find(_Renderer->GetOrder());
	}

	OrderIter->second.push_back(_Renderer);
}

void CGAMESCENE::PushCol(CCOL* _Col)
{
	std::map<int, std::list<CCOL*>>::iterator OrderIter = m_AllCol.find(_Col->GetOrder());

	if (m_AllCol.end() == OrderIter)
	{
		m_AllCol.insert(std::map<int, std::list<CCOL*>>::value_type(_Col->GetOrder(), std::list<CCOL*>()));
		OrderIter = m_AllCol.find(_Col->GetOrder());
	}

	OrderIter->second.push_back(_Col);
}
