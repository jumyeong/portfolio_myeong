#include "CGAMERES.h"
#include <assert.h>

void CGAMERES::BackBufferCreate(const CVECTOR& _Size)
{
	m_BackBuffer = new CGAMETEX();
	m_BackBuffer->Create(_Size);
}


CGAMERES::CGAMERES() : m_BackBuffer(nullptr)
{
}

CGAMERES::~CGAMERES()
{
	if (nullptr != m_BackBuffer)
	{
		delete m_BackBuffer;
	}

	{
		std::map<CGAMESTRING, CGAMESPRITE*>::iterator Start = m_AllSprite.begin();
		std::map<CGAMESTRING, CGAMESPRITE*>::iterator End = m_AllSprite.end();

		for (; Start != End; ++Start)
		{
			delete Start->second;
		}
	}

	{
		std::map<CGAMESTRING, CGAMETEX*>::iterator Start = m_AllTex.begin();
		std::map<CGAMESTRING, CGAMETEX*>::iterator End = m_AllTex.end();

		for (; Start != End; ++Start)
		{
			delete Start->second;
		}
	}
}


CGAMETEX* CGAMERES::FindTex(const CGAMESTRING& _FileName)
{
	std::map<CGAMESTRING, CGAMETEX*>::iterator FindIter = m_AllTex.find(_FileName);

	if (FindIter != m_AllTex.end())
	{
		return FindIter->second;
	}
	return nullptr;
}

void CGAMERES::LoadTex(const CGAMEFILE& _File)
{
	if (nullptr != FindTex(_File.FileName()))
	{
		return;
	}

	CGAMETEX* NewText = new CGAMETEX();
	NewText->m_File = _File;

	NewText->Loading();


	m_AllTex.insert(std::map<CGAMESTRING, CGAMETEX*>::value_type(NewText->m_File.FileName(), NewText));
}

/////////////////////////////////// Sprite
CGAMESPRITE* CGAMERES::FindSprite(const CGAMESTRING& _FileName)
{
	std::map<CGAMESTRING, CGAMESPRITE*>::iterator FindIter = m_AllSprite.find(_FileName);

	if (FindIter != m_AllSprite.end())
	{
		return FindIter->second;
	}

	return nullptr;
}

CGAMESPRITE* CGAMERES::CreateSprite(const CGAMESTRING& _TexName, unsigned int _W, unsigned int _H, unsigned int _TransColor, bool _Reverse)
{
	return CreateSprite(_TexName, _TexName, _W, _H, _TransColor, _Reverse);
}

CGAMESPRITE* CGAMERES::CreateSprite(const CGAMESTRING& _TexName, const CGAMESTRING& _SpriteName, unsigned int _W, unsigned int _H, unsigned int _TransColor, bool _Reverse)
{
	CGAMETEX* pTex = FindTex(_TexName);

	if (nullptr == pTex)
	{
		assert(false);
	}

	CGAMESPRITE* NewSprite = FindSprite(_TexName);

	if (nullptr != NewSprite)
	{
		assert(false);
	}

	NewSprite = new CGAMESPRITE();

	NewSprite->m_pTex = pTex;
	NewSprite->Create(_W, _H, _TransColor, _Reverse);

	m_AllSprite.insert(std::map<CGAMESTRING, CGAMESPRITE*>::value_type(_SpriteName, NewSprite));

	return NewSprite;
}