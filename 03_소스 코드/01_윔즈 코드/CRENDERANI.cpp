#include "CRENDERANI.h"
#include "CGAMERES.h"
#include <assert.h>
#include <CGAMEWINDOW.h>
#include <CGAMETIME.h>


void CFRAMEANI::Reset()
{
	m_CurFrame = m_StartFrame;
	m_fCurTime = m_fTime[m_CurFrame - m_StartFrame];
}

void CFRAMEANI::Update()
{
	// 0.1f -= DeltaTime;
	m_fCurTime -= CGAMETIME::DeltaTime();

	if (0 >= m_fCurTime)
	{
		++m_CurFrame;

		if (m_EndFrame + 1 <= m_CurFrame)
		{
			if (true == m_Loop)
			{
				Reset();
			}
			else
			{
				--m_CurFrame;
			}
		}

		m_fCurTime = m_fTime[m_CurFrame - m_StartFrame];
	}
}

void CRENDERANI::Render()
{
	if (nullptr == m_CurAni)
	{
		//CRENDER::Render();
		return;
	}
	m_CurAni->Update();

	SPRITERECT RC = m_CurAni->m_Sprite->GetSpriteRect(m_CurAni->m_CurFrame);

	CVECTOR ConPos = CGAMEWINDOW::MainWindow()->WindowPosTo3DPos(RenderPos());

	if (false == IsAlpha())
	{
		TransparentBlt(CGAMERES::Inst().BackBufferDC(),
			ConPos.IntX() - m_Size.HalfIntX(),
			ConPos.IntY() - m_Size.HalfIntY(),
			m_Size.IntX(),
			m_Size.IntY(),
			m_CurAni->m_Sprite->GetTexDC(),
			RC.m_POS.IntX(),
			RC.m_POS.IntY(),
			RC.m_SIZE.IntX(),
			RC.m_SIZE.IntY(),
			m_CurAni->m_Sprite->TransColor());
	}
	else
	{

		AlphaBlend(CGAMERES::Inst().BackBufferDC(),
			ConPos.IntX() - m_Size.HalfIntX(),
			ConPos.IntY() - m_Size.HalfIntY(),
			m_Size.IntX(),
			m_Size.IntY(),
			m_CurAni->m_Sprite->GetTexDC(),
			RC.m_POS.IntX(),
			RC.m_POS.IntY(),
			RC.m_SIZE.IntX(),
			RC.m_SIZE.IntY(),
			m_BF);
	}
}

CRENDERANI::~CRENDERANI()
{
	std::map<CGAMESTRING, CFRAMEANI*>::iterator Start = m_AllAni.begin();
	std::map<CGAMESTRING, CFRAMEANI*>::iterator End = m_AllAni.end();

	for (; Start != End; ++Start)
	{
		delete Start->second;
	}
}


void CRENDERANI::StartAni(const CGAMESTRING& _AniName)
{
	m_CurAni = FindAni(_AniName);

	if (nullptr == m_CurAni)
	{
		assert(false);	
	}

	m_CurAni->Reset();
}

CFRAMEANI* CRENDERANI::FindAni(const CGAMESTRING& _AniName)
{
	std::map<CGAMESTRING, CFRAMEANI*>::iterator FindIter = m_AllAni.find(_AniName);

	if (FindIter != m_AllAni.end())
	{
		return FindIter->second;
	}
	return nullptr;
}

void CRENDERANI::CreateAni(const CGAMESTRING& _SpriteName, const CGAMESTRING& _AniName, size_t _Start, size_t _End, float _Time, float _Loop)
{
	CGAMESPRITE* FindSprite = CGAMERES::Inst().FindSprite(_SpriteName);
	if (nullptr == FindSprite)
	{
		assert(false);
	}

	CFRAMEANI* NewAni = FindAni(_AniName);

	if (nullptr != NewAni)
	{
		assert(false);
	}

	NewAni = new CFRAMEANI();
	NewAni->m_StartFrame = _Start;
	NewAni->m_EndFrame = _End;

	NewAni->m_Sprite = FindSprite;
	NewAni->m_fTime.resize(_End - _Start + 1);

	for (size_t i = 0; i < NewAni->m_fTime.size(); i++)
	{
		NewAni->m_fTime[i] = _Time;
	}

	NewAni->m_Name = _AniName;

	NewAni->m_Loop = _Loop;

	m_AllAni.insert(std::map<CGAMESTRING, CFRAMEANI*>::value_type(_AniName, NewAni));
}
