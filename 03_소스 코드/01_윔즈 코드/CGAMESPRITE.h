#pragma once
#include "CGAMETEX.h"
#include <vector>


class SPRITERECT
{
public:
	CVECTOR m_POS;
	CVECTOR m_SIZE;
};

class CGAMERES;
class CGAMESPRITE : public CGAMERESBASE
{
	friend CGAMERES;

private:
	CGAMETEX* m_pTex;
	std::vector<SPRITERECT> m_SpriteData;
	unsigned int m_TransColor;

public:
	unsigned int TransColor() {
		return m_TransColor;
	}

	CGAMETEX* GetTex()
	{
		return m_pTex;
	}
	HDC GetTexDC()
	{
		return m_pTex->IMGDC();
	}

public:
	inline SPRITERECT GetSpriteRect(size_t _Index)
	{
		return m_SpriteData[_Index];
	}
public:

	void Create(unsigned int _W, unsigned int _H, unsigned int _TransColor, bool _Reverse = false);

public:
	CGAMESPRITE() : m_pTex(nullptr), m_TransColor(0) {	}
	~CGAMESPRITE() {	}
};

