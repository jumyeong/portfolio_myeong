#include "CRENDERTEX.h"
#include "CGAMERES.h"
#include <assert.h>
#include <Windows.h>
#include <CGAMEWINDOW.h>
#include "CGAMETEX.h"


CRENDERTEX::CRENDERTEX() 
	: m_Tex(nullptr), m_TransColor(CVECTOR::ColorToUintInt8(255, 255, 255, 0))
{
}

void CRENDERTEX::ScaleToImageSize() 
{
	return ScaleToImageSize(1);
}

void CRENDERTEX::ScaleToImageSize(float mutiple)
{
	if (nullptr == m_Tex)
	{
		assert(false);
	}
	m_Size.X = (float)m_Tex->GetW() * mutiple;
	m_Size.Y = (float)m_Tex->GetH() * mutiple;
}

void CRENDERTEX::SetTex(const CGAMESTRING& _Name)
{
	m_Tex = CGAMERES::Inst().FindTex(_Name);
	if (nullptr == m_Tex)
	{
		assert(false);
	}
}

void CRENDERTEX::TransColor(unsigned int _R, unsigned int _G, unsigned int _B, unsigned int _A) 
{
	m_TransColor = CVECTOR::ColorToUintInt8(_R, _G, _B, _A);
}

void CRENDERTEX::Render()
{
	if (nullptr == m_Tex)
	{
		//assert(false);
		CRENDER::Render();
		return;
	}
	CVECTOR ConPos = CGAMEWINDOW::MainWindow()->WindowPosTo3DPos(RenderPos());

	TransparentBlt(
		CGAMERES::Inst().BackBufferDC(),
		ConPos.IntX() - m_Size.HalfIntX(),
		ConPos.IntY() - m_Size.HalfIntY(),
		m_Size.IntX(),
		m_Size.IntY(),
		m_Tex->IMGDC(),
		0,
		0,
		m_Tex->GetW(),
		m_Tex->GetH(),
		m_TransColor
	);
}

