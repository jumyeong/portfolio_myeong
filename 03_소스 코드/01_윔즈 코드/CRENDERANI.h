#pragma once
#include "CRENDER.h"
#include <vector>
#include <map>

class CRENDERANI;
class CGAMESPRITE;
class CFRAMEANI
{
	friend CRENDERANI;

private:
	CGAMESTRING m_Name;
	CGAMESPRITE* m_Sprite;
	size_t m_StartFrame;
	size_t m_EndFrame;
	size_t m_CurFrame;
	bool m_Loop;
	bool m_Revese;
	std::vector<float> m_fTime;
	float m_fCurTime;

public:
	void Update();
	void Reset();

public:
	CFRAMEANI() 
		: m_StartFrame(0)
		, m_EndFrame(0)
		, m_CurFrame(0)
		, m_Loop(true)
		, m_Sprite(nullptr)
		, m_fCurTime(0.1f)
		, m_Revese(false) 
	{
	}
	
	virtual ~CFRAMEANI() 
	{
	}
};

class CRENDERANI : public CRENDER
{
private:
	std::map<CGAMESTRING, CFRAMEANI*>	m_AllAni;
	CFRAMEANI*							m_CurAni;

private:
	CFRAMEANI* FindAni(const CGAMESTRING& _AniName);

public:
	void StartAni(const CGAMESTRING& _AniName);
	void CreateAni(const CGAMESTRING& _SpriteName, const CGAMESTRING& _AniName, size_t _Start, size_t _End, float _Time, float _Loop);

public:
	CGAMESTRING CurAniName()
	{
		//size_t Size = m_CurAni->m_Name.StrCount();
		return m_CurAni->m_Name;
	}

	size_t CurAniFrame()
	{
		return m_CurAni->m_CurFrame;
	}

	bool CurAniEnd()
	{
		return m_CurAni->m_CurFrame == m_CurAni->m_EndFrame;
	}

public:
	void Render() override;

public:
	virtual ~CRENDERANI();
};

