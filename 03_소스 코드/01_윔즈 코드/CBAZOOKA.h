#pragma once
#include "CGAMEOBJECT.h"
#include "LOGICHEADER.h"

class CRENDERANI;
class CRENDERSPRITE;
class CBAZOOKA : public CGAMEOBJECT
{
public:
	static bool IsFire;

	// HP에 영향을 주는 데미지
	static float m_Dmg;

private:

	CRENDERSPRITE* SpriteRender;
	CVECTOR m_Dir;
	CVECTOR m_Gravity;

	float m_Gauge;

	bool PosDir[COLORDIR::MAX];
	bool IsInWater;

	CRENDERANI* BoomAni;
	
	bool IsEmpty;
	
	bool IsBoom;
	CVECTOR BoomPos;

	float DelayTime;

	bool DirCheck;

public:
	void SetDir(CVECTOR _Dir)
	{
		m_Dir = _Dir;
		m_Dir.UnitVector();
	}
	CVECTOR GetDir()
	{
		return m_Dir;
	}

public:
	CVECTOR GetBoomPos()
	{
		return BoomPos;
	}


public:
	CRENDERSPRITE* GetSprite()
	{
		return SpriteRender;
	}
	void Fire(CVECTOR _Dir);
	int SpriteNum(CVECTOR _Next, CVECTOR _Cur);
	void GetCurGauge();

	void DirColorCheck();
	bool ReachToObject();

	void CurrentPosBoom();
	void Reset();

public:

	bool CheckBoom();
	bool CheckFire();
	bool CheckEmpty();

public:
	void Loading() override;
	void Update() override;


public:
	CBAZOOKA();
	virtual ~CBAZOOKA();
};