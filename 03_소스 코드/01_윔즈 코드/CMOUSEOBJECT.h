#pragma once
#include "CGAMEOBJECT.h"

class CCOL;
class CRENDERTEX;
class CRENDERSPRITE;
class CMOUSEOBJECT : public CGAMEOBJECT
{
private:
	CRENDERSPRITE* mSpriteRender;
	CCOL* mPointCollision;

public:
	static bool m_isMode;

public:
	void Loading() override;
	void Update() override;

	void ShowInvenCursor(bool isActive);

public:
	CMOUSEOBJECT();
	virtual ~CMOUSEOBJECT();

};

