#pragma once
#include "CGAMEOBJECT.h"


class CRENDERTEX;
class CINVEN : public CGAMEOBJECT
{
public:
	CRENDERTEX* TexRender;
	CCOL* ARRCOL[11][5];
	CVECTOR StartInvenPos;
	CRENDERTEX* WeaponIconTexRender[11][5];

	std::wstring WeaponIconNameArr[11][5];
	std::wstring NoWeaponIconNameArr[11][5];
	CRENDERTEX* BorderLine;

	CVECTOR m_SelectIndex;		// 현재 Select되고 있는 인벤란 Index
	
	bool isClick;				// 클릭 여부 확인 
	std::wstring ClickWeapon;	// 클릭 아이템 이름 
public:
	void Loading() override;
	void Update() override;

public:
	virtual void ColEnter(CCOL* _this, CCOL* _Other);
	virtual void ColStay(CCOL* _this, CCOL* _Other);
	virtual void ColExit(CCOL* _this, CCOL* _Other);

public:
	void SetClick(bool _isClick)
	{
		isClick = _isClick;
	}
	bool GetClick()
	{
		return isClick;
	}
	std::wstring GetClickWeaponName()
	{
		return ClickWeapon;
	}

public:
	// Col 색 변경을 위한 함수들
	inline void RedColorCollision()
	{
		for (size_t i = 0; i < 5; ++i)
		{
			for (size_t j = 0; j < 11; ++j)
			{
				ARRCOL[j][i]->PenColorRed();
			}
		}
	}
	inline void ChangeColorCollision()
	{
		// pen color가 red 일때,
		if (true == ARRCOL[0][0]->IsPenColorNull())
		{
			for (size_t i = 0; i < 5; ++i)
			{
				for (size_t j = 0; j < 11; ++j)
				{

					ARRCOL[j][i]->PenColorRed();

				}
			}
		}
		else
		{
			for (size_t i = 0; i < 5; ++i)
			{
				for (size_t j = 0; j < 11; ++j)
				{

					ARRCOL[j][i]->PenColorNull();

				}
			}
		}
	}
	inline void NullColorCollision()
	{
		for (size_t i = 0; i < 5; ++i)
		{
			for (size_t j = 0; j < 11; ++j)
			{
				ARRCOL[j][i]->PenColorNull();
			}
		}
	}

public:
	CINVEN();
	virtual ~CINVEN();
};

