#pragma once
#include "CGAMEOBJECT.h"

class CRENDERTEX;
class CGAMEMAP : public CGAMEOBJECT
{
public:
	static CRENDERTEX* TexRender;

public:
	void Loading() override;
	void Update() override;

public:
	CGAMEMAP();
	virtual ~CGAMEMAP();
};

