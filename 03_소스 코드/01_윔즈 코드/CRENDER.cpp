#include "CRENDER.h"
#include <CGAMEWINDOW.h>
#include "CGAMEOBJECT.h"
#include "CGAMESCENE.h"
#include "CGAMERES.h"


CRENDER::CRENDER() : m_BF{ 0 }
{
	m_BF.BlendOp = AC_SRC_OVER;
	m_BF.BlendFlags = 0;
	m_BF.AlphaFormat = AC_SRC_ALPHA;
	m_BF.SourceConstantAlpha = 255;
}
CRENDER::~CRENDER()	{	}

void CRENDER::Render()
{
	CSUBOBJECT::Debug();
}

void CRENDER::PushRender()
{
	m_ParentObject->GetParentScene()->PushRender(this);
}
