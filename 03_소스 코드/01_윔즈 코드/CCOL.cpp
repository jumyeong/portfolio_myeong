#include "CCOL.h"
#include "CGAMEOBJECT.h"
#include "CGAMESCENE.h"
#include <CGAMEWINDOW.h>
#include "CGAMERES.h"

CCOL::CGAMECOLSTATER CCOL::Starter;

bool (*CCOL::ARRCOL[static_cast<int>(COLTYPE::CT_MAX)][static_cast<int>(COLTYPE::CT_MAX)])(const CCOLFIGURE&, const CCOLFIGURE&) = { nullptr };


CCOL::CGAMECOLSTATER::CGAMECOLSTATER()
{
	ARRCOL[static_cast<int>(COLTYPE::CT_RECT  )][static_cast<int>(COLTYPE::CT_RECT  )] = RECTTORECT;
	ARRCOL[static_cast<int>(COLTYPE::CT_RECT  )][static_cast<int>(COLTYPE::CT_CIRCLE)] = RECTTOCIRCLE;
	ARRCOL[static_cast<int>(COLTYPE::CT_RECT  )][static_cast<int>(COLTYPE::CT_POINT )] = RECTTOPOINT;
	ARRCOL[static_cast<int>(COLTYPE::CT_CIRCLE)][static_cast<int>(COLTYPE::CT_RECT  )] = CIRCLETORECT;
	ARRCOL[static_cast<int>(COLTYPE::CT_CIRCLE)][static_cast<int>(COLTYPE::CT_CIRCLE)] = CIRCLETOCIRCLE;
	ARRCOL[static_cast<int>(COLTYPE::CT_CIRCLE)][static_cast<int>(COLTYPE::CT_POINT )] = CIRCLETOPOINT;
	ARRCOL[static_cast<int>(COLTYPE::CT_POINT )][static_cast<int>(COLTYPE::CT_RECT  )] = POINTTORECT;
	ARRCOL[static_cast<int>(COLTYPE::CT_POINT )][static_cast<int>(COLTYPE::CT_CIRCLE)] = POINTTOCIRCLE;
	ARRCOL[static_cast<int>(COLTYPE::CT_POINT )][static_cast<int>(COLTYPE::CT_POINT )] = POINTTOPOINT;
}

////////////////// 충돌 함수


bool CCOL::RECTTORECT(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right)
{
	if (_Left.RIGHT() < _Right.LEFT())
	{
		return false;
	}
	if (_Left.LEFT() > _Right.RIGHT())
	{
		return false;
	}
	if (_Left.TOP() < _Right.BOTTOM())
	{
		return false;
	}
	if (_Left.BOTTOM() > _Right.TOP())
	{
		return false;
	}
	return true;
}

bool CCOL::RECTTOCIRCLE(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right)
{
	CCOLFIGURE WRECT = _Left;
	CCOLFIGURE HRECT = _Left;       /// 확인해봐야하는 곳

	WRECT.m_Size.X += _Right.m_Size.X;
	HRECT.m_Size.Y += _Right.m_Size.X;     /// 확인해봐야하는 곳

	if (true == RECTTOPOINT(WRECT, _Right)
		|| true == RECTTOPOINT(HRECT, _Right))
	{
		return true;
	}

	CCOLFIGURE Circle;
	Circle.m_Size = _Right.m_Size;

	Circle.m_Pos = _Left.LT();
	if (true == CIRCLETOPOINT(Circle, _Right)) { return true; }
	Circle.m_Pos = _Left.RT();
	if (true == CIRCLETOPOINT(Circle, _Right)) { return true; }
	Circle.m_Pos = _Left.LB();
	if (true == CIRCLETOPOINT(Circle, _Right)) { return true; }
	Circle.m_Pos = _Left.RB();
	if (true == CIRCLETOPOINT(Circle, _Right)) { return true; }

	return false;
}

bool CCOL::CIRCLETORECT(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right)
{
	return RECTTOCIRCLE(_Right, _Left);
}

bool CCOL::RECTTOPOINT(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right)
{
	if (_Right.m_Pos.X < _Left.LEFT())
	{
		return false;
	}
	if (_Right.m_Pos.X > _Left.RIGHT())
	{
		return false;
	}
	if (_Right.m_Pos.Y < _Left.BOTTOM())
	{
		return false;
	}
	if (_Right.m_Pos.Y > _Left.TOP())
	{
		return false;
	}

	return true;
}

bool CCOL::CIRCLETOCIRCLE(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right)
{
	CVECTOR DirLen = _Left.m_Pos - _Right.m_Pos;

	if (DirLen.LEN() < (_Left.m_Size.HalfX() + _Right.m_Size.HalfX()))
	{
		return true;
	}
	return false;
}

bool CCOL::CIRCLETOPOINT(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right)
{
	CVECTOR DirLen = _Left.m_Pos - _Right.m_Pos;

	if (DirLen.LEN() < _Left.m_Size.HalfX())
	{
		return true;
	}
	return false;
}

bool CCOL::POINTTOPOINT(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right)
{
	return _Left.m_Pos == _Right.m_Pos;
}

bool CCOL::POINTTORECT(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right)
{
	return RECTTOPOINT(_Right, _Left);
}

bool CCOL::POINTTOCIRCLE(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right)
{
	return CIRCLETOPOINT(_Right, _Left);
}

void CCOL::PushCol()
{
	m_ParentObject->GetParentScene()->PushCol(this); // scene 확인
}

bool CCOL::ColRangeCheck(const CCOL* _Other)
{
	return ARRCOL[static_cast<int>(ColType)][static_cast<int>(_Other->ColType)](m_FI, _Other->m_FI);
}

void CCOL::ColUpdate()
{
	m_FI.m_Pos = CGAMEWINDOW::MainWindow()->WindowPosTo3DPos(RenderPos());
	m_FI.m_Size = m_Size;
}


void CCOL::ColCheck(CCOL* _Col)
{
	if (true == ColRangeCheck(_Col))
	{
		FindCol = m_OtherCol.find(_Col);
		if (FindCol == m_OtherCol.end())
		{
			m_ParentObject->ColEnter(this, _Col);
			_Col->m_ParentObject->ColEnter(_Col, this);

			m_OtherCol.insert(_Col);
			_Col->m_OtherCol.insert(this);
		}
		else
		{
			m_ParentObject->ColStay(this, _Col);
			_Col->m_ParentObject->ColStay(_Col, this);
		}
	}
	else
	{
		FindCol = m_OtherCol.find(_Col);
		if (FindCol != m_OtherCol.end())
		{
			m_ParentObject->ColExit(this, _Col);
			_Col->m_ParentObject->ColExit(_Col, this);

			m_OtherCol.erase(_Col);
			_Col->m_OtherCol.erase(this);
		}
	}
}
void CCOL::Debug()
{
	switch (ColType)
	{
	case COLTYPE::CT_POINT:
	{
		//////////////////// BRUSH & PEN ///////////////////////////

		HDC m_hdc = CGAMERES::Inst().BackBufferDC();

		HBRUSH m_Brush = (HBRUSH)GetStockObject(NULL_BRUSH);
		HBRUSH m_OldBrush = (HBRUSH)SelectObject(m_hdc, m_Brush);

		HPEN m_Pen = CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
		HPEN m_OldPen = (HPEN)SelectObject(m_hdc, m_Pen);

		CVECTOR ConPos = CGAMEWINDOW::MainWindow()->WindowPosTo3DPos(RenderPos());
		Rectangle(CGAMERES::Inst().BackBufferDC(),
			ConPos.IntX() - 2,
			ConPos.IntY() - 2,
			ConPos.IntX() + 2,
			ConPos.IntY() + 2);

		SelectObject(m_hdc, m_OldPen);
		DeleteObject(m_Pen);

		SelectObject(m_hdc, m_OldBrush);
		DeleteObject(m_Brush);

		break;
	}
	case COLTYPE::CT_CIRCLE:
	{
		HDC m_hdc = CGAMERES::Inst().BackBufferDC();

		HBRUSH m_Brush = (HBRUSH)GetStockObject(NULL_BRUSH);
		HBRUSH m_OldBrush = (HBRUSH)SelectObject(m_hdc, m_Brush);

		HPEN m_Pen = CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
		HPEN m_OldPen = (HPEN)SelectObject(m_hdc, m_Pen);

		CVECTOR ConPos = CGAMEWINDOW::MainWindow()->WindowPosTo3DPos(RenderPos());
		Ellipse(CGAMERES::Inst().BackBufferDC(),
			ConPos.IntX() - m_Size.HalfIntX(),
			ConPos.IntY() - m_Size.HalfIntY(),
			ConPos.IntX() + m_Size.HalfIntX(),
			ConPos.IntY() + m_Size.HalfIntY());
		
		SelectObject(m_hdc, m_OldPen);
		DeleteObject(m_Pen);

		SelectObject(m_hdc, m_OldBrush);
		DeleteObject(m_Brush);
		
		//////////////////// BRUSH & PEN ///////////////////////////

		break;
	}
	case COLTYPE::CT_RECT:
		CSUBOBJECT::Debug();
		break;
	default:
		break;
	}

}
