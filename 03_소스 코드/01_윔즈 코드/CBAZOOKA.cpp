#include "CBAZOOKA.h"
#include <Windows.h>
#include <CGAMETIME.h>
#include "CRENDERTEX.h"
#include "LOGICHEADER.h"
#include "CRENDERSPRITE.h"
#include <assert.h>
#include "CGAMEMAP.h"
#include "CGAMERES.h"
#include "CGAMESCENE.h"
#include "CGAMESOUND.h"
#include "CRENDERANI.h"
#include "CGAMEPLAYER.h"
#include "LOGICHEADER.h"

bool CBAZOOKA::IsFire = false;
float CBAZOOKA::m_Dmg = 45.0f;

CBAZOOKA::CBAZOOKA() 
	: m_Gauge(0.0f)
	, m_Dir()
	, IsInWater(false)
	, BoomAni(nullptr)
	, IsBoom(false)
	, DelayTime(-1.0f)
	//, IsFire(false)
	, IsEmpty(false)
	, DirCheck(false)
	, BoomPos(CVECTOR::ZERO)
{
	m_Gravity = { 0.0f, -1.0f };
}

CBAZOOKA::~CBAZOOKA()
{
}

void CBAZOOKA::Loading()
{
	SpriteRender = CreateRender<CRENDERSPRITE>(CG_BULLET, { 60.0f, 60.0f }, { 0, 0 });
	SpriteRender->Off();

	BoomAni = CreateRender<CRENDERANI>(CG_BULLET, { 100, 100 }, { 0.0f, 0.0f });
	BoomAni->CreateAni(L"circl100.bmp", L"Effect100", 0, 3, 0.01f, false);
	BoomAni->Off();
}

void CBAZOOKA::Update()
{
	CVECTOR OldPos = GetPos();
	CVECTOR pVector = m_Dir + m_Gravity;

	if (true == IsFire)
	{
		CGAMEPLAYER::PlayerCameraAuth = CAMERAAUTHORITY::CA_WEAPON;
		GetParentScene()->CameraPos(GetPos());
	}

	if (3.0f < DelayTime)
	{
		IsEmpty = false;
		
		Off();

		GetParentScene()->ChangeCameraTarget();
		CGAMEPLAYER::PlayerCameraAuth = CAMERAAUTHORITY::CA_PLAYER;
		SpriteRender->Off();
		DelayTime = -1.0f;
		DirCheck = false;
		IsInWater = false;
		return;
	}
	else if (0.0f <= DelayTime)
	{
		DelayTime += CGAMETIME::DeltaTime();
		return;
	}

	// 미사일의 마지막
	if (true == IsBoom && true == BoomAni->IsOn() && true == BoomAni->CurAniEnd())
	{
		IsBoom = false;
		IsFire = false;
		BoomAni->Off();

		DelayTime = 0.0f;
	}

	if (true == IsInWater)
	{
		IsFire = false;
		BoomAni->Off();

		pVector = m_Gravity * 0.05f;
	}

	Move(pVector * CGAMETIME::DeltaTime());
	// 8방향의 색을 확인
	DirColorCheck();

	m_Gravity += CVECTOR::DOWN * 400.0f * CGAMETIME::DeltaTime();

	SpriteRender->Sprite(L"missile.bmp", SpriteNum(GetPos(), OldPos));

	// 물 속에서 카메라가 잡히지 않는 경우,
	if (true == SpriteRender->IsOn() && -900.0f >= GetPos().Y)
	{
		IsBoom = false;
		IsInWater = false;

		SpriteRender->Off();

		DelayTime = 0.0f;
		Reset();
	}


	DirCheck = ReachToObject();

	if (true == DirCheck && false == IsBoom)
	{
		if (5.0f <= GetPos().X && 1915.0f >= GetPos().X && -5.0f >= GetPos().Y)
		{
			if (-691.0f >= GetPos().Y)
			{
				IsInWater = true;
			}
			else
			{
				CGAMESOUND::Play(L"Explosion2.wav");
				CurrentPosBoom();

				SpriteRender->Off();

				IsBoom = true;
				// 폭발 위치
				BoomPos = GetPos();

				BoomAni->StartAni(L"Effect100");
				BoomAni->On();

				Reset();
			}
		}
		if (-691.0f >= GetPos().Y)
		{
			IsInWater = true;
		}
	}
}

void CBAZOOKA::Reset()
{
	m_Gauge = 0.0f;
	m_Gravity = { 0.0f, -1.0f };
}

bool CBAZOOKA::CheckBoom()
{
	return IsBoom;
}

bool CBAZOOKA::CheckFire()
{
	return IsFire;
}

bool CBAZOOKA::CheckEmpty()
{
	return IsEmpty;
}

void CBAZOOKA::Fire(CVECTOR _Dir)
{
	m_Dir *= 10 * m_Gauge;
	SpriteRender->Sprite(L"missile.bmp", SpriteNum(GetPos(), _Dir));
	SpriteRender->On();
	IsFire = true;
	IsEmpty = true;
}

void CBAZOOKA::GetCurGauge()
{
	if (MAX_GAUGE >= m_Gauge)
	{
		m_Gauge += 50.f * CGAMETIME::DeltaTime();
	}
}

int CBAZOOKA::SpriteNum(CVECTOR _Next, CVECTOR _Cur)
{
	float Deg = CVECTOR::DirToDeg(_Cur, _Next);

	Deg = -Deg + 90.0f;

	if (360.0f <= Deg)
	{
		Deg -= 360.0f;
	}
	else if (0 >= Deg)
	{
		Deg += 360.0f;
	}

	int SpriteNum = (int)(Deg / 11.25f);

	if (32 <= SpriteNum)
	{
		SpriteNum = 0;
	}
	else if (0 > SpriteNum)
	{
		SpriteNum = 31;
	}

	// 32 개로 나누어서 이미지 8개씩
	//  0 ~  7 : 위쪽(90) ~ 오른쪽가기전
	//  8 ~ 15 : 오른쪽(0도) ~ 아래가기전
	// 16 ~ 23 : 아래(270) ~ 왼쪽가기전
	// 24 ~ 31 : 왼쪽(180) ~ 위쪽가기전

	return SpriteNum;
}

// DirColorCheck
void CBAZOOKA::DirColorCheck()
{
	// 프레임마다 확인
	for (size_t i = 0; i < COLORDIR::MAX; i++)
	{
		PosDir[i] = false;
	}

	CVECTOR Pos = GetPos();
	CGAMETEX* Tex = CGAMEMAP::TexRender->GetTex();

	CVECTOR Color = Tex->GetColor(Pos.IntX(), -(Pos.IntY()) + 5);
	if (Color != CVECTOR::WRITE) { PosDir[COLORDIR::B] = true; };
	Color = Tex->GetColor(Pos.IntX(), -(Pos.IntY()) - 5);
	if (Color != CVECTOR::WRITE) { PosDir[COLORDIR::T] = true; };

	Color = Tex->GetColor(Pos.IntX() + 5, -(Pos.IntY()) - 5);
	if (Color != CVECTOR::WRITE) { PosDir[COLORDIR::RT] = true; };
	Color = Tex->GetColor(Pos.IntX() + 5, -(Pos.IntY()));
	if (Color != CVECTOR::WRITE) { PosDir[COLORDIR::R] = true; };
	Color = Tex->GetColor(Pos.IntX() + 5, -(Pos.IntY()) + 5);
	if (Color != CVECTOR::WRITE) { PosDir[COLORDIR::RB] = true; };

	Color = Tex->GetColor(Pos.IntX() - 5, -(Pos.IntY()) - 5);
	if (Color != CVECTOR::WRITE) { PosDir[COLORDIR::LT] = true; };
	Color = Tex->GetColor(Pos.IntX() - 5, -(Pos.IntY()));
	if (Color != CVECTOR::WRITE) { PosDir[COLORDIR::L] = true; };
	Color = Tex->GetColor(Pos.IntX() - 5, -(Pos.IntY()) + 5);
	if (Color != CVECTOR::WRITE) { PosDir[COLORDIR::LB] = true; };
}

bool CBAZOOKA::ReachToObject()
{
	if (true == PosDir[COLORDIR::T]) { return true; };
	if (true == PosDir[COLORDIR::B]) { return true; };

	if (true == PosDir[COLORDIR::RT]) { return true; };
	if (true == PosDir[COLORDIR::R]) { return true; };
	if (true == PosDir[COLORDIR::RB]) { return true; };

	if (true == PosDir[COLORDIR::LT]) { return true; };
	if (true == PosDir[COLORDIR::L]) { return true; };
	if (true == PosDir[COLORDIR::LB]) { return true; };

	return false;
}

void CBAZOOKA::CurrentPosBoom()
{
	CGAMETEX* TexBack = CGAMEMAP::TexRender->GetTex();
	CGAMETEX* TexBoom = CGAMERES::Inst().FindTex(L"Boom100.bmp");
	CVECTOR Pos = GetPos();
	// 마우스로 변경해보자

	TexBack->IMGDC();
	TexBoom->IMGDC();

	TransparentBlt(
		TexBack->IMGDC(),
		Pos.IntX() - 50,
		-Pos.IntY() - 50,
		100,
		100,
		TexBoom->IMGDC(),
		0,
		0,
		100,
		100,
		0
	);

}
