#include "CGAMEPLAYER.h"
#include <Windows.h>
#include <CGAMETIME.h>
#include <CGAMEWINDOW.h>
#include <CGAMEINPUT.h>
#include "CBAZOOKA.h"
#include "CGAMEMAP.h"
#include "CGAMESPRITE.h"
#include "CGAMESCENE.h"
#include "CRENDERANI.h"
#include "CRENDERTEX.h"
#include "CRENDERSPRITE.h"
#include "CCOL.h"
#include "LOGICHEADER.h"
#include <CGAMESOUND.h>
#include "CGAMERES.h"
#include "CGAMEPLAYBACK.h"

CAMERAAUTHORITY CGAMEPLAYER::PlayerCameraAuth = CAMERAAUTHORITY::CA_MOUSE;

CGAMEPLAYER::CGAMEPLAYER() : m_Speed(50.0f), m_JumpPower(0.0f), m_IsJumping(false), m_Gravity(5.0f), m_Accumulate(0.f), m_Hp(100), HpBar(nullptr), m_HpDigit{0},
	m_PlayerCol(nullptr), Arrow(nullptr), Dir(0), PlayerAni(nullptr), EndMoving(0), TargetPlayer(false), AimPoint(nullptr), PlayerRender(nullptr),
	m_eState(PLAYERSTATE::PS_IDLE), m_eDir(ACTORDIR::AD_LEFT), PosDir{ false }, m_Inven(nullptr), m_PlayerCameraPos(nullptr), InWater(false),
	m_DirString(), m_AniString(), m_DirCheck(),
	IsOpeningWeapon(false), m_AimPos({ -1.0f, 0.0f }), ClickWeaponName(L""), CurWeaponName(L""), m_Degree(180.0f), m_FireGaze{nullptr}, GazePower(0.0f),
	MouseMoveStart(false), MouseMoveEnd(false), TempMousePos(),
	isHit(false), HitType(WEAPONTYPE::WT_MAX), Dmg(0.0f), PlayerHitRender(nullptr), m_HitDir(), HitPower(200.0f), m_vGravity(CVECTOR::DOWN), CheckHitMap(false)
{
}

CGAMEPLAYER::~CGAMEPLAYER()
{
}

void CGAMEPLAYER::Loading()
{
	SetPos({ -100, 0 });

	CGAMEINPUT::CreateKey(L"L", VK_LEFT);
	CGAMEINPUT::CreateKey(L"R", VK_RIGHT);

	CGAMEINPUT::CreateKey(L"U", VK_UP);
	CGAMEINPUT::CreateKey(L"D", VK_DOWN);

	CGAMEINPUT::CreateKey(L"JUMP", VK_RETURN);

	CGAMEINPUT::CreateKey(L"Fire", VK_SPACE);

	CGAMEINPUT::CreateKey(L"INVEN", VK_RBUTTON);
	CGAMEINPUT::CreateKey(L"SelectingWeapon", VK_LBUTTON);

	// 플레이어의 상태를 구분짓고 그것들을 1가지 상태만 실행될수 있도록 하는게
	// FSM

	//enum ACTORDIR
	//{
	//	LEFT,
	//	RIGHT,
	//};
	m_DirString.push_back(L"L_");
	m_DirString.push_back(L"R_");
	m_DirString.push_back(L"T_");
	m_DirString.push_back(L"B_");

	PlayerAni = CreateRender<CRENDERANI>(CG_PLAYER, { 60, 60 }, { 0.0f, -0.1F });

	// Idle 모션
	m_DirCheck.push_back(true);
	m_AniString.push_back(L"Idle");
	PlayerAni->CreateAni(L"wbrth1Right.bmp", L"R_Idle", 0, 39, 0.1f, true);
	PlayerAni->CreateAni(L"wbrth1Left.bmp", L"L_Idle", 0, 39, 0.1f, true);

	// Fall 모션
	m_DirCheck.push_back(false);
	m_AniString.push_back(L"Fall");
	PlayerAni->CreateAni(L"R_wfall.bmp", L"R_Fall", 0, 1, 0.1f, false);
	PlayerAni->CreateAni(L"L_wfall.bmp", L"L_Fall", 0, 1, 0.1f, false);

	// Moving 모션
	m_DirCheck.push_back(true);
	m_AniString.push_back(L"Moving");
	PlayerAni->CreateAni(L"R_wwalk1.bmp", L"R_Moving", 0, 14, 0.03f, true);
	PlayerAni->CreateAni(L"L_wwalk1.bmp", L"L_Moving", 0, 14, 0.03f, true);
	
	// Jump 모션
	m_DirCheck.push_back(false);
	m_AniString.push_back(L"Jump");
	PlayerAni->CreateAni(L"R_wflylnk.bmp", L"R_Jump", 0, 6, 0.1f, false);
	PlayerAni->CreateAni(L"L_wflylnk.bmp", L"L_Jump", 0, 6, 0.1f, false);


	// 무기 모션 스프라이트(7개, 9개)
	m_DirCheck.push_back(false);
	m_AniString.push_back(L"Weapon");
	
	// Bazooka open 모션
	PlayerAni->CreateAni(L"R_wbazlnk.bmp", L"R_Bazooka_Open", 0, 6, 0.01f, false);
	PlayerAni->CreateAni(L"L_wbazlnk.bmp", L"L_Bazooka_Open", 0, 6, 0.01f, false);

	// Homing Missile open 모션
	PlayerAni->CreateAni(L"R_wbz2lnk.bmp", L"R_Homing Missile_Open", 0, 6, 0.01f, false);
	PlayerAni->CreateAni(L"L_wbz2lnk.bmp", L"L_Homing Missile_Open", 0, 6, 0.01f, false);

	// Grenade open 모션
	PlayerAni->CreateAni(L"R_wgrnlnk.bmp", L"R_Grenade_Open", 0, 9, 0.01f, false);
	PlayerAni->CreateAni(L"L_wgrnlnk.bmp", L"L_Grenade_Open", 0, 9, 0.01f, false);

	// Holy Grenade open 모션
	PlayerAni->CreateAni(L"R_whgrlnk.bmp", L"R_Holy Grenade_Open", 0, 9, 0.01f, false);
	PlayerAni->CreateAni(L"L_whgrlnk.bmp", L"L_Holy Grenade_Open", 0, 9, 0.01f, false);

	// Air Strike open 모션
	PlayerAni->CreateAni(L"R_wairlnk.bmp", L"R_Air Strike_Open", 0, 9, 0.01f, false);
	PlayerAni->CreateAni(L"L_wairlnk.bmp", L"L_Air Strike_Open", 0, 9, 0.01f, false);

	// Mine open 모션
	PlayerAni->CreateAni(L"R_wminlnk.bmp", L"R_Mine_Open", 0, 9, 0.01f, false);
	PlayerAni->CreateAni(L"L_wminlnk.bmp", L"L_Mine_Open", 0, 9, 0.01f, false);

	// Shotgun open 모션
	PlayerAni->CreateAni(L"R_wshglnk.bmp", L"R_Shotgun_Open", 0, 9, 0.01f, false);
	PlayerAni->CreateAni(L"L_wshglnk.bmp", L"L_Shotgun_Open", 0, 9, 0.01f, false);

	// Teleport open 모션
	PlayerAni->CreateAni(L"R_wtellnk.bmp", L"R_Teleport_Open", 0, 9, 0.01f, false);
	PlayerAni->CreateAni(L"L_wtellnk.bmp", L"L_Teleport_Open", 0, 9, 0.01f, false);


	//////////////////////////////////////////////////////////////////////
	// 데미지 받는 모션
	// Hit 미끄러짐 (ANI)
	m_DirCheck.push_back(false);
	m_AniString.push_back(L"Slide");
	PlayerAni->CreateAni(L"R_wslideu.bmp", L"R_Slide", 0, 2, 0.01f, true);
	PlayerAni->CreateAni(L"L_wslideu.bmp", L"L_Slide", 0, 2, 0.01f, true);

	// Hit 미끄러짐 후, Idle 상태 전환 (ANI)
	PlayerAni->CreateAni(L"R_wsldlk1.bmp", L"R_Slide_End", 0, 21, 0.1f, false);
	PlayerAni->CreateAni(L"L_wsldlk1.bmp", L"L_Slide_End", 0, 21, 0.1f, false);

	// Hit Fly (SPRITE)
	PlayerHitRender = CreateRender<CRENDERSPRITE>(CG_PLAYER, { 60, 60 }, { 0.0f, 0.0f });
	PlayerHitRender->Off();

	/////////////////////////////////////////////////////////////////////////

	// 게이지 표시
	for (int i = 0; i < 16; i++)
	{
		m_FireGaze[i] = CreateRender<CRENDERSPRITE>(CG_PLAYER, { 64, 64 }, { 0.0f, 0.0f });
		m_FireGaze[i]->Sprite(L"blob.bmp", i);
		m_FireGaze[i]->Off();
	}

	// 플레이어 스프라이트 이미지
	// 바주카 조준 스프라이트(32개)
	PlayerRender = CreateRender<CRENDERSPRITE>(CG_PLAYER, { 60, 60 }, { 0.0f, 0.0f });
	PlayerRender->Off();

	// 웜즈 표시 (화살표)
	Arrow = CreateRender<CRENDERANI>(CG_PLAYER, { 60, 60 }, { 0.0f, 50.0f });
	Arrow->CreateAni(L"arrowdnb.bmp", L"Arrow_Blue", 0, 29, 0.03f, true);
	Arrow->StartAni(L"Arrow_Blue");

	// 조준경 표시
	AimPoint = CreateRender<CRENDERSPRITE>(CG_PLAYER, { 60, 60 }, { 0.0f, 0.0f });
	AimPoint->Off();

	// 미사일 생성
	m_Bullet = GetParentScene()->CreateObject<CBAZOOKA>();
	m_Bullet->Off();

	// 체력바
	HpBar = CreateRender<CRENDERTEX>(CG_PLAYER, { 56.0f, 18.0f }, { 0.0f, 31.0f });
	HpBar->SetTex(L"5618.bmp");
	HpBar->TransColor(255, 255, 255, 0);
	HpBar->ScaleToImageSize();
	HpBar->Off();

	// 숫자들을 모두 이미지로 저장
	for (int i = 0; i < 3; i++)
	{
		m_AllNumbers[i] = CreateRender<CRENDERSPRITE>(CG_PLAYER, { 18.0f, 18.0f }, { 0.0f, 0.0f });
		m_AllNumbers[i]->Off();
	}
	
	// 초기 상태
	ChangeState(PLAYERSTATE::PS_IDLE);

	// 45도
	//CVECTOR::DegToDir(CVECTOR::DirToDeg({ 1, 1 }, { 5, 5 }));
	//CVECTOR::DegToDir(CVECTOR::DirToDeg({ 0, 0 }, { -1, 1 }));
	//CVECTOR::DegToDir(CVECTOR::DirToDeg({ -1, -1 }, { -5, -5 }));
	//CVECTOR::DegToDir(CVECTOR::DirToDeg({ 0, 0 }, { 5, -5 }));
}

void CGAMEPLAYER::DirCheck()
{
	if (false == m_DirCheck[m_eState])
	{
		return;
	}

	ACTORDIR Dir = m_eDir;

	if (true == CGAMEINPUT::Press(L"L"))
	{
		// 이전 방향에 대한 문제 해결 방법
		if (false == CGAMEINPUT::Press(L"R"))
		{
			Dir = AD_LEFT;
		}
	}
	if (true == CGAMEINPUT::Press(L"R"))
	{
		// 이전 방향에 대한 문제 해결 방법
		if (false == CGAMEINPUT::Press(L"L"))
		{
			Dir = AD_RIGHT;
		}
	}

	if (Dir != m_eDir)
	{
		m_eDir = Dir;

		// 그러면 애니메이션을 변경해준다.
		// 현재까지 방향 전환뿐
		PlayerAni->StartAni(m_DirString[m_eDir] + m_AniString[m_eState]);
	}
}
void CGAMEPLAYER::ChangeState(PLAYERSTATE _Change, CGAMESTRING _AddStr, bool _NotAni)
{
	m_eState = _Change;

	if (false == _NotAni)
	{
		if (L"" == _AddStr)
		{
			//                  R_                    Idle;
			PlayerAni->StartAni(m_DirString[m_eDir] + m_AniString[m_eState]);
		}
		else
		{
			PlayerAni->StartAni(m_DirString[m_eDir] + m_AniString[m_eState] + _AddStr);
		}
	}
}

void CGAMEPLAYER::Update()
{

	if (true == GroundCheck() && true == TargetPlayer)
	{
		if (CA_MOUSE == PlayerCameraAuth)
		{
			CVECTOR CurrentMousePos = CGAMEWINDOW::MainWindow()->MousePosTo3D();
			GetParentScene()->CameraPos(GetPos() + (CurrentMousePos - TempMousePos) * 2.0f);
		}
		if (CA_PLAYER == PlayerCameraAuth)
		{
			GetParentScene()->CameraPos(GetPos());
			TempMousePos = CGAMEWINDOW::MainWindow()->MousePosTo3D();
		}
		if (CA_WEAPON == PlayerCameraAuth)
		{
			GetParentScene()->CameraPos(m_Bullet->GetPos());
			TempMousePos = CGAMEWINDOW::MainWindow()->MousePosTo3D();
		}
	}

	if (false != TargetPlayer)
	{
		DirCheck();
	}
	
	DirColorCheck();

	// 무조건 한번에 1가지 상태를 유지하는게 FSM

	switch (m_eState)
	{
	case PS_IDLE:
		IDLE();
		break;
	case PS_FALL:
		FALL();
		break;
	case PS_WEAPON:
		WEAPON();
		break;
	case PS_MOVE:
		MOVE();
		break;
	case PS_JUMP:
		JUMP();
		break;
	case PS_HIT:
		HIT();
		break;
	default:
		break;
	}

	// 체력바
	if (false == TargetPlayer)
	{
		HpRenderOn();
	}
	else
	{
		HpRenderOff();
	}
}
////////////////////////////////////////
// Hp Function
void CGAMEPLAYER::HpRenderOn()
{	
	// 체력 확인 후 이미지 사용할 수 있게 변경
	HpRender();
	HpBar->On();
	for (int i = 0; i < 3; i++)
	{
		m_AllNumbers[i]->On();
	}
}
void CGAMEPLAYER::HpRenderOff()
{
	HpRender();
	HpBar->Off();
	for (int i = 0; i < 3; i++)
	{
		m_AllNumbers[i]->Off();
	}
}

void CGAMEPLAYER::HpRender()
{
	// 체력 배열에 값 넣기
	HpChangeDigit();

	float SizeHelper = 0.0f;
	//m_AllNumbers[1]->SetPos({ 0.0f, 30.0f });
	//m_AllNumbers[1]->On();

	for (int i = 0; i < 3; i++)
	{
		if (-1 != m_HpDigit[3 - i - 1])
		{
			m_AllNumbers[i]->Sprite(L"Numbers.bmp", m_HpDigit[3 - i - 1]);
			m_AllNumbers[i]->SetPos({11.0f * (i-1) + SizeHelper, 30.0f});
			m_AllNumbers[i]->On();
		}
		else
		{
			SizeHelper -= 4.5f;
		}
	}
}
////////////////////////////////////////
// Gaze Function

void CGAMEPLAYER::GazePowerOn()
{
	if (16 > GazePower)
	{
		GazePower += CGAMETIME::DeltaTime(10);
	}
	if (16 <= GazePower)
	{
		GazePower = 16;
	}
	for (int i = 0; i < (int)GazePower; ++i)
	{
		m_FireGaze[i]->SetPos({ CVECTOR::DegToDir(m_Degree).X * (4.0f * i + 10.0f), CVECTOR::DegToDir(m_Degree).Y * (4.0f * i + 10.0f) });
		m_FireGaze[i]->On();
	}
}
void CGAMEPLAYER::GazePowerOff()
{
	for (int i = 0; i < (int)GazePower; ++i)
	{
		m_FireGaze[i]->Off();
	}
	GazePower = 0;
}

////////////////////////////////////////
bool CGAMEPLAYER::GroundCheck() {
	DirColorCheck();

	if (true == PosDir[COLORDIR::B]) { return true; }
	return false;
}

bool CGAMEPLAYER::TopCheck() {
	DirColorCheck();

	if (true == PosDir[COLORDIR::T]) {	return true; }
	return false;
}
bool CGAMEPLAYER::TopRightCheck() {
	DirColorCheck();

	if (true == PosDir[COLORDIR::RT]) { return true; }
	return false;
}
bool CGAMEPLAYER::RightCheck() {
	DirColorCheck();

	if (true == PosDir[COLORDIR::R]) { return true; }
	return false;
}
bool CGAMEPLAYER::TopLeftCheck() {
	DirColorCheck();

	if (true == PosDir[COLORDIR::LT]) { return true; }
	return false;
}
bool CGAMEPLAYER::LeftCheck() {
	DirColorCheck();

	if (true == PosDir[COLORDIR::L]) { return true; }
	return false;
}

//////////////////////// 이동 가능 공간 확인 함수(hill, ground check 함수) //////////////////////////////
// Pos.IntX() +1 or -1 확인(왼쪽, 오른쪽 차이), 보는 방향의 크기(~7px) : 
// -Pos.IntY() + 10.0f 웜즈 크기(~26px) : 
bool CGAMEPLAYER::PreHillCheck(int _X, int _Y)
{
	// 웜즈 중심 점 위치(변수)
	CVECTOR Pos = GetPos();
	// 맵 객체 주소
	CGAMETEX* Tex = CGAMEMAP::TexRender->GetTex();
	
	CVECTOR PrePos = { Pos.IntX() + _X, -(Pos.IntY()) + _Y };
	CVECTOR PreColor = Tex->GetColor(PrePos.IntX(), PrePos.IntY());
	

	// BLACK 이 색이 나올때까지 Y축을 증가
	while (PreColor != CVECTOR::WRITE)
	{
		PrePos.Y -= 1.0f;
		PreColor = Tex->GetColor(PrePos.IntX(), PrePos.IntY());
	}

	// PreColor의 위치와 Pos 위치의 거리를 계산한다.
	// 너무 멀면 움직이지 못하게 만든다.(적당히 계산)
	// 또는 너무 높아도 안됨(각도) (낮은 건 상관 X)
	
	Pos.Y = -(float)(Pos.IntY()) + _Y;
	CVECTOR DifPos = Pos - PrePos;
	
	if (26 <= DifPos.LEN())
	{
		return false;
	}

	if ( 0 <= _X)
	{
		if (90.0f < DifPos.DirToDeg(Pos, PrePos) && 0.0f > DifPos.DirToDeg(Pos, PrePos))
		{
			return false;
		}
	}
	else
	{
		if (90.0f > DifPos.DirToDeg(Pos, PrePos))
		{
			return false;
		}
	}
	return true;
}
bool CGAMEPLAYER::PreGroundCheck(int _X, int _Y)
{
	// 웜즈 중심 점 위치(변수)
	CVECTOR Pos = GetPos();
	// 맵 객체 주소
	CGAMETEX* Tex = CGAMEMAP::TexRender->GetTex();

	CVECTOR PrePos = { Pos.IntX() + _X, -(Pos.IntY()) + _Y };
	CVECTOR PreColor = Tex->GetColor(PrePos.IntX(), PrePos.IntY());
	
	while (PreColor == CVECTOR::WRITE)
	{
		PrePos.Y += 1.0f;
		PreColor = Tex->GetColor(PrePos.IntX(), PrePos.IntY());
	}

	// PreColor의 위치와 Pos 위치의 거리를 계산한다.
	// 너무 멀면 움직이지 못하게 만든다.(적당히 계산)
	// 또는 너무 높아도 안됨(각도) (낮은 건 상관 X)

	Pos.Y = -(float)(Pos.IntY()) + _Y;
	CVECTOR DifPos = Pos - PrePos;

	if (26 <= DifPos.LEN())
	{
		return false;
	}

	if (0 <= _X)
	{
		if (270.0f > DifPos.DirToDeg(Pos, PrePos) && 360.0f < DifPos.DirToDeg(Pos, PrePos))
		{
			return false;
		}
	}
	else
	{
		if (270.0f < DifPos.DirToDeg(Pos, PrePos))
		{
			return false;
		}
	}
	return true;
}
////////////////////////////////////////////////////////

void CGAMEPLAYER::GroundPosSetting()
{
	while (false == GroundCheck()) //내 아래 땅이 없다면
	{
		Move(CVECTOR::DOWN);
		//SetPlayerCameraCurPos();
	}
	
	while (true == GroundCheck()) //내가 땅에 박혀있다면?
	{
		Move(CVECTOR::UP);
		//SetPlayerCameraCurPos();
	}
	
	if (false == GroundCheck())
	{
		Move(CVECTOR::DOWN);
		//SetPlayerCameraCurPos();
	}
}

// IDLE
void CGAMEPLAYER::IDLE()
{
	static int IdleCheck = 0;

	//화살 표시
	if (false != TargetPlayer)
	{
		IdleCheck++;
		if (1000 < IdleCheck)
		{
			HpBar->On();
			Arrow->On();
			IdleCheck = 0;
		}
		if (CA_FIX != PlayerCameraAuth )
		{
			PlayerCameraAuth = CA_MOUSE;
		}
	}
	else
	{
		// 체력바 활성화
		HpRenderOn();
	}

	if (true == isHit)
	{
		ChangeState(PLAYERSTATE::PS_HIT);
		Arrow->Off();
		IdleCheck = 0;
		return;
	}

	// 내 아래 땅이 없다면
	// FALL
	if (false == GroundCheck() && false == PreGroundCheck(0))
	{
		ChangeState(PLAYERSTATE::PS_FALL);
		Arrow->Off();
		IdleCheck = 0;
		return;
	}

	/////////////////////////////////////////////////////////////////////////
	// 아래는 FALL 이외의 버튼으로 갈수 없는 모션들
	if (false == TargetPlayer)
	{
		Arrow->Off();
		return;
	}

	if ((true == CGAMEINPUT::Press(L"L") && true == CGAMEINPUT::Press(L"R"))
		|| (true == CGAMEINPUT::Down(L"L") && true == CGAMEINPUT::Press(L"R"))
		|| (true == CGAMEINPUT::Press(L"L") && true == CGAMEINPUT::Down(L"R")))
	{
		return;
	}

	if (true == CGAMEINPUT::Press(L"L") && true == PreHillCheck(-5)
		|| true == CGAMEINPUT::Press(L"R") && true == PreHillCheck(5))
	{
		ChangeState(PLAYERSTATE::PS_MOVE);
		Arrow->Off();
		IdleCheck = 0;
		return;
	}

	if (true == CGAMEINPUT::Down(L"JUMP"))
	{
		ChangeState(PLAYERSTATE::PS_JUMP);
		Arrow->Off();
		IdleCheck = 0;
		return;
	}

	if (CurWeaponName != ClickWeaponName)
	{
		CurWeaponName = ClickWeaponName;
		m_AniString[PS_WEAPON] = CurWeaponName;
		ChangeState(PLAYERSTATE::PS_WEAPON, L"_Open");
		IdleCheck = 0;
		return;
	}
}
void CGAMEPLAYER::FALL() 
{
	if (true == isHit)
	{
		ChangeState(PLAYERSTATE::PS_HIT);
		Arrow->Off();
		return;
	}

	Move(CVECTOR::DOWN * CGAMETIME::DeltaTime(m_Accumulate * m_Speed));
	m_Accumulate += CGAMETIME::DeltaTime() * m_Gravity;

	//if (-680.0f >= GetPos().Y)
	//{
	//	InWater = true;
	//}

	if (false != TargetPlayer)
	{
		SetPlayerCameraMove(AD_DOWN, CGAMETIME::DeltaTime(m_Accumulate * m_Speed));
		//GetParentScene()->CameraPosMove(CVECTOR::DOWN * CGAMETIME::DeltaTime(m_Accumulate * m_Speed));
	}

	if (true == GroundCheck() && true == PreGroundCheck(0))
	{
		m_Accumulate = 0.f;

		if (L"" != ClickWeaponName)
		{
			ChangeState(PLAYERSTATE::PS_WEAPON, L"_Open");
		}
		else
		{
			ChangeState(PLAYERSTATE::PS_IDLE);
		}
		return;
	}
}


// HIT
void CGAMEPLAYER::HIT()
{
	CVECTOR OldPos = GetPos();
	CVECTOR pVector = m_HitDir + m_vGravity;

	Move(pVector * CGAMETIME::DeltaTime());
	// 8방향의 색을 확인
	DirColorCheck();

	m_vGravity += CVECTOR::DOWN * 400.0f * CGAMETIME::DeltaTime();

	if (true == GroundCheck())
	{
		pVector.Y *= -1.0f;
		pVector.UnitVector();
		m_HitDir = pVector;
		HitPower = 0.0f;
		m_vGravity = CVECTOR::ZERO;
		
		GroundPosSetting();
		ChangeState(PLAYERSTATE::PS_IDLE);
		isHit = false;
	}
}



// WATERFALL
// 물속에 빠졌을 때
void CGAMEPLAYER::WATERFALL()
{
}


// MOVE
void CGAMEPLAYER::MOVE()
{
	static int Count = 0;

	
	if (false == TargetPlayer)
	{
		Count = 0;

		Arrow->Off();
		ChangeState(PLAYERSTATE::PS_IDLE);
		return;
	}
	else
	{
		PlayerCameraAuth = CA_PLAYER;
	}

	if (Count <= 0)
	{
		CGAMESOUND::Play(L"Walk-Expand.wav");
	}
	Count++;
	if (Count > 160)
	{
		Count = 0;
	}

	// 한번에 한개의 상태만 실행한다.
	if (true == CGAMEINPUT::Up(L"L") || true == CGAMEINPUT::Up(L"R"))
	{
		if (L"" != ClickWeaponName)
		{
			ChangeState(PLAYERSTATE::PS_WEAPON, L"_Open");
		}
		else 
		{
			ChangeState(PLAYERSTATE::PS_IDLE);
		}
		Count = 0;
		return;
	}

	if ((true == CGAMEINPUT::Down(L"L") && true == CGAMEINPUT::Press(L"R"))
		|| (true == CGAMEINPUT::Press(L"L") && true == CGAMEINPUT::Down(L"R"))
		|| (true == CGAMEINPUT::Press(L"L") && true == CGAMEINPUT::Press(L"R")))
	{
		if (L"" != ClickWeaponName)
		{
			ChangeState(PLAYERSTATE::PS_WEAPON, L"_Open");
		}
		else
		{
			ChangeState(PLAYERSTATE::PS_IDLE);
		}
		Count = 0;
		return;
	}

	if (false == GroundCheck() && false == PreGroundCheck(0))
	{
		ChangeState(PLAYERSTATE::PS_FALL);
		Count = 0;
		return;
	}

	GroundPosSetting();


	if (true == CGAMEINPUT::Press(L"L"))
	{
		// 5px 앞 만큼 확인, 언덕이 있을 경우
		if (true == PreHillCheck(-5))
		{
			Move(CVECTOR::LEFT * CGAMETIME::DeltaTime(m_Speed));
			//SetPlayerCameraMove(AD_LEFT, CGAMETIME::DeltaTime(m_Speed));
			return;
		}
	}

	if (true == CGAMEINPUT::Press(L"R"))
	{
		if (true == PreHillCheck(5))
		{
			Move(CVECTOR::RIGHT * CGAMETIME::DeltaTime(m_Speed));
			//SetPlayerCameraMove(AD_RIGHT, CGAMETIME::DeltaTime(m_Speed));
			return;
		}
	}
}

// JUMP
void CGAMEPLAYER::JUMP()
{
	DirColorCheck();

	CVECTOR m_JumpPrevPos;

	float DeclareSpeed = 0.003f;

	if (false == TargetPlayer)
	{
		m_IsJumping = false;
		m_Accumulate = 0;

		Arrow->Off();
		ChangeState(PLAYERSTATE::PS_IDLE);
		return;
	}
	else
	{
		PlayerCameraAuth = CA_PLAYER;
	}

	// 점프할 경우,
	if (false == m_IsJumping)
	{
		CGAMESOUND::Play(L"Jump1.wav");
		m_JumpPrevPos = GetPos();
		m_JumpPower = 100.f;
		m_IsJumping = true;
	}

	m_JumpPower -= CGAMETIME::DeltaTime() * m_Gravity * 120.0f;
	m_Accumulate += m_JumpPower * 0.015f;

	if (true == m_IsJumping)
	{
		if (L"L_Jump" == PlayerAni->CurAniName())
		{
			if (false == PreHillCheck(-3, 0) && false == PreGroundCheck(-3, 0))
			{
				if (L"" != ClickWeaponName)
				{
					ChangeState(PLAYERSTATE::PS_WEAPON, L"_Open");
				}
				else
				{
					ChangeState(PLAYERSTATE::PS_IDLE);
				}
				return;
			}

			if (true == TopCheck() || true == TopLeftCheck())
			{
				Move(CVECTOR::DOWN * CGAMETIME::DeltaTime(m_Accumulate * 4 * 0.3f));
				SetPlayerCameraMove(AD_DOWN, CGAMETIME::DeltaTime(m_Accumulate * 4 * 0.3f));
				if (0 < m_Accumulate)
				{
					m_Accumulate *= -1.0f;
				}
			}
			else
			{
				Move(CVECTOR::UP * CGAMETIME::DeltaTime(m_Accumulate * 4));
				SetPlayerCameraMove(AD_UP, CGAMETIME::DeltaTime(m_Accumulate * 4));
			}

			if (true == LeftCheck() || true == TopLeftCheck())
			{
				Move(CVECTOR::RIGHT * CGAMETIME::DeltaTime(m_Speed * 3.5f * 0.3f));
				SetPlayerCameraMove(AD_RIGHT, CGAMETIME::DeltaTime(m_Speed * 3.5f * 0.3f));

				m_eDir = AD_RIGHT;
				ChangeState(PLAYERSTATE::PS_JUMP);
			}
			else
			{
				Move(CVECTOR::LEFT * CGAMETIME::DeltaTime(m_Speed * 3.5f));
				SetPlayerCameraMove(AD_LEFT, CGAMETIME::DeltaTime(m_Speed * 3.5f));
			}
		}
		else if (L"R_Jump" == PlayerAni->CurAniName())
		{
			if (false == PreHillCheck(3, 0) && false == PreGroundCheck(3, 0))
			{
				if (L"" != ClickWeaponName)
				{
					ChangeState(PLAYERSTATE::PS_WEAPON, L"_Open");
				}
				else
				{
					ChangeState(PLAYERSTATE::PS_IDLE);
				}
				return;
			}
			if (true == TopCheck() || true == TopRightCheck())
			{
				Move(CVECTOR::DOWN * CGAMETIME::DeltaTime(m_Accumulate * 4 * 0.3f));
				SetPlayerCameraMove(AD_DOWN, CGAMETIME::DeltaTime(m_Accumulate * 4 * 0.3f));
				
				if (0 < m_Accumulate)
				{
					m_Accumulate *= -1.0f;
				}
			}
			else
			{
				Move(CVECTOR::UP * CGAMETIME::DeltaTime(m_Accumulate) * 4);
				SetPlayerCameraMove(AD_UP, CGAMETIME::DeltaTime(m_Accumulate * 4));
			}

			if (true == RightCheck() || true == TopRightCheck())
			{
				Move(CVECTOR::LEFT * CGAMETIME::DeltaTime(m_Speed * 3.5f * 0.3f));
				SetPlayerCameraMove(AD_LEFT, CGAMETIME::DeltaTime(m_Speed * 3.5f * 0.3f));

				m_eDir = AD_LEFT;
				ChangeState(PLAYERSTATE::PS_JUMP);
			}
			else
			{
				Move(CVECTOR::RIGHT * CGAMETIME::DeltaTime(m_Speed * 3.5f));
				SetPlayerCameraMove(AD_RIGHT, CGAMETIME::DeltaTime(m_Speed * 3.5f));
			}
		}
	}

	// 점프 이전 위치보다 내려갈 경우, 
	//if (m_JumpPrevPos.IntY() )
	//{
		// fall 상태
	//}

	if (true == GroundCheck())
	{
		// 힘을 다하거나 힘의 방향이 아래일 경우,
		if (0 > m_JumpPower)
		{
			GroundPosSetting();
			m_IsJumping = false;

			m_Accumulate = 0;

			if (L"" != ClickWeaponName)
			{
				ChangeState(PLAYERSTATE::PS_WEAPON, L"_Open");
			}
			else
			{
				ChangeState(PLAYERSTATE::PS_IDLE);
			}
		}
	}
}

void CGAMEPLAYER::WEAPON()
{
	if (true == isHit)
	{
		ChangeState(PLAYERSTATE::PS_IDLE);
		PlayerAni->On();
		PlayerRender->Off();
		AimPoint->Off();

		Arrow->Off();
		return;
	}

	if (false == GroundCheck() && false == PreGroundCheck(0))
	{
		PlayerAni->On();
		PlayerRender->Off();
		AimPoint->Off();

		ChangeState(PLAYERSTATE::PS_FALL);
		Arrow->Off();
		return;
	}

	if (false == TargetPlayer)
	{
		PlayerAni->On();
		PlayerRender->Off();
		AimPoint->Off();

		Arrow->Off();
		ChangeState(PLAYERSTATE::PS_IDLE);
		return;
	}
	else
	{
		if (CA_FIX != PlayerCameraAuth)
		{
			PlayerCameraAuth = CA_MOUSE;
		}
	}


	// 현재 Ani를 확인,
	if (L"L_Jump" == PlayerAni->CurAniName() || L"R_Jump" == PlayerAni->CurAniName())
	{
		return;
	}

	if (0 > m_Degree)
	{
		m_Degree = 360.0f + m_Degree;
	}
	if (360.0f < m_Degree)
	{
		m_Degree = m_Degree - 360.0f;
	}


	if (L"R_" == m_DirString[m_eDir])
	{
		if (0 >= m_AimPos.X)
		{
			m_AimPos.X *= -1.0f;
			if (180.0f >= m_Degree && 90.0f <= m_Degree)
			{
				m_Degree = 180.0f - m_Degree;
			}
			else
			{
				m_Degree = 540.0f - m_Degree;
			}
		}
	}
	else if (L"L_" == m_DirString[m_eDir])
	{
		if (0 < m_AimPos.X)
		{
			m_AimPos.X *= -1.0f;
			if (0.0f <= m_Degree && 90.0f >= m_Degree)
			{
				m_Degree = 180.0f - m_Degree;
			}
			else
			{
				m_Degree = 540.0f - m_Degree;
			}
		}
	}

	if (0 > m_AimPos.X)
	{
		if (true == CGAMEINPUT::Press(L"U"))
		{
			m_Degree -= 180.0f * CGAMETIME::DeltaTime();
			if (90.0f >= m_Degree)
			{
				m_Degree = 90.0f;
			}
		}
		if (true == CGAMEINPUT::Press(L"D"))
		{
			m_Degree += 180.0f * CGAMETIME::DeltaTime();
			if (270.0f <= m_Degree)
			{
				m_Degree = 270.0f;
			}
		}
	}
	else
	{
		if (true == CGAMEINPUT::Press(L"U"))
		{
			m_Degree += 180.0f * CGAMETIME::DeltaTime();
			if (90.0f <= m_Degree && 180.0f > m_Degree)
			{
				m_Degree = 90.0f;
			}
		}
		if (true == CGAMEINPUT::Press(L"D"))
		{
			m_Degree -= 180.0f * CGAMETIME::DeltaTime();
			if (270.0f >= m_Degree && 180.0f <= m_Degree)
			{
				m_Degree = 270.0f;
			}
		}
	}

	if (0 > m_Degree)
	{
		m_Degree = 360.0f + m_Degree;
	}
	if (360.0f < m_Degree)
	{
		m_Degree = m_Degree - 360.0f;
	}

	//int spriteNum = DegreeChange();

	if (90.0f == m_Degree)
	{
		AimPoint->SetPos({0.0f, 70.0f});
	}
	else
	{
		AimPoint->SetPos(CVECTOR::DegToDir(m_Degree) * 70);
	}
	
	int SpriteNum;
	float TmpDegree = -m_Degree + 90.0f;

	if (L"L_" == m_DirString[m_eDir])
	{
		if (360.0f <= TmpDegree)
		{
			TmpDegree -= 360.0f;
		}
		else if (0 > TmpDegree)
		{
			TmpDegree += 360.0f;
		}

		SpriteNum = (int)(TmpDegree / 5.625f);

		if (0 == SpriteNum)
		{
			SpriteNum = 31;
		}
		else
		{
			SpriteNum = SpriteNum - 32;
		}
	}
	else if (L"R_" == m_DirString[m_eDir])
	{
	
		if (360.0f <= TmpDegree)
		{
			TmpDegree -= 360.0f;
		}
		else if (0 > TmpDegree)
		{
			TmpDegree += 360.0f;
		}

		SpriteNum = (int)(TmpDegree / 5.625f);

		if (0 == SpriteNum)
		{
			SpriteNum = 31;
		}
		else
		{
			SpriteNum = 32 - SpriteNum;
		}
	}
	
	if (true == PlayerAni->CurAniEnd())
	{
		PlayerAni->Off();
		PlayerRender->Sprite(m_DirString[m_eDir] + L"wbaz.bmp", SpriteNum);
		PlayerRender->On();
	}

	AimPoint->Sprite(m_DirString[m_eDir] + L"crshairb.bmp", SpriteNum); // 여기서 각도에 따른 Sprite 출력(현재 고정)
	AimPoint->On();

	// 미사일일 비어 있지 않을 경우만,
	if (false == m_Bullet->CheckEmpty())
	{
		// 발사 버튼
		if (true == CGAMEINPUT::Down(L"Fire"))
		{
			CGAMESOUND::Play(L"GazeSound", L"ROCKETPOWERUP.wav");

			m_Bullet->SetDir(AimPoint->GetPos());
			m_Bullet->SetPos(GetPos());
			m_Bullet->Off();
		}

		if (true == CGAMEINPUT::Press(L"Fire"))
		{
			m_Bullet->GetPower();
			GazePowerOn();
		}

		if (true == CGAMEINPUT::Up(L"Fire"))
		{
			GazePowerOff();

			if (true == CGAMESOUND::IsPlay(L"GazeSound"))
			{
				CGAMESOUND::Stop(L"GazeSound");
			}

			CGAMESOUND::Play(L"ROCKETRELEASE.wav");
			
			m_Bullet->SetDir(AimPoint->GetPos());
			m_Bullet->SetPos({ GetPos().X + CVECTOR::DegToDir(m_Degree).X * 5.0f, GetPos().Y + CVECTOR::DegToDir(m_Degree).Y * 5.0f });
			m_Bullet->Fire(AimPoint->GetPos());
			m_Bullet->On();
		}
	}

	if (false == CGAMEINPUT::Press(L"Fire") && false == m_Bullet->CheckFire() && false == m_Bullet->CheckBoom())
	{

		if (true == CGAMEINPUT::Down(L"JUMP"))
		{
			PlayerAni->On();
			PlayerRender->Off();

			AimPoint->Off();  // 문제 확인
			ChangeState(PLAYERSTATE::PS_JUMP);
			return;
		}    

		if (true == CGAMEINPUT::Down(L"L") || true == CGAMEINPUT::Down(L"R"))
		{
			PlayerAni->On();
			PlayerRender->Off();

			AimPoint->Off();
			ChangeState(PLAYERSTATE::PS_MOVE);
			return;
		}
		//m_Bazooka->Sprite(m_DirString[m_eDir] + L"wbaz.bmp", 0);

		if (CurWeaponName != ClickWeaponName)
		{
			PlayerAni->On();
			PlayerRender->Off();

			CurWeaponName = ClickWeaponName;
			m_AniString[PS_WEAPON] = CurWeaponName;
			//ChangeState(PLAYERSTATE::PS_WEAPON, L"_Back");

			ChangeState(PLAYERSTATE::PS_WEAPON, L"_Open");
			return;
		}
	}
}

////////////////////////////////////////////////////////////////////////

int CGAMEPLAYER::SpriteNum(CVECTOR _Next, CVECTOR _Cur)
{
	// 방향이 있는 상태로 32 개로 나누어서 이미지 16개씩
	//  0 ~  15 : 위쪽(90) ~ 오른쪽(or 왼쪽)가기전
	//  16 ~ 31 : 오른쪽(or 왼쪽)(0도) ~ 아래가기전

	int SpriteNum;

	float TmpDegree = CVECTOR::DirToDeg(_Cur, _Next);
	TmpDegree = -TmpDegree + 90.0f;

	if (L"L_" == m_DirString[m_eDir])
	{
		if (360.0f <= TmpDegree)
		{
			TmpDegree -= 360.0f;
		}
		else if (0.0f > TmpDegree)
		{
			TmpDegree += 360.0f;
		}

		SpriteNum = (int)(TmpDegree / 5.625f);

		if (0 != SpriteNum)
		{
			SpriteNum = 64 - SpriteNum;
		}
	}
	else if (L"R_" == m_DirString[m_eDir])
	{

		if (360.0f <= TmpDegree)
		{
			TmpDegree -= 360.0f;
		}
		else if (0.0f > TmpDegree)
		{
			TmpDegree += 360.0f;
		}

		SpriteNum = (int)(TmpDegree / 5.625f);
	}

	if (32 == SpriteNum)
	{
		SpriteNum -= 1;
	}

	return SpriteNum;
}

void CGAMEPLAYER::ChangeSpriteAni()
{
	if (PlayerAni->IsOn())
	{
		PlayerAni->On();
		PlayerRender->Off();
	}
	else
	{
		PlayerAni->Off();
		PlayerRender->On();
	}
}



// DirColorCheck
void CGAMEPLAYER::DirColorCheck(int _X, int _Y)
{
	// 프레임마다 확인
	for (size_t i = 0; i < COLORDIR::MAX; i++)
	{
		PosDir[i] = false;
	}

	// 1920 x 696

	CVECTOR Pos = GetPos();
	CGAMETEX* Tex = CGAMEMAP::TexRender->GetTex();
	CVECTOR Color = Tex->GetColor(Pos.IntX(), -(Pos.IntY()) + _Y);
	if (Color != CVECTOR::WRITE)
	{ PosDir[COLORDIR::B] = true; };
	Color = Tex->GetColor(Pos.IntX(), -(Pos.IntY()) - _Y);
	if (Color != CVECTOR::WRITE)
	{ PosDir[COLORDIR::T] = true; };

	Color = Tex->GetColor(Pos.IntX() + _X, -(Pos.IntY()) - _Y);
	if (Color != CVECTOR::WRITE)
	{ PosDir[COLORDIR::RT] = true; };
	Color = Tex->GetColor(Pos.IntX() + _X, -(Pos.IntY()));
	if (Color != CVECTOR::WRITE)
	{ PosDir[COLORDIR::R] = true; };
	Color = Tex->GetColor(Pos.IntX() + _X, -(Pos.IntY()) + _Y);
	if (Color != CVECTOR::WRITE)
	{ PosDir[COLORDIR::RB] = true; };

	Color = Tex->GetColor(Pos.IntX() - _X, -(Pos.IntY()) - _Y);
	if (Color != CVECTOR::WRITE)
	{ PosDir[COLORDIR::LT] = true; };
	Color = Tex->GetColor(Pos.IntX() - _X, -(Pos.IntY()));
	if (Color != CVECTOR::WRITE)
	{ PosDir[COLORDIR::L] = true; };
	Color = Tex->GetColor(Pos.IntX() - _X, -(Pos.IntY()) + _Y);
	if (Color != CVECTOR::WRITE)
	{ PosDir[COLORDIR::LB] = true; };
}


// Camera
void CGAMEPLAYER::SetPlayerCameraMove(const ACTORDIR _DirStr, const float _Scale)
{
	switch (_DirStr)
	{
	case AD_RIGHT:
	{
		if (2500.0f >= CGAMEPLAYBACK::TexRender->GetPos().X)
		{
			m_PlayerCameraPos->X = CVECTOR::RIGHT.X * _Scale;
			m_PlayerCameraPos->Y = CVECTOR::RIGHT.Y * _Scale;
		}
		break;
	}
	case AD_LEFT:
	{
		if (0.0f <= CGAMEPLAYBACK::TexRender->GetPos().X)
		{
			m_PlayerCameraPos->X = CVECTOR::LEFT.X * _Scale;
			m_PlayerCameraPos->Y = CVECTOR::LEFT.Y * _Scale;
		}
		break;
	}
	case AD_UP:
	{
		if (0.0f >= CGAMEPLAYBACK::TexRender->GetPos().Y)
		{
			m_PlayerCameraPos->X = CVECTOR::UP.X * _Scale;
			m_PlayerCameraPos->Y = CVECTOR::UP.Y * _Scale;
		}
		break;
	}
	case AD_DOWN:
	{
		if (-1563.0f <= CGAMEPLAYBACK::TexRender->GetPos().Y)
		{
			m_PlayerCameraPos->X = CVECTOR::DOWN.X * _Scale;
			m_PlayerCameraPos->Y = CVECTOR::DOWN.Y * _Scale;
		}		
		break;
	}
	default:
		break;
	}
	GetParentScene()->CameraPosMove(*m_PlayerCameraPos);
}

void CGAMEPLAYER::SetPlayerCameraCurPos()
{
	GetParentScene()->CameraPos(GetPos());
}


CBAZOOKA* CGAMEPLAYER::GetPlayerBullet()
{
	return m_Bullet;
}

void CGAMEPLAYER::ColEnter(CCOL* _this, CCOL* _Other)
{

}

void CGAMEPLAYER::ColStay(CCOL* _this, CCOL* _Other)
{

}

void CGAMEPLAYER::ColExit(CCOL* _this, CCOL* _Other)
{

}
