#include "CGAMEOBJECT.h"


CGAMEOBJECT::CGAMEOBJECT() 
	: m_pParentScene(nullptr)
	, m_CameraEffect(true)
{
}

CGAMEOBJECT::~CGAMEOBJECT()
{
	{
		std::list<CRENDER*>::iterator Start = m_AllRender.begin();
		std::list<CRENDER*>::iterator End = m_AllRender.end();

		for (; Start != End; ++Start)
		{
			delete (*Start);
		}
	}

	{
		std::list<CCOL*>::iterator Start = m_AllCol.begin();
		std::list<CCOL*>::iterator End = m_AllCol.end();

		for (; Start != End; ++Start)
		{
			delete (*Start);
		}
	}
}


///////////////

void CGAMEOBJECT::Loading()
{
}

void CGAMEOBJECT::UpdatePrev()
{
}

void CGAMEOBJECT::Update()
{
}

void CGAMEOBJECT::UpdateNext()
{
}

void CGAMEOBJECT::Render()
{
	std::list<CRENDER*>::iterator Start = m_AllRender.begin();
	std::list<CRENDER*>::iterator End = m_AllRender.end();

	for (;Start != End ; ++Start)	
	{
		(*Start)->Render();
	}
}