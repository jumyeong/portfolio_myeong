#pragma once
#include "CRENDER.h"

class CGAMETEX;
class CRENDERTEX : public CRENDER
{
private:
	unsigned int m_TransColor;
	CGAMETEX* m_Tex;

public:
	CGAMETEX* GetTex() {
		return m_Tex;
	}

	void SetTex(const CGAMESTRING& _Name);
	void TransColor(unsigned int _R, unsigned int _G, unsigned int _B, unsigned int _A);
	void ScaleToImageSize();
	void ScaleToImageSize(float multiple);

public:
	void Render();
	
public:
	CRENDERTEX();
};

