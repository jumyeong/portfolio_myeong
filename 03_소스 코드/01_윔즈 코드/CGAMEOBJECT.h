#pragma once
#include <CGAMEMATH.h>
#include <list>
#include "CRENDER.h"
#include "CGAMESTATE.h"
#include "CCOL.h"

class CRENDER;
class CGAMESCENE;
class CGAMEOBJECT : public CGAMESTATE
{
	friend CGAMESCENE;

public:
	CGAMEOBJECT();
	virtual ~CGAMEOBJECT();

	///////////////////////////// Camera Effect
private:
	bool m_CameraEffect;

public:
	bool GetCameraEffect()
	{
		return m_CameraEffect;
	}

	void CameraEffectOn()
	{
		m_CameraEffect = true;
	}
	void CameraEffectOff()
	{
		m_CameraEffect = false;
	}
	///////////////////////////// Camera Effect

private:
	CGAMESCENE*		m_pParentScene;
	CVECTOR			m_Pos;

public:
	CGAMESCENE* GetParentScene()
	{
		return m_pParentScene;
	}

public:
	void SetPos(const CVECTOR& _Pos) { m_Pos = _Pos; }
	CVECTOR GetPos() { 
		return m_Pos; 
	}

	void Move(const CVECTOR& _Pos) 
	{
		m_Pos += _Pos; 
	}

	// 모든 오브젝트는 장면에서 어떤 행동을 해야한다

public:
	virtual void Loading();
	virtual void UpdatePrev();
	virtual void Update();
	virtual void UpdateNext();
	virtual void Debug() {}


	//////////////////////////// Render
private:
	std::list<CRENDER*> m_AllRender;

public:
	template <typename T>
	T* CreateRender(int _Order, CVECTOR _Size, CVECTOR _Pos)
	{
		T* NewRender = new T();
		NewRender->m_Order = _Order; 
		NewRender->m_ParentObject = this;
		NewRender->m_Pos = _Pos;
		NewRender->m_Size = _Size;
		NewRender->PushRender();
		// 삭제와 생성을 위한 자료구조
		m_AllRender.push_back(NewRender);

		return NewRender;
	}

///////////////////////// Col

private:
	std::list<CCOL*>		m_AllCol;

public:
	CCOL* CreateCol(int _Order, CVECTOR _Size, CVECTOR _Pos, COLTYPE _eType = COLTYPE::CT_RECT)
	{
		CCOL* NewCol = new CCOL();
		NewCol->m_Order = _Order;
		NewCol->m_ParentObject = this;
		NewCol->m_Pos = _Pos;
		NewCol->m_Size = _Size;
		NewCol->ColType = _eType;
		NewCol->PushCol();

		m_AllCol.push_back(NewCol);

		return NewCol;
	}

	virtual void ColEnter(CCOL* _this, CCOL* _Other) = 0;
	virtual void ColStay(CCOL* _this, CCOL* _Other) = 0;
	virtual void ColExit(CCOL* _this, CCOL* _Other) = 0;

public:
	void Render();

};

