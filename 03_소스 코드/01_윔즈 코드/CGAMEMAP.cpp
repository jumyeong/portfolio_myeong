#include "CGAMEMAP.h"
#include "CRENDERTEX.h"
#include <CGAMEWINDOW.h>
#include "CGAMETEX.h"


CRENDERTEX* CGAMEMAP::TexRender = nullptr;

CGAMEMAP::CGAMEMAP()
{
}

CGAMEMAP::~CGAMEMAP()
{
}

void CGAMEMAP::Loading()
{
	SetPos({ 0, 0 });

	TexRender = CreateRender<CRENDERTEX>(10, { 1920.0f, 696.0f }, { 0, 0 });
	TexRender->SetTex(L"C_CHATEAU_1x_update.bmp");
	TexRender->TransColor(255, 255, 255, 0);
	TexRender->ScaleToImageSize();

	CVECTOR ConPos = { TexRender->GetTex()->GetW(), TexRender->GetTex()->GetH() };
	ConPos = ConPos.HalfVector();
	ConPos.Y = -ConPos.Y;
	TexRender->SetPos(ConPos);
}
void CGAMEMAP::Update()
{
}