#include "CINVEN.h"
#include <CGAMEWINDOW.h>
#include "CRENDERTEX.h"
#include "CGAMESPRITE.h"
#include "LOGICHEADER.h"
#include "CGAMEINPUT.h"
#include <CGAMEMATH.h>
#include "CGAMESCENE.h"

CINVEN::CINVEN() 
	: StartInvenPos(500.0f - 32.0f - 32.0f - 16.0f, 86.0f)
	, WeaponIconTexRender{ nullptr }
	, WeaponIconNameArr{ L"" }
	, NoWeaponIconNameArr{ L"" }
	, BorderLine(nullptr)
	, m_SelectIndex()
	, isClick(false)
	, ClickWeapon(L"")
{
	{
		WeaponIconNameArr[1][0] = L"Bazooka.1.bmp";
		WeaponIconNameArr[1][1] = L"Homing Missile.1.bmp";
		WeaponIconNameArr[2][0] = L"Grenade.1.bmp";
		WeaponIconNameArr[3][0] = L"Shotgun.1.bmp";
		WeaponIconNameArr[4][0] = L"Mine.1.bmp";
		WeaponIconNameArr[4][1] = L"Holy Grenade.1.bmp";
		WeaponIconNameArr[5][0] = L"Air Strike.1.bmp";
		WeaponIconNameArr[7][0] = L"Teleport.1.bmp";
	}

	{
		NoWeaponIconNameArr[1][0] = L"Bazooka.2.bmp";
		NoWeaponIconNameArr[1][1] = L"Homing Missile.2.bmp";
		NoWeaponIconNameArr[2][0] = L"Grenade.2.bmp";
		NoWeaponIconNameArr[3][0] = L"Shotgun.2.bmp";
		NoWeaponIconNameArr[4][0] = L"Mine.2.bmp";
		NoWeaponIconNameArr[4][1] = L"Holy Grenade.2.bmp";
		NoWeaponIconNameArr[5][0] = L"Air Strike.2.bmp";
		NoWeaponIconNameArr[7][0] = L"Teleport.2.bmp";
	}
}


CINVEN::~CINVEN()
{
}

void CINVEN::Loading()
{
	TexRender = CreateRender<CRENDERTEX>(CG_INVENTORY, { 177.0f, 362.0f }, { 0, 0 });
	TexRender->SetTex(L"Inventory.bmp");
	TexRender->TransColor(1, 0, 0, 0);
	TexRender->ScaleToImageSize();

	CameraEffectOff();

	TexRender->SetPos({ 468.0f, -100.0f });

	CVECTOR RightPos = { 33.0f, 0.0f };
	CVECTOR DownPos	 = { 0.0f, -33.0f };

	// UI는 전부다 100번 ENUM COL_UI
	for (int i = 0; i < 5; ++i)
	{
		for (int j = 0; j < 11; ++j)
		{
			ARRCOL[j][i] = CreateCol(CG_INVENTORY, { 31.0f, 31.0f }, StartInvenPos + (DownPos * (float)j) + (RightPos * (float)i));

			if (L"" != WeaponIconNameArr[j][i])
			{
				WeaponIconTexRender[j][i] = CreateRender<CRENDERTEX>(CG_INVENTORY + 1, { 32.0f, 32.0f }, { 0, 0 });
				
				// 그냥 전부 만들자 수량이 없으면, 다른 걸로 나오게 변경
				if (true)
				{
					WeaponIconTexRender[j][i]->SetTex(WeaponIconNameArr[j][i]);
				}
				else
				{
					WeaponIconTexRender[j][i]->SetTex(NoWeaponIconNameArr[j][i]);
				}

				WeaponIconTexRender[j][i]->TransColor(0, 0, 0, 0);
				WeaponIconTexRender[j][i]->ScaleToImageSize();
				WeaponIconTexRender[j][i]->SetPos(StartInvenPos + (DownPos * (float)j) + (RightPos * (float)i));
			}
		}
	}
	// CreateCol(100, { 32.0f, 32.0f }, StartInvenPos + CVECTOR{33.0f, 0.0f});
	// CreateCol(100, { 32.0f, 32.0f }, { 500.0f, -100.0f });

	CameraEffectOff();
}

void CINVEN::Update()
{

}

void CINVEN::ColEnter(CCOL* _this, CCOL* _Other) 
{
	if (nullptr != BorderLine)
	{
		return;
	}

	m_SelectIndex.X = -1;
	m_SelectIndex.Y = -1;

	for (size_t i = 0; i < 5; ++i)
	{
		for (size_t j = 0; j < 11; ++j)
		{
			if (ARRCOL[j][i] == _this)
			{
				m_SelectIndex.X = (float)i;
				m_SelectIndex.Y = (float)j;

				break;
			}
		}
		if (-1 != m_SelectIndex.IntX() || -1 != m_SelectIndex.IntY())
		{
			break;
		}
	}

	if (-1 == m_SelectIndex.IntX() || -1 == m_SelectIndex.IntY())
	{
		return;
	}

	if (nullptr == WeaponIconTexRender[m_SelectIndex.IntY()][m_SelectIndex.IntX()])
	{
		return;
	}

	CVECTOR WeaponIconPos = WeaponIconTexRender[m_SelectIndex.IntY()][m_SelectIndex.IntX()]->GetPos();
	WeaponIconPos.X -= 1;

	BorderLine = CreateRender<CRENDERTEX>(CG_INVENTORY + 2, { 0.0f, 0.0f }, WeaponIconPos);
	BorderLine->SetTex(L"SeletWeapon.bmp");
	BorderLine->TransColor(0, 0, 0, 0);
	BorderLine->ScaleToImageSize();
}
void CINVEN::ColStay(CCOL* _this, CCOL* _Other) 
{
	if (nullptr == BorderLine)
	{
		return;
	}

	// select index 예외 처리
	if (-1 == m_SelectIndex.IntX() || -1 == m_SelectIndex.IntY())
	{
		return;
	}

	ClickWeapon = WeaponIconNameArr[m_SelectIndex.IntY()][m_SelectIndex.IntX()];
	int DelStrPos = (int)ClickWeapon.find(L".");
	ClickWeapon.replace(DelStrPos, 6, L"");	// 나중에 아이템 명으로 출력을 위함

	if (true == CGAMEINPUT::Down(L"SelectingWeapon"))
	{
		SetClick(true);

		// 현재 플레이어의 모션을 변경할 수 있게 해야함.
		// 즉, 현재 플레이어의 정보를 플레이어에게 주어 Update 처리해야한다?
		// 직접 처리할려고 한다면 현재 플레이어의 정보뿐만 아니라 현재 모션이 인벤 열기가 가능한가? 같은 상황들을 고려해야함
		// WeaponIconNameArr[m_SelectIndex->IntY()][m_SelectIndex->IntX()]; 현재 무기 이름 
	}
}
void CINVEN::ColExit(CCOL* _this, CCOL* _Other) 
{
	if (nullptr == BorderLine)
	{
		return;
	}

	if (false == BorderLine->IsDeath())
	{
		BorderLine->Death();
		BorderLine = nullptr;

		m_SelectIndex.X = -1;
		m_SelectIndex.Y = -1;
	}
}
