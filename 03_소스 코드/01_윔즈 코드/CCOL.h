#pragma once
#include "CSUBOBJECT.h"
#include <set>

enum class COLTYPE
{
	CT_POINT,
	CT_CIRCLE,
	CT_RECT,
	CT_MAX
};

class CCOLFIGURE
{
public:
	CVECTOR m_Pos;
	CVECTOR m_Size;

public:
	float LEFT() const
	{
		return m_Pos.X - m_Size.HalfX();
	}
	float RIGHT() const
	{
		return m_Pos.X + m_Size.HalfX();
	}
	float TOP() const
	{
		return m_Pos.Y + m_Size.HalfY();
	}
	float BOTTOM() const
	{
		return m_Pos.Y - m_Size.HalfY();
	}

public:
	CVECTOR LT() const
	{
		return { LEFT(), TOP() };
	}
	CVECTOR RT() const
	{
		return { RIGHT(), TOP() };
	}
	CVECTOR LB() const
	{
		return { LEFT(), BOTTOM() };
	}
	CVECTOR RB() const
	{
		return { RIGHT(), BOTTOM() };
	}

};

class CGAMEOBJECT;
class CGAMESCENE;
class CCOL : public CSUBOBJECT
{
public:
	static bool RECTTORECT(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right);
	static bool RECTTOCIRCLE(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right);
	static bool RECTTOPOINT(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right);

	static bool CIRCLETORECT(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right);
	static bool CIRCLETOCIRCLE(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right);
	static bool CIRCLETOPOINT(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right);

	static bool POINTTORECT(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right);
	static bool POINTTOCIRCLE(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right);
	static bool POINTTOPOINT(const CCOLFIGURE& _Left, const CCOLFIGURE& _Right);

	static bool (*ARRCOL[static_cast<int>(COLTYPE::CT_MAX)][static_cast<int>(COLTYPE::CT_MAX)]) (const CCOLFIGURE& _Left, const CCOLFIGURE& _Right);

public:
	// CGAMECOLSTATER class
	class CGAMECOLSTATER
	{
	public:
		CGAMECOLSTATER();
	};
	friend CGAMECOLSTATER;
	static CGAMECOLSTATER Starter;

public:
	friend CGAMESCENE;
	friend CGAMEOBJECT;

private:
	CCOLFIGURE	m_FI;
	COLTYPE		ColType = COLTYPE::CT_MAX;

	std::set<CCOL*>::iterator FindCol;
	std::set<CCOL*> m_OtherCol;

	bool ColRangeCheck(const CCOL* _Other);
	void ColCheck(CCOL* _Col);

public:
	void ColUpdate();
	void Debug() override;

private:
	void PushCol();
};

