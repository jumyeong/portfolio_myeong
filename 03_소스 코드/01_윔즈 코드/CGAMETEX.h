#pragma once
#include "CGAMERESBASE.h"
#include <Windows.h>
#include <CGAMEMATH.h>

class CGAMETEX : public CGAMERESBASE
{
private:
	HDC m_Image;
	HBITMAP m_BitMap;
	HBITMAP m_OldBitMap;
	BITMAP m_Info;

	unsigned int m_W;
	unsigned int m_H;

public:
	unsigned int GetW()
	{
		return m_W;
	}
	unsigned int GetH()
	{
		return m_H;
	}

public:
	inline HDC IMGDC()
	{
		return m_Image;
	}
	void Loading();
	void Create(const CVECTOR& _Size);

	// 색깔 가져오는 함수이다.
	CVECTOR GetColor(int X, int Y);

public:
	CGAMETEX() : m_Image(nullptr), m_BitMap(nullptr), m_OldBitMap(nullptr), m_W(0), m_H(0), m_Info{ 0 } {	}
	~CGAMETEX();
};

