#pragma once
#include "CGAMESCENE.h"
#include <vector>

class CMOUSEOBJECT;
class CINVEN;
class CGAMEMAP;
class CGAMEPLAYBACK;
class CGAMEPLAYER;
class CPLAYSCENE : public CGAMESCENE
{
private:
	std::vector<CGAMEPLAYER*> vecPlayers;
	CINVEN* m_Inven;
	CMOUSEOBJECT* m_Cursor;

	CGAMEOBJECT* m_BoomObject;
	bool m_BoomHitCheck;
	CVECTOR m_BoomPos;

public:
	void Loading() override;
	void SceneUpdate() override;
	void DebugRender() override;

	void ChangeCameraTarget() override;

public:
	CPLAYSCENE();
	virtual ~CPLAYSCENE();
};

