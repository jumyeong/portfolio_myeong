#pragma once
#include "CRENDER.h"

class CGAMESPRITE;
class CRENDERSPRITE : public CRENDER
{
private:
	CGAMESPRITE* m_pSprite;
	size_t m_Index;

public:
	void Sprite(const CGAMESTRING& _SpriteName, size_t _Index);

public:
	void Render() override;

public:
	CRENDERSPRITE();
};

