#pragma once
#include <CGAMEMATH.h>
#include "CGAMESTATE.h"
#include <Windows.h>

class CGAMESCENE;
class CGAMEOBJECT;
class CSUBOBJECT : public CGAMESTATE
{
public:
	friend CGAMEOBJECT;
	friend CGAMESCENE;

protected:
	CGAMEOBJECT* m_ParentObject;

	//	////// MY CODE /////
	//public:
	//	HDC m_hdc;
	//	PAINTSTRUCT m_PS;
	//	HBRUSH m_Brush;
	//	HBRUSH m_OldBrush;
	//	////// MY CODE /////

private:
	int m_Order;

public:
	int GetOrder()
	{
		return m_Order;
	}

protected:
	CVECTOR m_Pos;
	CVECTOR m_Size;

	bool isPenColorNull; // Collision �� ����

public:
	void SetPos(const CVECTOR& _Pos) { m_Pos = _Pos; }
	CVECTOR GetPos() { return m_Pos; }

	void SetSize(const CVECTOR& _Size) { m_Size = _Size; }
	CVECTOR GetSize() { return m_Size; }

public:
	bool IsUpdate() override;
	bool IsLive() override;
	bool IsDeath() override;

public:
	inline bool IsPenColorNull()
	{
		return isPenColorNull;
	}

	inline void PenColorRed()
	{
		isPenColorNull = false;
	}
	inline void PenColorNull()
	{
		isPenColorNull = true;
	}

public:
	CVECTOR RenderPos() const;

public:
	virtual void Debug();

public:
	CSUBOBJECT() 
		: m_Order(0)
		, m_ParentObject(nullptr)
		, m_Pos()
		, m_Size()
		, isPenColorNull(true) 
	{	
	}

	virtual ~CSUBOBJECT() 
	{
	}
};

