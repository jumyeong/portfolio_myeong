#pragma once

class CGAMESTATE
{
private:
	bool m_Update;
	bool m_Death;

public:
	// ��� Ű�� ���� ���
	void On() {
		m_Update = true;
	}

	void Off() {
		m_Update = false;
	}

	// ���̴� ���
	void Death() {
		m_Death = true;
	}

	bool IsOn() {
		return m_Update;
	}

public:
	virtual bool IsUpdate() {
		return IsOn() && IsLive();
	}

	virtual bool IsLive() {
		return !m_Death;
	}

	virtual bool IsDeath() {
		return m_Death;
	}

public:
	CGAMESTATE()
		: m_Update(true)
		, m_Death(false)
	{	
	}
	virtual ~CGAMESTATE() = 0;

};

